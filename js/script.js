$(document).ready(function(){
    
    $(document).ajaxStart(function() {
        $('#loading').show();
    });
    
    $(document).ajaxStop(function() {
        $('#loading').hide();
    });
    
    $('#date').datepicker({
        format: "dd.mm.yyyy",
        todayBtn: "linked",
        keyboardNavigation: false,
        autoclose: true,
        todayHighlight: true,
        language: 'ru'
    });
    
    $('#club_card').inputmask('9999',{ 
        showMaskOnHover: false,
        "placeholder": ""
    });
    
    $('#date').inputmask('d.m.y',{ 
        showMaskOnHover: false,
        "placeholder": "  .  .    "
    });
    
    $('#price').inputmask('9999999',{ 
        showMaskOnHover: false,
        "placeholder": ""
    });
    
    $('#phone').inputmask('+7(999)9999999',{ 
        showMaskOnHover: false,
        "placeholder": "+7(   )       "
    });
    
    $('#state_number, .number-input').inputmask('Regex',{ 
        regex: "[A-Z][0-9]{3}[A-Z]{2} [0-9|A-Z]{6}",
        showMaskOnHover: false,
        "placeholder": "A000AA 000AAA"
    });
    
    $('#time').inputmask('h:s',{ 
        showMaskOnHover: false,
        "placeholder": "  :  "
    });
    
    setTimeout(checkEvents, 2000);
    
    $('#right-panel-button').on('click', function(){
        if($('#right-panel').hasClass('close')){
            $('#right-panel').removeClass('close');
            $('#wrap').show();
        }
    });
    
    $('#right-panel .close-button').on('click', function(){
        $('#right-panel').addClass('close');
            $('#wrap').hide();
    });
    
    $('#left-panel-button').on('click', function(){
        if ($('#left-panel').hasClass('close')){
            $('#left-panel').removeClass('close');
            $('body').addClass('left-panel-open');
        }else{
            $('#left-panel').addClass('close');
            $('body').removeClass('left-panel-open');
        }
    });
    
    $('#wrap').on('click', function(){
        $('#right-panel .close-button').click();
        if($('.drop-menu-list.open').length > 0) {
            $('.drop-menu-list.open').fadeOut('200');
            $('.drop-menu-list.open').removeClass('open');
            $('#wrap').hide();
            $('#wrap').removeClass("transparent");
            $(window).unbind('scroll');
        }
        $('.modal.open').hide(0, function(){
            $(this).removeClass('open');
        });
    });
    
    $('.search').on('click', function(){
        if($('#search-input').hasClass('hidden')){
            $('#search-input').removeClass('hidden');
            $('#search-input input').animate({
                width: 200,
                paddingLeft: 10,
                paddingRight: 10
            }, 500, function(){
                $('#search-input input').focus();
            });
            
        }else{
            $('#search-input input').animate({
                width: 0,
                padding: 0
            }, 500, function(){
                $('#search-input').addClass('hidden');
            });
        }
    });
    
    $('body').on('click', '.drop-menu', function(event){
        $("#wrap").addClass("transparent");
        event.stopPropagation();
        var $menu = $(this).children('ul');
        if($(this).position().left + 140 >= $(document).width()){
            $menu.css('margin-left', '-85px');
        }
        OffScroll ();
        $('#wrap').show();
        $menu.fadeIn(200, function(){
            $menu.addClass('open');
        });
    });
    
    $('body').on('click', '.dropdown-btn', function(event){
        event.stopPropagation();
        var $menu = $(this).children('ul');
        if($(this).position().left + 140 >= $(document).width()){
            $menu.css('margin-left', '-85px');
        }else{
            $width = $menu.outerWidth() / 5;
            $height = $menu.height() / 2;
            $menu = $menu.css({
                marginLeft: -$width,
                marginTop: -$height
            });
        }
        OffScroll ();
        $menu.fadeIn(200, function(){
            $menu.addClass('open');
        });
        
        $('body').on('click', function(){
            $(window).unbind('scroll');
            $('.dropdown-btn ul').fadeOut('200');
        });
    });
    
    $('.sort').on('click', function(){
        if($(this).hasClass('active-sort')){
            if($(this).attr('sort') == 'asc'){
                $(this).attr('sort', 'desc');
            }else{
                $(this).attr('sort', 'asc');
            }
        }else{
            $('.active-sort').removeClass('active-sort');
            $(this).addClass('active-sort');
            $(this).attr('sort', 'asc');
        }
        $('.navigation-btn:first-of-type').click();
    });
    
    $('.modal .close-button').on('click', function(){
        $(this).closest('.modal').hide(0, function(){
            $(this).removeClass('open');
        });
        $('#wrap').hide();
    });
    
    $('.autocomplete').on('focusin', function(){
        $(this).addClass('focus');
        var $width = $(this).width() + 18;
        var $left = $(this).position().left + 1;
        $(this).siblings('.autocomplete-area').css('width', $width);
        $(this).siblings('.autocomplete-area').css('left', $left);
    });
    
    $('body').on('click', '#right-panel .notification', function(){
        var $item = $(this);
        var notify_id = $item.attr('id');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api.php',
            data: { 
                'f': 'deleteNotification',
                'notify_id': notify_id
            },
            success: function(data){
                $item.remove();
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.');
            }
        });
    });
    
    $('body').on('click', '#notifications .notification', function(){
        $(this).remove();
    });
    
});

function OffScroll() {
    var winScrollTop = $(window).scrollTop();
    $(window).bind('scroll',function () {
        $(window).scrollTop(winScrollTop);
    });
}


function modalShow($item){
    $('.modal').each(function(){
        $(this).find('.close-button').click();
    });
    $item.show(0, function(){
        $left = ($(window).width() - $(this).width())/2;
        if($(this).height() > $(window).height()){
            $top = $(window)[0].scrollY + 10;
        }else{
            $top = ($(window).height() - $(this).height())/2 + $(window)[0].scrollY;
        }
        
        $(this).css('left', $left + 'px');
        $(this).css('top', $top + 'px');
        $item.addClass('open');
    });
    $('#wrap').show();
    
}

function inputValidation($input_arr){        
    $sub = true;
    $input_arr.each(function(){
        if($(this).hasClass('check-required')){
            var $val = $(this).val();
            $val = $.trim($val);
            if($val == ""){
                $(this).siblings('.input-errors-list').children('.error-required').show('fast');
                $sub = false;
            }else{
                $(this).siblings('.input-errors-list').children('.error-required').hide('fast');
            }
        }
    });
    return $sub;    
}

function checkEvents(){
    $.ajax({ 
        global: false,
        type: 'POST',  
        dataType: 'json',
        url: '/php/api.php',
        data: { 
            'f': 'checkEvents'
        },
        success: function(data){
            for (i = 0; i < data.records.length; i++){
                var $count = $('#notifications .notification').length;
                $('#notifications').prepend(data.records[i]);
                $('#right-panel .panel-body').prepend(data.records[i]);
            }
            for (i = 0; i < data.tasks.length; i++){
                var $count = $('#notifications .notification').length;
                $('#notifications').prepend(data.tasks[i]);
                $('#right-panel .panel-body').prepend(data.tasks[i]);
            }
        },
        error: function(data){
        }
    });
    setTimeout(checkEvents, 60000);
}