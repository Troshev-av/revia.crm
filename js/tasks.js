$(document).ready(function(){
    
    $('body').on('click', '#list-tasks + #table-info .navigation-btn', function(){
        $('.active-btn').removeClass('active-btn');
        $(this).addClass('active-btn');
        var $filter = $('#filter-tasks').val();
        var $column = $('.active-sort').attr('column');
        var $order = $('.active-sort').attr('sort');
        var $offset = $('.active-btn').attr('sheet');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_tasks.php',
            data: { 
                'f': 'getFilterTasks',
                'filter': $filter,
                'column': $column,
                'order': $order,
                'sheet': $offset,
            },
            success: function(data){
                $('#list-tasks .data-row').remove();
                $('#list-tasks').append(data.result);
                $('#table-navigation').html(data.tableNavigation);
                $('#records-info').html(data.tableCounter);
            },
            error: function(data){
                alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    $('#filter-tasks').on('change', function(){
        $('.navigation-btn:first-of-type').click();
    });
    
    $('#task-edit #client').on('keyup', function(){
        $('#client').val('').off('keyup');
        $('#client_id').val('');
    });
    
    $('#client').autocomplete({
        source: function(request, response){
            $.ajax({
                type: 'POST', 
                global: false,
                dataType: 'json',
                url: '/php/api_tasks.php',
                data: { 'f': 'clientSearch', 'str': request.term},
                success: function(data){
                    $('#client_id').val('');
                    response($.map(data, function(item){
                        return {
                            id: item.value,
                            label: item.label
                        }
                    }));
                }
            });
        },
        select: function( event, ui ) {
            $('#client_id').val(ui.item.id);
            $('#client').off('keyup').on('keyup', function(){
                $('#client').val('').off('keyup');
                $('#client_id').val('');
            });
        },
        minLength: 1,// начинать поиск с трех
        appendTo: '#client_area',
        position: { my : "left top", at: "left bottom" }
    });
    
    $('#task-add .form-submit').on('click', function(e){
        var $sub = inputValidation($('#task-add input'));
        if ($sub){
            var $client_id = $('#client_id').val();
            var $name = $('#name').val();
            var $date = $('#date').val();
            var $time = $('#time').val();
            $.ajax({ 
                type: 'POST',  
                dataType: 'json',
                url: '/php/api_tasks.php',
                data: { 
                    'f': 'newTask',
                    'client_id': $client_id,
                    'name': $name,
                    'date': $date,
                    'time': $time,
                },
                success: function(data){
                    $('#task-add .form-response-' + data.result).removeClass('hidden');
                    $('#task-add .form-response-' + !data.result).addClass('hidden');
                    $('#task-add input').val('');
                    $('#task-add .input-errors-list').children('.error-required').hide('fast');
                },
                error: function(data){
                     alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
                }
            });
        }
    });
    
    $('body').on('click', '#list-tasks .drop-menu .delete', function(){
        var $row = $(this).closest('.data-row');
        var $task = $row.attr('tasks');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_tasks.php',
            data: { 
                'f': 'deleteTask',
                'task_id': $task
            },
            success: function(data){
                $row.remove();
                var $str = $('#records-info').html();
                $str = $.trim($str);
                var $count = $str.charAt($str.length - 1);
                $count = parseInt($count) - 1;
                $str = $str.substring(0, $str.length-1) + $count;
                $('#records-info').html($str);
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    $('#task-edit .form-submit').on('click', function(){
        var $sub = inputValidation($('#task-edit input'));
        if ($sub){
            var $task_id = $('#task_id').val();
            var $client_id = $('#client_id').val();
            var $name = $('#name').val();
            var $date = $('#date').val();
            var $time = $('#time').val();
            
            $.ajax({ 
                type: 'POST',  
                dataType: 'json',
                url: '/php/api_tasks.php',
                data: { 
                    'f': 'updateTask',
                    'task_id': $task_id,
                    'client_id': $client_id,
                    'name': $name,
                    'date': $date,
                    'time': $time
                },
                success: function(data){
                    $('#task-edit .form-response-' + data.result).removeClass('hidden');
                    $('#task-edit .form-response-' + !data.result).addClass('hidden');
                    $('#task-edit .error-required').css('display', 'none');
                },
                error: function(data){
                     alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
                }
            });
        }
    });
    
    $('body').on('click', '#list-tasks .data-row .drop-menu-item.edit', function(){
        $task_id = $(this).closest('.data-row').attr('tasks');
        location.href = '/tasks/edit?task_id=' + $task_id;
    });
   
    $('body').on('click', '#list-tasks .data-row', function(){
        var $task_id = $(this).attr('task');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_tasks.php',
            data: { 
                'f': 'getTaskInfo',
                'task_id': $task_id
            },
            success: function(data){
                $('#modal-task #task_id').val(data.task_id);
                $('#modal-task #client-surname a').html(data.client);
                $('#modal-task #client-surname a').attr('href', '/clients?client_id=' + data.client_id);
                $('#modal-task #name').html(data.name);
                $('#modal-task #price').html(data.price);
                $('#modal-task #phone').html(data.phone);
                $('#modal-task #date').html(data.date + ' ' + data.time);
                modalShow($('#modal-task'));
            },
            error: function(data){
                alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
        
    });
    
    $('#modal-task #delete-task').on('click', function(){
        var $task = $('#task_id').val();
        $task = $.trim($task);
        if($task == ""){
            return false;
        }
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_tasks.php',
            data: { 
                'f': 'deleteTask',
                'task_id': $task
            },
            success: function(data){
                $('#wrap').click();
                $('.data-row[tasks="' + $task + '"]').remove();
                var $str = $('#records-info').html();
                $str = $.trim($str);
                var $count = $str.charAt($str.length - 1);
                $count = parseInt($count) - 1;
                $str = $str.substring(0, $str.length-1) + $count;
                $('#records-info').html($str);
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    $('#modal-task #edit-task').on('click', function(){
        var $task_id = $('#task_id').val();
        window.location = "/tasks/edit?task_id=" + $task_id;
    });
    
});