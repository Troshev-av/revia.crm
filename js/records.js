$(document).ready(function(){
    
    $('body').on('click', '.calendar .record-href, #modal-records-list .record-href', function(event){
        var $record_id = $(this).attr('record');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_records.php',
            data: { 
                'f': 'getRecordInfo',
                'record_id': $record_id
            },
            success: function(data){
                $('#modal-record #record_id').val(data.record_id);
                $('#modal-record #name').html(data.name);
                $('#modal-record #client-surname a').html(data.client);
                $('#modal-record #client-surname a').attr('href', '/clients?client_id=' + data.client_id);
                $('#modal-record #phone').html(data.phone);
                $('#modal-record #address').html(data.address);
                $('#modal-record #price').html(data.price);
                $('#modal-record #date').html(data.date + ' ' + data.time);
                modalShow($('#modal-record'));
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
        
    });
    
    $('.records-count').on('click', function(){
        var $listRecords = $(this).closest('.cell').children('.record-href').clone();
        modalShow($('#modal-records-list'));
        $('#modal-records-list .modal-body').html($listRecords);
        
    });
    
    $('#modal-record #delete-record').on('click', function(){
        var $record = $('#record_id').val();
        $record = $.trim($record);
        if($record == ""){
            return false;
        }
        $.ajax({ 
            type: 'POST',
            dataType: 'json',
            url: '/php/api_records.php',
            data: { 
                'f': 'deleteRecord',
                'record_id': $record
            },
            success: function(data){
                $('#wrap').click();
                $('.record-href[record="' + $record + '"]').remove();
                $('.data-row[record="' + $record + '"]').remove();
                var $str = $('.record-count').html();
                $str = $.trim($str);
                var $count = $str.charAt($str.length - 1);
                $count = parseInt($count) - 1;
                $str = $str.substring(0, $str.length-1) + $count;
                $('.record-count').html($str);
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    $('#modal-record #edit-record').on('click', function(){
        var $record_id = $('#record_id').val();
        window.location = "/records/edit?record_id=" + $record_id;
    });
    
    $('.calendar-month .calendar-navigation .navigation-btn').on('click', function(){
        $(this).closest('.calendar-month').hide();
        var $month_id = $(this).attr('target');
        $('#' + $month_id).show();
    })
    
    $('body').on('click', '#list-records + #table-info .navigation-btn', function(){
        $('.active-btn').removeClass('active-btn');
        $(this).addClass('active-btn');
        var $filter = $('#filter-record').val();
        var $column = $('.active-sort').attr('column');
        var $order = $('.active-sort').attr('sort');
        var $offset = $('.active-btn').attr('sheet');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_records.php',
            data: { 
                'f': 'getFilterRecords',
                'filter': $filter,
                'column': $column,
                'order': $order,
                'sheet': $offset,
            },
            success: function(data){
                $('#list-records .data-row').remove();
                $('#list-records').append(data.result);
                $('#table-navigation').html(data.tableNavigation);
                $('#records-info').html(data.tableCounter);
            },
            error: function(data){
                
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    $('#filter-record').on('change', function(){
        $('.navigation-btn:first-of-type').click();
    });
    
    $('body').on('click', '#list-records .data-row', function(){
        var $record_id = $(this).closest(".data-row").attr('record');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_records.php',
            data: { 
                'f': 'getRecordInfo',
                'record_id': $record_id
            },
            success: function(data){
                
                $('#modal-record #record_id').val(data.record_id);
                $('#modal-record #name').html(data.name);
                $('#modal-record #client-surname a').html(data.client);
                $('#modal-record #client-surname a').attr('href', '/clients?client_id=' + data.client_id);
                $('#modal-record #phone').html(data.phone);
                $('#modal-record #address').html(data.address);
                $('#modal-record #price').html(data.price);
                $('#modal-record #date').html(data.date + ' ' + data.time);
                modalShow($('#modal-record'));
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });

    $('#client').autocomplete({
        source: function(request, response){
            $.ajax({
                global: false,
                type: 'POST', 
                dataType: 'json',
                url: '/php/api_records.php',
                data: { 'f': 'clientSearch', 'str': request.term},
                success: function(data){
                    $('#client_id').val('');
                    response($.map(data, function(item){
                        return {
                            id: item.value,
                            label: item.label
                        }
                    }));
                }
            });
        },
        select: function( event, ui ) {
            $('#client_id').val(ui.item.id);
            $('#client').off('keyup').on('keyup', function(){
                $('#client').val('').off('keyup');
                $('#client_id').val('');
            });
        },
        minLength: 1,// начинать поиск с трех
        appendTo: '#client_area',
        position: { my : "left top", at: "left bottom" }
    });
    
    $('#record-edit #client').on('keyup', function(){
        $('#client').val('').off('keyup');
        $('#client_id').val('');
    });
    
    $('#record-add .form-submit').on('click', function(e){
        var $sub = inputValidation($('#record-add input'));
        if ($sub){
            var $client_id = $('#client_id').val();
            var $name = $('#name').val();
            var $price = $('#price').val();
            var $date = $('#date').val();
            var $time = $('#time').val();
            $.ajax({ 
                type: 'POST',  
                dataType: 'json',
                url: '/php/api_records.php',
                data: { 
                    'f': 'newRecord',
                    'client_id': $client_id,
                    'name': $name,
                    'price': $price,
                    'date': $date,
                    'time': $time,
                },
                success: function(data){
                    $('#record-add .form-response-' + data.result).removeClass('hidden');
                    $('#record-add .form-response-' + !data.result).addClass('hidden');
                    $('#record-add input').val('');
                },
                error: function(data){
                     alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
                }
            });
        }
    });
    
    $('body').on('click', '#list-records .drop-menu .delete', function(){
        var $row = $(this).closest('.data-row');
        var $record = $row.attr('record');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_records.php',
            data: { 
                'f': 'deleteRecord',
                'record_id': $record
            },
            success: function(data){
                $row.remove();
                var $str = $('#records-info').html();
                var count = parseInt($('#records-info .record-count').html());
                $('#records-info .record-count').html(count - 1);
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    $('#record-edit .form-submit').on('click', function(){
        var $sub = inputValidation($('#record-edit input'));
        if ($sub){
            $record_id = $('#record_id').val();
            $client_id = $('#client_id').val();
            $name = $('#name').val();
            $price = $('#price').val();
            $date = $('#date').val();
            $time = $('#time').val();
            
            $.ajax({ 
                type: 'POST',  
                dataType: 'json',
                url: '/php/api_records.php',
                data: { 
                    'f': 'updateRecord',
                    'record_id': $record_id,
                    'client_id': $client_id,
                    'name': $name,
                    'price': $price,
                    'date': $date,
                    'time': $time
                },
                success: function(data){
                    $('#record-edit .form-response-' + data.result).removeClass('hidden');
                    $('#record-edit .form-response-' + !data.result).addClass('hidden');
                    $('#record-edit .error-required').css('display', 'none');
                },
                error: function(data){
                     alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
                }
            });
        }
    });
    
    $('body').on('click', '#list-records .data-row .drop-menu-item.edit', function(){
        $record_id = $(this).closest('.data-row').attr('record');
        location.href = '/records/edit?record_id=' + $record_id;
    });
    
//    $('#modal-record .order-btn').on('click', function(){
//        $('body').click();
//        var $record_id = $(this).closest('.modal').find('#record_id').val();
//        var $href = '/print/order?record_id=' + $record_id;
//        window.open($href,'_blank'); return false;
//    });
//    
//    $('#modal-record .invoice-btn').on('click', function(){
//        $('body').click();
//        var $record_id = $(this).closest('.modal').find('#record_id').val();
//        var $href = '/print/invoice?record_id=' + $record_id;
//        window.open($href,'_blank'); return false;
//    });
//    
//    $('#modal-record .receipt-btn').on('click', function(){
//        $('body').click();
//        var $record_id = $(this).closest('.modal').find('#record_id').val();
//        var $href = '/print/receipt?record_id=' + $record_id;
//        window.open($href,'_blank'); return false;
//    });
//    
//    $('#modal-record .act-btn').on('click', function(){
//        $('body').click();
//        var $record_id = $(this).closest('.modal').find('#record_id').val();
//        var $href = '/print/act?record_id=' + $record_id;
//        window.open($href,'_blank'); return false;
//    });
    
});
