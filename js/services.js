$(document).ready(function(){
    
     $('body').on('click', '#list-services + #table-info .navigation-btn', function(){
        $('.active-btn').removeClass('active-btn');
        $(this).addClass('active-btn');
        var $filter = $('#filter-services').val();
        var $column = $('.active-sort').attr('column');
        var $order = $('.active-sort').attr('sort');
        var $offset = $('.active-btn').attr('sheet');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api.php',
            data: { 
                'f': 'getFilterServices',
                'filter': $filter,
                'column': $column,
                'order': $order,
                'sheet': $offset,
            },
            success: function(data){
                $('#list-services .data-row').remove();
                $('#list-services').append(data.result);
                $('#table-navigation').html(data.tableNavigation);
                $('#records-info').html(data.tableCounter);
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    $('#filter-services').on('change', function(){
        $('.navigation-btn:first-of-type').click();
    });
    
    $('#service-add .form-submit').on('click', function(e){
        var $sub = inputValidation($('#service-add input'));
        if ($sub){
            var $name = $('#name').val();
            $.ajax({ 
                type: 'POST',  
                dataType: 'json',
                url: '/php/api.php',
                data: { 
                    'f': 'newService',
                    'name': $name,
                },
                success: function(data){
                    $('#service-add .form-response-' + data.result).removeClass('hidden');
                    $('#service-add .form-response-' + !data.result).addClass('hidden');
                    $('#service-add input').val('');
                },
                error: function(data){
                     alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
                }
            });
        }
    });
    
    $('body').on('click', '#list-services .drop-menu .delete', function(){
        var $row = $(this).closest('.data-row');
        var $service = $row.attr('record');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api.php',
            data: { 
                'f': 'deleteService',
                'service_id': $service
            },
            success: function(data){
                $row.remove();
                var $str = $('#records-info').html();
                $str = $.trim($str);
                var $count = $str.charAt($str.length - 1);
                $count = parseInt($count) - 1;
                $str = $str.substring(0, $str.length-1) + $count;
                $('#records-info').html($str);
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    $('#service-edit .form-submit').on('click', function(){
        var $sub = inputValidation($('#service-edit input'));
        if ($sub){
            $service_id = $('#service_id').val();
            $name = $('#name').val();
            $date = $('#date').val();
            
            $.ajax({ 
                type: 'POST',  
                dataType: 'json',
                url: '/php/api.php',
                data: { 
                    'f': 'updateService',
                    'service_id': $service_id,
                    'name': $name,
                    'date': $date
                },
                success: function(data){
                    $('#service-edit .form-response-' + data.result).removeClass('hidden');
                    $('#service-edit .form-response-' + !data.result).addClass('hidden');
                    $('#service-edit .error-required').css('display', 'none');
                },
                error: function(data){
                     alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
                }
            });
        }
    });
    
    $('body').on('click', '#list-services .data-row .drop-menu-item.edit', function(){
        $record_id = $(this).closest('.data-row').attr('record');
        location.href = '/services/edit?service_id=' + $record_id;
    });
    
});