$(document).ready(function(){

    $('#print-document table tr:last-of-type td').on('input', function(){
        $(this).off('focusout');
        $(this).on('focusout', changeLastTr);
    });
    
    $('#act .edit').on('input', act_changeLastTr);
    
    $('#print-documnet-btn').on('click', function(){
        var $check = $('#check').html();
        var check_val = $('.check.total_price').val();
        $('#receipt').html('');
        $('#receipt').css('opacity', '1');
        $('#receipt').append($check);
        $('.receipt.total_price').val(check_val);
        
        $('div[contenteditable="true"]').css('border', '0px');
        $selects = $('select');
        $selects.each(function(){
            var $val = $(this).val();      
            var $html = '<input type="text" value="' + $val + '">';
            $(this).after($html);
            $(this).remove();
        });
        var $trs = $('table:not(.system) tr:not(:first-of-type)');
        $trs.each(function(){
            var $inputs = $(this).find('div');
            var inputLength = $(this).find('div').length;
            var count = 0;
                $inputs.each(function(){
                    var val = $(this).html();
                    val = $.trim(val);
                    if(val.length == 0){
                        count++;
                    }
                });
            if (count == inputLength){
                $(this).remove();
            }
        });
        $.each($('table.hidden_table'), function() {
            var count = $(this).find('tr').length;
            if (count <= 1) {
                var area = $(this).attr('id') + '_area';
                var comment = $(this).attr('id') + '_comment';
                $('#' + area).hide();
                $('#' + comment).hide();
            }
        });
        $('input, select, textarea').addClass('print-ready');
        $(this).hide();
        if($(this).hasClass('order')){
            var html = $('body').html();
            var sum = $('.total_price').val();
            var cause = $('.cause').val();
            var other = $('.other').val();
            html = '<div id="copy" style="page-break-before: always;">' + html + '</div>';
            $('body').append(html);
            $('#copy .client').hide();
            $('#copy .total_price').val(sum);
            $('#copy .cause').text(cause);
            $('#copy .other').text(other);
            html = '<div id="copy2" style="page-break-before: always;">' + html + '</div>';
            $('body').append(html);
            $('#copy2 .client').hide();
            $('#copy2 .for-client').hide();
            $('#copy2 .for-work').show();
            var conf = confirm('Добавить доход в систему?');
            if(conf){
                var $services = $('#order:first .service');
                var service_arr = [];
                $.each($services, function(i){
                    service_arr.push(this.textContent);
                });
                var service_str = service_arr.join(', ');
                $.ajax({ 
                    type: 'POST',  
                    dataType: 'json',
                    url: '/php/api_print.php',
                    data: { 
                        'f': 'addIncomeFromOrder',
                        'record_id': $('#record_id').val(),
                        'name': service_str,
                        'price': sum
                    }
                });
            }
        }
        window.print();
    });
    
    $('body').on('input', '.count, .price', function(){
        var $table_id = $(this).closest('table').attr('id');
        if($(this).hasClass('count')){
            var $count = $(this).html();
            var $price = $(this).closest('tr').find('.price').html();
        }else{
            var $price = $(this).html();
            var $count = $(this).closest('tr').find('.count').html();
        }
        $count = parseFloat($count);
        $price = parseFloat($price)
        if(!isNaN($count) && !isNaN($price)){
            var $sum = $count * $price;
            $(this).closest('tr').find('.sum').html($sum);
            var $inputs_sum = $(this).closest('table').find('.sum');
            var $total_sum = 0;
            $inputs_sum.each(function(){
                var $val = $(this).html();
                $val = parseFloat($val);
                if(!isNaN($val)){
                    $total_sum += $val;
                }
            });
            $('.' + $table_id + '.total_price').val($total_sum);
            $('.act.total_price').text($total_sum);
        }
    });
    
});
    
function changeLastTr(){
        var $tr = $(this).closest('tr');
        var afterTr = $tr.next('tr').length;
        if(afterTr == 0){
            var $html = '<tr>' + $tr.html() + '</tr>';
            $tr.after($html);
            $tr.closest('table').find('td').off('change');
            var $inputs = $tr.next('tr').find("div");
            $inputs.html('');
            $('#print-document table tr:last-of-type td').on('input', function(){
                $(this).off('focusout');
                $(this).on('focusout', changeLastTr);
            });
        }
        var $inputs = $(this).closest('table').find('.number');
        var $total_count = 0;
        $inputs.each(function(){
            var $val = $(this).html();
            $val = $.trim($val);
            if($val.length > 0){
                $total_count += 1;
            }
        });
        $('.total_count').val($total_count);
}

function act_changeLastTr(){
    var $thisTr = $(this).closest('tr');
    var $thisTrHtml = $thisTr.clone();
    var trCount = $thisTr.next('tr').find('.edit').length;
    if(trCount == 0){
        $thisTr.after($thisTrHtml);
        $thisTr.next('tr').find('div').text('');
        $thisTr.next('tr').find('.edit').on('input', act_changeLastTr);
    }
}