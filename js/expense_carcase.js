$(document).ready(function(){
    
     $('body').on('click', '#list-expense + #table-info .navigation-btn', function(){
        $('.active-btn').removeClass('active-btn');
        $(this).addClass('active-btn');
        var $filter = $('#filter-expense').val();
        var $column = $('.active-sort').attr('column');
        var $order = $('.active-sort').attr('sort');
        var $offset = $('.active-btn').attr('sheet');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api.php',
            data: { 
                'f': 'getFilterExpenseCarcase',
                'filter': $filter,
                'column': $column,
                'order': $order,
                'sheet': $offset,
            },
            success: function(data){
                $('#list-expense .data-row').remove();
                $('#list-expense').append(data.result);
                $('#table-navigation').html(data.tableNavigation);
                $('#records-info').html(data.tableCounter);
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    $('#filter-expense').on('change', function(){
        $('.navigation-btn:first-of-type').click();
    });
    
    $('#expense-add .form-submit').on('click', function(e){
        var $sub = inputValidation($('#expense-add input'));
        if ($sub){
            var $name = $('#name').val();
            var $price = $('#price').val();
            var $order = $('#order').val();
            var $date = $('#date').val();
            var $time = $('#time').val();
            $.ajax({ 
                type: 'POST',  
                dataType: 'json',
                url: '/php/api.php',
                data: { 
                    'f': 'newExpenseCarcase',
                    'name': $name,
                    'price': $price,
                    'order': $order,
                    'date': $date,
                    'time': $time,
                },
                success: function(data){
                    $('#expense-add .form-response-' + data.result).removeClass('hidden');
                    $('#expense-add .form-response-' + !data.result).addClass('hidden');
                    $('#expense-add input').val('');
                    $('#expense-add .input-errors-list').children('.error-required').hide('fast');
                    location.href = "/print/expense_carcase?expense_id=" + data.expense_carcase_id;
                },
                error: function(data){
                     alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
                }
            });
        }
    });
    
    $('body').on('click', '#list-expense .drop-menu .delete', function(){
        var $row = $(this).closest('.data-row');
        var $expense = $row.attr('expense');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api.php',
            data: { 
                'f': 'deleteExpenseCarcase',
                'expense_carcase_id': $expense
            },
            success: function(data){
                $row.remove();
                var $str = $('#records-info').html();
                $str = $.trim($str);
                var $count = $str.charAt($str.length - 1);
                $count = parseInt($count) - 1;
                $str = $str.substring(0, $str.length-1) + $count;
                $('#records-info').html($str);
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    $('#expense-edit .form-submit').on('click', function(){
        var $sub = inputValidation($('#expense-edit input'));
        if ($sub){
            $expense_carcase_id = $('#expense_carcase_id').val();
            $name = $('#name').val();
            $price = $('#price').val();
            $order = $('#order').val();
            $date = $('#date').val();
            $time = $('#time').val();
            
            $.ajax({ 
                type: 'POST',  
                dataType: 'json',
                url: '/php/api.php',
                data: { 
                    'f': 'updateExpenseCarcase',
                    'expense_carcase_id': $expense_carcase_id,
                    'name': $name,
                    'price': $price,
                    'order': $order,
                    'date': $date,
                    'time': $time
                },
                success: function(data){
                    $('#expense-edit .form-response-' + data.result).removeClass('hidden');
                    $('#expense-edit .form-response-' + !data.result).addClass('hidden');
                    $('#expense-edit .error-required').css('display', 'none');
                },
                error: function(data){
                     alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
                }
            });
        }
    });
    
    $('body').on('click', '#list-expense .data-row .drop-menu-item.edit', function(){
        $expense_id = $(this).closest('.data-row').attr('expense');
        location.href = '/expense_carcase/edit?expense_id=' + $expense_id;
    });
    
    $('body').on('click', '#list-expense .data-row', function(){
        var $expense_carcase_id = $(this).attr('expense');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api.php',
            data: { 
                'f': 'getExpenseCarcaseInfo',
                'expense_carcase_id': $expense_carcase_id
            },
            success: function(data){
                $('#modal-expense #expense_carcase_id').val(data.expense_carcase_id);
                $('#modal-expense #name').html(data.name);
                $('#modal-expense #price').html(data.price);
                $('#modal-expense #order').html(data.order);
                $('#modal-expense #date').html(data.date + ' ' + data.time);
                modalShow($('#modal-expense'));
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
        
    });
    
    $('#modal-expense #delete-expense').on('click', function(){
        var $expense = $('#expense_carcase_id').val();
        $expense = $.trim($expense);
        if($expense == ""){
            return false;
        }
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api.php',
            data: { 
                'f': 'deleteExpenseCarcase',
                'expense_carcase_id': $expense
            },
            success: function(data){
                $('#wrap').click();
                $('.data-row[expense="' + $expense + '"]').remove();
                var $str = $('#records-info').html();
                $str = $.trim($str);
                var $count = $str.charAt($str.length - 1);
                $count = parseInt($count) - 1;
                $str = $str.substring(0, $str.length-1) + $count;
                $('#records-info').html($str);
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    $('#modal-expense #edit-expense').on('click', function(){
        var $expense_id = $('#expense_carcase_id').val();
        window.location = "/expense_carcase/edit?expense_id=" + $expense_id;
    });
    
    
    
    $('#print-expense').on('click', function(){
        $('body').click();
        var $expense_id = $(this).closest('.modal').find('#expense_carcase_id').val();
        var $href = '/print/expense_carcase?expense_id=' + $expense_id;
        window.open($href,'_blank'); return false;
    });
    
});