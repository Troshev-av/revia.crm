$(document).ready(function(){
    
     $('body').on('click', '#list-sources + #table-info .navigation-btn', function(){
        $('.active-btn').removeClass('active-btn');
        $(this).addClass('active-btn');
        var $filter = $('#filter-sources').val();
        var $column = $('.active-sort').attr('column');
        var $order = $('.active-sort').attr('sort');
        var $offset = $('.active-btn').attr('sheet');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_sources.php',
            data: { 
                'f': 'getFilterSources',
                'filter': $filter,
                'column': $column,
                'order': $order,
                'sheet': $offset,
            },
            success: function(data){
                $('#list-sources .data-row').remove();
                $('#list-sources').append(data.result);
                $('#table-navigation').html(data.tableNavigation);
                $('#records-info').html(data.tableCounter);
            },
            error: function(data){
                alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.');
            }
        });
    });
    
    $('#filter-sources').on('change', function(){
        $('.navigation-btn:first-of-type').click();
    });
    
    $('#sources-add .form-submit').on('click', function(e){
        var $sub = inputValidation($('#sources-add input'));
        if ($sub){
            var $name = $('#name').val();
            var $phone = $('#phone').val();
            var $address = $('#address').val();
            var $comment = $('#comment').val();
            $.ajax({ 
                type: 'POST',  
                dataType: 'json',
                url: '/php/api_sources.php',
                data: { 
                    'f': 'newSource',
                    'name': $name,
                    'phone': $phone,
                    'address': $address,
                    'comment': $comment,
                },
                success: function(data){
                    $('#sources-add .form-response-' + data.result).removeClass('hidden');
                    $('#sources-add .form-response-' + !data.result).addClass('hidden');
                    $('#sources-add input, #sources-add textarea').val('');
                    $('#sources-add .input-errors-list').children('.error-required').hide('fast');
                },
                error: function(data){
                    alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.');
                }
            });
        }
    });
    
    $('body').on('click', '#list-sources .drop-menu .delete', function(){
        var $row = $(this).closest('.data-row');
        var $source = $row.attr('source');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_sources.php',
            data: { 
                'f': 'deleteSource',
                'source_id': $sources
            },
            success: function(data){
                $row.remove();
                var $str = $('#records-info').html();
                $str = $.trim($str);
                var $count = $str.charAt($str.length - 1);
                $count = parseInt($count) - 1;
                $str = $str.substring(0, $str.length-1) + $count;
                $('#records-info').html($str);
            },
            error: function(data){
                alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.');
            }
        });
    });
    
    $('#sources-edit .form-submit').on('click', function(){
        var $sub = inputValidation($('#sources-edit input'));
        if ($sub){
            var $source_id = $('#source_id').val();
            var $name = $('#name').val();
            var $phone = $('#phone').val();
            var $address = $('#address').val();
            var $comment = $('#comment').val();
            
            $.ajax({ 
                type: 'POST',  
                dataType: 'json',
                url: '/php/api_sources.php',
                data: { 
                    'f': 'updateSource',
                    'source_id': $source_id,
                    'name': $name,
                    'phone': $phone,
                    'address': $address,
                    'comment': $comment,
                },
                success: function(data){
                    $('#sources-edit .form-response-' + data.result).removeClass('hidden');
                    $('#sources-edit .form-response-' + !data.result).addClass('hidden');
                    $('#sources-edit .error-required').css('display', 'none');
                },
                error: function(data){
                    alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.');
                }
            });
        }
    });
    
    $('body').on('click', '#list-sources .data-row .drop-menu-item.edit', function(){
        $source_id = $(this).closest('.data-row').attr('source');
        location.href = '/sources/edit?source_id=' + $source_id;
    });
    
    $('body').on('click', '#list-sources .data-row', function(){
        var $source_id = $(this).attr('source');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_sources.php',
            data: { 
                'f': 'getSourceInfo',
                'source_id': $source_id
            },
            success: function(data){
                $('#modal-source #source_id').val(data.source_id);
                $('#modal-source #name').html(data.name);
                $('#modal-source #phone').html(data.phone);
                $('#modal-source #address').html(data.address);
                $('#modal-source #comment').html(data.comment);
                $('#modal-source #date').html(data.date);
                modalShow($('#modal-source'));
            },
            error: function(data){
                console.log(data);
                alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.');
            }
        });
        
    });
    
    $('#modal-source #delete-sources').on('click', function(){
        var $source = $('#source_id').val();
        $source = $.trim($source);
        if($sources == ""){
            return false;
        }
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_sources.php',
            data: { 
                'f': 'deleteSource',
                'source_id': $source
            },
            success: function(data){
                $('#wrap').click();
                $('.data-row[source="' + $source + '"]').remove();
                var $str = $('#records-info').html();
                $str = $.trim($str);
                var $count = $str.charAt($str.length - 1);
                $count = parseInt($count) - 1;
                $str = $str.substring(0, $str.length-1) + $count;
                $('#records-info').html($str);
            },
            error: function(data){
                alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.');
            }
        });
    });
    
    $('#modal-source #edit-sources').on('click', function(){
        var $source_id = $('#source_id').val();
        window.location = "/sources/edit?source_id=" + $source_id;
    });
    
});