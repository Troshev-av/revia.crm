$(document).ready(function(){
    
    $('#client-add .form-submit').on('click', function(e){
        var $sub = inputValidation($('#client-add input'));
        if ($sub){
            var $surname = $('#surname').val();
            var $name = $('#name').val();
            var $phone = $('#phone').val();
            var $email = $('#email').val();
            var $address = $('#address').val();
            var $club_card = $('#club_card').val();
            var $comment = $('#comment').val();
            $.ajax({ 
                type: 'POST',  
                dataType: 'json',
                url: '/php/api_clients.php',
                data: { 
                    'f': 'newClient',
                    'surname': $surname,
                    'name': $name,
                    'phone': $phone,
                    'email': $email,
                    'address': $address,
                    'club_card': $club_card,
                    'comment': $comment,
                },
                success: function(data){
                    $('#client-add .form-response-' + data.result).removeClass('hidden');
                    $('#client-add .form-response-' + !data.result).addClass('hidden');
                    $('#client-add .error-required').css('display', 'none');
                    $('#client-add input').val('');
                    $('#client-add textarea').val('');
                },
                error: function(data){
                     alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
                }
            });
        }
    });
    
    
    $('body').on('click', '#clients + #table-info .navigation-btn', function(){
        $('.active-btn').removeClass('active-btn');
        $(this).addClass('active-btn');
        var $filter = $('#filter-clients').val();
        var $column = $('.active-sort').attr('column');
        var $order = $('.active-sort').attr('sort');
        var $offset = $('.active-btn').attr('sheet');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_clients.php',
            data: { 
                'f': 'getFilterClients',
                'filter': $filter,
                'column': $column,
                'order': $order,
                'sheet': $offset,
            },
            success: function(data){
                $('#clients .data-row').remove();
                $('#clients').append(data.result);
                $('#table-navigation').html(data.tableNavigation);
                $('#records-info').html(data.tableCounter);
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    
    $('#filter-clients').on('change', function(){
        $('.navigation-btn:first-of-type').click();
    });
    
    $('body').on('click', '#clients .drop-menu .delete', function(){
        var $row = $(this).closest('.data-row');
        var $client = $row.attr('client');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_clients.php',
            data: { 
                'f': 'deleteClient',
                'client_id': $client
            },
            success: function(data){
                $row.remove();
                var $str = $('#records-info').html();
                $str = $.trim($str);
                var $count = $str.charAt($str.length - 1);
                $count = parseInt($count) - 1;
                $str = $str.substring(0, $str.length-1) + $count;
                $('#records-info').html($str);
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    $('body').on('click', '#clients .data-row', function(event){
        var $client_id = $(this).attr('client');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_clients.php',
            data: { 
                'f': 'getClientInfo',
                'client_id': $client_id
            },
            success: function(data){
                $('#client_id').val(data.client_id);
                $('#client-records').html(data.records);
                $('#client-tasks').html(data.tasks);
                $('#modal-client #client-id').html(data.client_id);
                $('#modal-client #client-date').html(data.date);
                $('#modal-client #client-surname').html(data.surname);
                $('#modal-client #client-name').html(data.name);
                $('#modal-client #client-phone').html(data.phone);
                $('#modal-client #client-email').html(data.email);
                $('#modal-client #client-address').html(data.address);
                $('#modal-client #club_card').html(data.club_card);
                $('#modal-client #client-comment').html(data.comment);
                $('#client-records-href a').attr('href', '/records/list?client_id=' + data.client_id);
                $('#client-tasks-href a').attr('href', '/tasks?client_id=' + data.client_id);
                modalShow($('#modal-client'));
            },
            error: function(data){
                console.log(data);
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    $('#client-edit .form-submit').on('click', function(e){
        var $sub = inputValidation($('#client-edit input'));
        if ($sub){
            var $client_id = $('#client-edit').attr('client_id');
            var $surname = $('#surname').val();
            var $name = $('#name').val();
            var $phone = $('#phone').val();
            var $email = $('#email').val();
            var $address = $('#address').val();
            var $club_card = $('#club_card').val();
            var $comment = $('#comment').val();
            
            $.ajax({ 
                type: 'POST',  
                dataType: 'json',
                url: '/php/api_clients.php',
                data: { 
                    'f': 'updateClient',
                    'client_id': $client_id,
                    'surname': $surname,
                    'name': $name,
                    'phone': $phone,
                    'email': $email,
                    'address': $address,
                    'club_card': $club_card,
                    'comment': $comment,
                },
                success: function(data){
                    $('#client-edit .form-response-' + data.result).removeClass('hidden');
                    $('#client-edit .form-response-' + !data.result).addClass('hidden');
                    $('#client-edit .error-required').css('display', 'none');
                },
                error: function(data){
                     alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
                }
            });
        }
    });

    $('#client_del').on('click', function(){
        var $client = $('#client_id').val();
        $client = $.trim($client);
        if($client == ""){
            return false;
        }
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_clients.php',
            data: { 
                'f': 'deleteClient',
                'client_id': $client
            },
            success: function(data){
                $('#wrap').click();
                $('.data-row[client="' + $client + '"]').remove();
                var $str = $('#records-info').html();
                $str = $.trim($str);
                var $count = $str.charAt($str.length - 1);
                $count = parseInt($count) - 1;
                $str = $str.substring(0, $str.length-1) + $count;
                $('#records-info').html($str);
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    $('#client_edit').on('click', function(){
        var $client_id = $('#client_id').val();
        window.location = "/clients/edit?client_id=" + $client_id;
    });
    
    $('body').on('click', '#clients .data-row .drop-menu-item.edit', function(){
        $client_id = $(this).closest('.data-row').attr('client');
        location.href = '/clients/edit?client_id=' + $client_id;
    });
    
});