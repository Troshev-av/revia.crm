$(document).ready(function(){
    
     $('body').on('click', '#list-income + #table-info .navigation-btn', function(){
        $('.active-btn').removeClass('active-btn');
        $(this).addClass('active-btn');
        var $filter = $('#filter-income').val();
        var $column = $('.active-sort').attr('column');
        var $order = $('.active-sort').attr('sort');
        var $offset = $('.active-btn').attr('sheet');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_income.php',
            data: { 
                'f': 'getFilterIncome',
                'filter': $filter,
                'column': $column,
                'order': $order,
                'sheet': $offset,
            },
            success: function(data){
                $('#list-income .data-row').remove();
                $('#list-income').append(data.result);
                $('#table-navigation').html(data.tableNavigation);
                $('#records-info').html(data.tableCounter);
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    $('#filter-income').on('change', function(){
        $('.navigation-btn:first-of-type').click();
    });
    
    $('#income-edit #client').on('keyup', function(){
        $('#client').val('').off('keyup');
        $('#client_id').val('');
    });
    
    $('#client').autocomplete({
        source: function(request, response){
            $.ajax({
                type: 'POST', 
                global: false,
                dataType: 'json',
                url: '/php/api_income.php',
                data: { 'f': 'clientSearch', 'str': request.term},
                success: function(data){
                    $('#client_id').val('');
                    response($.map(data, function(item){
                        return {
                            id: item.value,
                            label: item.label
                        }
                    }));
                }
            });
        },
        select: function( event, ui ) {
            $('#client_id').val(ui.item.id);
            $('#client').off('keyup').on('keyup', function(){
                $('#client').val('').off('keyup');
                $('#client_id').val('');
            });
        },
        minLength: 1,// начинать поиск с трех
        appendTo: '#client_area',
        position: { my : "left top", at: "left bottom" }
    });
    
    $('#income-add .form-submit').on('click', function(e){
        var $sub = inputValidation($('#income-add input'));
        if ($sub){
            var $client_id = $('#client_id').val();
            var $name = $('#name').val();
            var $price = $('#price').val();
            var $date = $('#date').val();
            var $time = $('#time').val();
            $.ajax({ 
                type: 'POST',  
                dataType: 'json',
                url: '/php/api_income.php',
                data: { 
                    'f': 'newIncome',
                    'client_id': $client_id,
                    'name': $name,
                    'price': $price,
                    'date': $date,
                    'time': $time,
                },
                success: function(data){
                    $('#income-add .form-response-' + data.result).removeClass('hidden');
                    $('#income-add .form-response-' + !data.result).addClass('hidden');
                    $('#income-add input').val('');
                    $('#income-add .input-errors-list').children('.error-required').hide('fast');
                },
                error: function(data){
                     alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
                }
            });
        }
    });
    
    $('body').on('click', '#list-income .drop-menu .delete', function(){
        var $row = $(this).closest('.data-row');
        var $income = $row.attr('income');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_income.php',
            data: { 
                'f': 'deleteIncome',
                'income_id': $income
            },
            success: function(data){
                $row.remove();
                var $str = $('#records-info').html();
                $str = $.trim($str);
                var $count = $str.charAt($str.length - 1);
                $count = parseInt($count) - 1;
                $str = $str.substring(0, $str.length-1) + $count;
                $('#records-info').html($str);
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    $('#income-edit .form-submit').on('click', function(){
        var $sub = inputValidation($('#income-edit input'));
        if ($sub){
            $income_id = $('#income_id').val();
            $name = $('#name').val();
            $client_id = $('#client_id').val();
            $price = $('#price').val();
            $date = $('#date').val();
            $time = $('#time').val();
            
            $.ajax({ 
                type: 'POST',  
                dataType: 'json',
                url: '/php/api_income.php',
                data: { 
                    'f': 'updateIncome',
                    'income_id': $income_id,
                    'client_id': $client_id,
                    'name': $name,
                    'price': $price,
                    'date': $date,
                    'time': $time
                },
                success: function(data){
                    $('#income-edit .form-response-' + data.result).removeClass('hidden');
                    $('#income-edit .form-response-' + !data.result).addClass('hidden');
                    $('#income-edit .error-required').css('display', 'none');
                },
                error: function(data){
                    console.log(data);
                     alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
                }
            });
        }
    });
    
    $('body').on('click', '#list-income .data-row .drop-menu-item.edit', function(){
        $income_id = $(this).closest('.data-row').attr('income');
        location.href = '/income/edit?income_id=' + $income_id;
    });
    
    $('body').on('click', '#list-income .data-row', function(){
        var $income_id = $(this).attr('income');
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_income.php',
            data: { 
                'f': 'getIncomeInfo',
                'income_id': $income_id
            },
            success: function(data){
                $('#modal-income #income_id').val(data.income_id);
                $('#modal-income #client-surname a').html(data.client);
                $('#modal-income #client-surname a').attr('href', '/clients?client_id=' + data.client_id);
                $('#modal-income #name').html(data.name);
                $('#modal-income #price').html(data.price);
                $('#modal-income #date').html(data.date + ' ' + data.time);
                modalShow($('#modal-income'));
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
        
    });
    
    $('#modal-income #delete-income').on('click', function(){
        var $income = $('#income_id').val();
        $income = $.trim($income);
        if($income == ""){
            return false;
        }
        $.ajax({ 
            type: 'POST',  
            dataType: 'json',
            url: '/php/api_income.php',
            data: { 
                'f': 'deleteIncome',
                'income_id': $income
            },
            success: function(data){
                $('#wrap').click();
                $('.data-row[income="' + $income + '"]').remove();
                var $str = $('#records-info').html();
                $str = $.trim($str);
                var $count = $str.charAt($str.length - 1);
                $count = parseInt($count) - 1;
                $str = $str.substring(0, $str.length-1) + $count;
                $('#records-info').html($str);
            },
            error: function(data){
                 alert('Произошла ошибка, перезагрузите страницу и попробуйте еще раз.')
            }
        });
    });
    
    $('#modal-income #edit-income').on('click', function(){
        var $income_id = $('#income_id').val();
        window.location = "/income/edit?income_id=" + $income_id;
    });
    
});