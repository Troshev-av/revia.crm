<?php
header('Content-Type: text/html; charset=utf-8');
include('html.php');
include('sql.php');
include('../application/models/model_income.php');

$return = Array();  

switch ($_POST['f']) {
    
    case "updateIncome":
        $return['result'] = updateIncome($_POST['income_id'], $_POST['client_id'], $_POST['name'], $_POST['price'], $_POST['date'], $_POST['time']);
    break;
        
    case "newIncome":
        $return['result'] = addIncome($_POST['client_id'], $_POST['name'], $_POST['price'], $_POST['date'], $_POST['time']);
    break;
        
    case "getFilterIncome":
        $result = getFilterIncome($_POST['sheet'], $_POST['column'], $_POST['order'], $_POST['filter']);
        $incomeRows = "";
        while($item = mysql_fetch_assoc($result)){
            $incomeRows = $incomeRows.incomeRow($item['income_id'], $item['client'], $item['name'], $item['price'], $item['date']);
        }
        $return['result'] = $incomeRows;
        if($_POST['filter'] == ''){
            $incomeCount = getCountIncome();
            $incomeCount = mysql_fetch_row($incomeCount);
            $return['tableNavigation'] = tableNavigation($incomeCount[0], $_POST['sheet']);
            $return['tableCounter'] = 'Всего записей в таблице: '.$incomeCount[0];
        }else{
            $return['tableNavigation'] = tableNavigation(mysql_num_rows($result), $_POST['sheet']);
            $return['tableCounter'] = 'Найдено записей: '.mysql_num_rows($result);
        }
    break;
    
    case "deleteIncome":
        $return['result'] = deleteIncome($_POST['income_id']);    
    break;
        
    case "getIncomeInfo":
        $records = getIncome($_POST["income_id"]);
        $return = mysql_fetch_assoc($records);
    break;
        
    case "clientSearch":
        $clients = clientSearch($_POST['str']);
        $clients_arr = array();
        while($item = mysql_fetch_assoc($clients)){
            $item_arr = array();
            $item_arr['label'] = $item['name'];
            $item_arr['value'] = $item['client_id'];
            array_push($clients_arr, $item_arr);
        }
        $return = $clients_arr;
    break;
    
}

echo json_encode($return);

?>