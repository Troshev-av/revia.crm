<?php

function emptyResult(){
    $li = '<li class="autocomplete-item disabled">Пустой результат</li>';
    return $li;
}

function clientSearchHtml($client_id, $name){
    $li = '<li client='.$client_id.' class="autocomplete-item">'.$name.'</li>';
    return $li;
}

function clientRow($client_id, $surname, $name, $phone, $email, $address, $date){
    $clientRow = '<div class="row data-row" client="'.$client_id.'">
        <div class="cell table-client_id">'.$client_id.'</div>
        <div class="cell table-surname">'.$surname.' '.$name.'</div>
        <div class="cell table-phone">'.$phone.'</div>
        <div class="cell table-email">'.$email.'</div>
        <div class="cell table-address">'.$address.'</div>
        <div class="cell table-date">'.$date.'</div>
        <div class="cell">
            <div class="drop-menu">
                <ul class="drop-menu-list">
                    <li class="drop-menu-item edit">Редактировать</li>
                    <li class="drop-menu-item delete">Удалить</li>
                </ul>
            </div>
        </div>
    </div>';
    return $clientRow;
}

function clientRecordsHtml($time, $date, $name, $price){
    $item = '<div class="record">
                <div class="record-info">
                    <div class="record-cost">'.$price.'</div>
                    '.$name.'
                    <div class="record-car">'.$date.' '.$time.'</div>
                </div>
            </div>';
    return $item;
}

function clientTasksHtml($time, $date, $name, $client){
    $item = '<div class="task">
                <div class="task-client">'.$client.'</div>
                <div class="task-info">
                    '.$name.'
                    <div class="task-date">'.$date.' '.$time.'</div>
                </div>
            </div>';
    return $item;
}

function tableNavigation($recordCount, $sheet){
    $tableNavigation = ''; 
    if ($recordCount == 0){
        $tableNavigation = '<div class="navigation-btn hidden active-btn" sheet="0">1</div>';
    }else{
        $countStr = ceil(intval($recordCount) / 20);
        for($i = 1; $i <= $countStr; $i++){
            $tableNavigation = $tableNavigation.'<div class="navigation-btn';
            if($i == $sheet + 1){
                $tableNavigation = $tableNavigation.' active-btn';
            }
                $tableNavigation = $tableNavigation.'" sheet="'.($i-1).'">'.$i.'</div>';
        }
    }
    
    return $tableNavigation;
}

function getCalendar($month, $year){
    $day_count = date('t', mktime(0, 0, 0, $month, 1, $year));
    $sart_weekday = date('N', mktime(0, 0, 0, $month, 1, $year));
    $finish_weekday = date('N', mktime(0, 0, 0, $month, $day_count, $year));
    $weekday = 1;
    $return_html = '<div class="row">';
    for($i = 1; $i < $sart_weekday; $i++){
        $return_html = $return_html.'<div class="cell locked"></div>';
        $weekday++;
    }
    for($i = 1; $i <= $day_count; $i++){
        if ($month == date('m') && $year == date('Y') && date('d') == $i){
            $return_html = $return_html.'<div class="cell today"><div class="day_number">'.$i.'</div>';
        }else{
            $return_html = $return_html.'<div class="cell"><div class="day_number">'.$i.'</div>';
        }
        $day = ($i < 10) ? '0'.$i : $i;
        $records = getRecordFromDate($day,$month, $year);
        $recordsCount = mysql_num_rows($records);
        while($record = mysql_fetch_assoc($records)){
            $return_html = $return_html.'<a class="record-href" record="'.$record['record_id'].'" cleint="'.$record['client_id'].'">'.$record['surname'].' '.$record['name'].'</a>';
        }
        if($recordsCount > 0){
            $return_html .= '<div class="records-count">'.$recordsCount.'</div>';
        }
        
        $return_html = $return_html.'</div>';
        $weekday++;
        if($weekday / 8 == 1 && $i != $day_count){
            $return_html = $return_html.'</div><div class="row">';
            $weekday = 1;
        }
    }
    for($i = $finish_weekday; $i < 7; $i++){
        $return_html = $return_html.'<div class="cell locked"></div>';
    }
    
    $return_html = $return_html.'</div>';
    $return_html = $return_html.$recordCount;
    return $return_html;
}

function recordRows($record_id, $client, $name, $address, $price, $date, $time){
    $recordRow = '
    <div class="row data-row" record="'.$record_id.'">
        <div class="cell table-record_id">'.$record_id.'</div>
        <div class="cell table-client">'.$client.'</div>
        <div class="cell table-name">'.$name.'</div>
        <div class="cell table-address">'.$address.'</div>
        <div class="cell table-price">'.$price.'</div>
        <div class="cell table-date">'.$date.' '.$time.'</div>
        <div class="cell">
            <div class="drop-menu">
                <ul class="drop-menu-list">
                    <li class="drop-menu-item edit">Редактировать</li>
                    <li class="drop-menu-item delete">Удалить</li>
                </ul>
            </div>
        </div>
    </div>';
    return $recordRow;
}

function taskRow($task_id, $client, $name, $date, $time){
    $recordRow = '
    <div class="row data-row" task="'.$task_id.'">
        <div class="cell table-task_id">'.$task_id.'</div>
        <div class="cell table-client">'.$client.'</div>
        <div class="cell table-name">'.$name.'</div>
        <div class="cell table-date">'.$date.' '.$time.'</div>
        <div class="cell">
            <div class="drop-menu">
                <ul class="drop-menu-list">
                    <li class="drop-menu-item edit">Редактировать</li>
                    <li class="drop-menu-item delete">Удалить</li>
                </ul>
            </div>
        </div>
    </div>';
    return $recordRow;
}

function incomeRow($income_id, $client, $name, $price, $date){
    $recordRow = '
    <div class="row data-row" income="'.$income_id.'">
        <div class="cell table-income_id">'.$income_id.'</div>
        <div class="cell table-client">'.$client.'</div>
        <div class="cell table-name">'.$name.'</div>
        <div class="cell table-price">'.$price.'</div>
        <div class="cell table-date">'.$date.'</div>
        <div class="cell">
            <div class="drop-menu">
                <ul class="drop-menu-list">
                    <li class="drop-menu-item edit">Редактировать</li>
                    <li class="drop-menu-item delete">Удалить</li>
                </ul>
            </div>
        </div>
    </div>';
    return $recordRow;
}

function expenseRow($expense_id, $name, $price, $order, $date, $time){
    $recordRow = '
    <div class="row data-row" expense="'.$expense_id.'">
        <div class="cell table-expense_id">'.$expense_id.'</div>
        <div class="cell table-name">'.$name.'</div>
        <div class="cell table-price">'.$price.'</div>
        <div class="cell table-order">'.$order.'</div>
        <div class="cell table-date">'.$date.' '.$time.'</div>
        <div class="cell ">
            <div class="drop-menu">
                <ul class="drop-menu-list">
                    <li class="drop-menu-item edit">Редактировать</li>
                    <li class="drop-menu-item delete">Удалить</li>
                </ul>
            </div>
        </div>
    </div>';
    return $recordRow;
}

function sourceRow($source_id, $name, $phone, $address, $date){
    $recordRow = '
    <div class="row data-row" source="'.$source_id.'">
        <div class="cell table-source_id">'.$source_id.'</div>
        <div class="cell table-name">'.$name.'</div>
        <div class="cell table-price">'.$phone.'</div>
        <div class="cell table-order">'.$address.'</div>
        <div class="cell table-order">'.$date.'</div>
        <div class="cell ">
            <div class="drop-menu">
                <ul class="drop-menu-list">
                    <li class="drop-menu-item edit">Редактировать</li>
                    <li class="drop-menu-item delete">Удалить</li>
                </ul>
            </div>
        </div>
    </div>';
    return $recordRow;
}

function notificationRecord($item){
    $html = '
        <div id="'.$item['notification_id'].'" class="record-notification notification">
            <div class="record-date">
                <div class="record-time">'.$item['time'].'</div>
                '.$item['date'].'
            </div>
            <div class="record-info">
                <div class="record-cost">'.$item['price'].'</div>
                '.$item['service'].'
                <div class="record-car">'.$item['car'].'</div>
            </div>
        </div>
    ';
    return $html;
}

function notificationTask($item){
    $html = '
        <div id="'.$item['notification_id'].'" class="task-notification notification" task="'.$item['task_id'].'">
            <div class="task-date">
                <div class="task-time">'.$item['time'].'</div>
                '.$item['date'].'
            </div>
            <div class="task-info">
                '.$item['name'].'
                <div class="task-client">'.$item['client'].'</div>
            </div>
        </div>
    ';
    return $html;
}

?>