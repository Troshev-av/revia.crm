<?php
header('Content-Type: text/html; charset=utf-8');
include('html.php');
include('sql.php');
include('../application/models/model_records.php');

$return = Array();  

switch ($_POST['f']) {
        
    case "newRecord":
        $return['result'] = addRecord($_POST['client_id'], $_POST['name'], $_POST['price'], $_POST['date'], $_POST['time']);
    break;
    
    case "updateRecord":
        $return['result'] = updateRecord($_POST['record_id'], $_POST['client_id'], $_POST['name'], $_POST['price'], $_POST['date'], $_POST['time']);
    break;

    case "getRecordInfo":
        $records = getRecord($_POST["record_id"]);
        $return = mysql_fetch_assoc($records);
    break;
    
    case "getFilterRecords":
        $result = getFilterRecords($_POST['sheet'], $_POST['column'], $_POST['order'], $_POST['filter']);
        $recordRows = "";
        while($item = mysql_fetch_assoc($result)){
            $recordRows = $recordRows.recordRows($item['record_id'], $item['client'], $item['name'], $item['address'], $item['price'], $item['date'], $item['time']);
        }
        $return['result'] = $recordRows;
        if($_POST['filter'] == ''){
            $recordCount = getCountRecords();
            $recordCount = mysql_fetch_row($recordCount);
            $return['tableNavigation'] = tableNavigation($recordCount[0], $_POST['sheet']);
            $return['tableCounter'] = 'Всего записей: '.$recordCount[0];
        }else{
            $return['tableNavigation'] = tableNavigation(mysql_num_rows($result), $_POST['sheet']);
            $return['tableCounter'] = 'Найдено записей: '.mysql_num_rows($result);
        }
    break;
    
    case "deleteRecord":
        $return['result'] = deleteRecord($_POST['record_id']);    
    break;
        
    case "clientSearch":
        $clients = clientSearch($_POST['str']);
        $clients_arr = array();
        while($item = mysql_fetch_assoc($clients)){
            $item_arr = array();
            $item_arr['label'] = $item['name'];
            $item_arr['value'] = $item['client_id'];
            array_push($clients_arr, $item_arr);
        }
        $return = $clients_arr;
    break;

}

echo json_encode($return);

?>