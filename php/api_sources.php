<?php
header('Content-Type: text/html; charset=utf-8');
include('html.php');
include('sql.php');
include('../application/models/model_sources.php');

$return = Array();  

switch ($_POST['f']) {
        
    case "newSource":
        $return['result'] = addSource($_POST['name'], $_POST['phone'], $_POST['address'], $_POST['comment']);
    break;
        
    case "getSourceInfo":
        $source = getSource($_POST['source_id']);
        $return = mysql_fetch_assoc($source);
    break;
        
    case "getFilterSources":
        $result = getFilterSources($_POST['sheet'], $_POST['column'], $_POST['order'], $_POST['filter']);
        $sourceRows = "";
        while($item = mysql_fetch_assoc($result)){
            $sourceRows = $sourceRows.sourceRow($item['source_id'], $item['name'], $item['phone'], $item['address'], $item['date']);
        }
        $return['result'] = $sourceRows;
        if($_POST['filter'] == ''){
            $sourceCount = getCountSources();
            $sourceCount = mysql_fetch_row($sourceCount);
            $return['tableNavigation'] = tableNavigation($sourceCount[0], $_POST['sheet']);
            $return['tableCounter'] = 'Всего записей в таблице: '.$sourceCount[0];
        }else{
            $return['tableNavigation'] = tableNavigation(mysql_num_rows($result), $_POST['sheet']);
            $return['tableCounter'] = 'Найдено записей: '.mysql_num_rows($result);
        }
    break;
        
    case "updateSource":
        $return['result'] = updateSource($_POST['source_id'], $_POST['name'], $_POST['phone'], $_POST['address'], $_POST['comment']);
    break;
    
    case "deleteSource":
        $return['result'] = deleteSource($_POST['source_id']);    
    break;
    
}

echo json_encode($return);

?>