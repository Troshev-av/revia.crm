<?php
header('Content-Type: text/html; charset=utf-8');
include('html.php');
include('sql.php');
include('../application/models/model_tasks.php');

$return = Array();  

switch ($_POST['f']) {
    
    case "newTask":
        $return['result'] = addTask($_POST['client_id'], $_POST['name'], $_POST['date'], $_POST['time']);
    break;
        
    case "getFilterTasks":
        $result = getFilterTasks($_POST['sheet'], $_POST['column'], $_POST['order'], $_POST['filter']);
        $taskRows = "";
        while($item = mysql_fetch_assoc($result)){
            $taskRows = $taskRows.taskRow($item['task_id'], $item['client'], $item['name'], $item['date'], $item['time']);
        }
        $return['result'] = $taskRows;
        if($_POST['filter'] == ''){
            $taskCount = getCountTasks();
            $taskCount = mysql_fetch_row($taskCount);
            $return['tableNavigation'] = tableNavigation($taskCount[0], $_POST['sheet']);
            $return['tableCounter'] = 'Всего задач: '.$taskCount[0];
        }else{
            $return['tableNavigation'] = tableNavigation(mysql_num_rows($result), $_POST['sheet']);
            $return['tableCounter'] = 'Найдено задач: '.mysql_num_rows($result);
        }
    break;
    
    case "updateTask":
        $return['result'] = updateTask($_POST['task_id'], $_POST['client_id'], $_POST['name'], $_POST['date'], $_POST['time']);
    break;
    
    case "deleteTask":
        $return['result'] = deleteTask($_POST['task_id']);    
    break;
    
    case "getTaskInfo":
        $records = getTask($_POST["task_id"]);
        $return = mysql_fetch_assoc($records);
    break;
        
    case "clientSearch":
        $clients = clientSearch($_POST['str']);
        $clients_arr = array();
        while($item = mysql_fetch_assoc($clients)){
            $item_arr = array();
            $item_arr['label'] = $item['name'];
            $item_arr['value'] = $item['client_id'];
            array_push($clients_arr, $item_arr);
        }
        $return = $clients_arr;
    break;
    
}

echo json_encode($return);

?>