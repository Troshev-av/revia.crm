<?php

include('sql_settings.php');

function getUser($login){
    $query = "SELECT `user_id`, `password` FROM `users` WHERE NOT `is_del` = 1  AND `login` = '".$login."'";
    $result = mysql_query($query);
    return mysql_fetch_assoc($result);
}

function getUserInfo($user_id){
    $query = "SELECT `users`.`name` as user_name, `user_types`.`name` as user_type
              FROM `user_types`
              LEFT JOIN `users` ON `user_types`.`user_type_id` = `users`.`user_type_id`
              WHERE NOT `users`.`is_del` = 1 AND `users`.`user_id` = ".$user_id;
    $result = mysql_query($query);
    return mysql_fetch_assoc($result);
}

function getUserAllAccess($user_id){
    $query = "SELECT `user_types`.*
              FROM `user_types`
              LEFT JOIN `users` ON `user_types`.`user_type_id` = `users`.`user_type_id`
              WHERE NOT `users`.`is_del` = 1 AND `users`.`user_id` = ".$user_id;
    $result = mysql_query($query);
    return mysql_fetch_assoc($result);
}

function getUserAccess($user_id, $page){
    $query = "SELECT `start_page`, `".$page."`
              FROM `user_types`
              LEFT JOIN `users` ON `user_types`.`user_type_id` = `users`.`user_type_id`
              WHERE NOT `users`.`is_del` = 1 AND `users`.`user_id` = ".$user_id;
    $result = mysql_query($query);
    return mysql_fetch_assoc($result);
}

function insertNewNotifications(){
    $query = "
        INSERT INTO `notifications` (`type`, `item_id`)
            SELECT 'record', `records`.`record_id`
            FROM `records`
            LEFT JOIN `notifications` ON `records`.`record_id` = `notifications`.`item_id` AND `notifications`.`type` = 'record'
            WHERE `notifications`.`item_id` IS NULL AND `records`.`is_del` = 0 
        UNION
            SELECT 'task', `tasks`.`task_id`
            FROM `tasks`
            LEFT JOIN `notifications` ON `tasks`.`task_id` = `notifications`.`item_id` AND `notifications`.`type` = 'task'
            WHERE `notifications`.`item_id` IS NULL AND `tasks`.`is_del` = 0";
    $result = mysql_query($query);
    return $result;  
}

function getNewTasksNotifications($hide_old){
    $date = date('d.m.Y', strtotime(" +15 min"));
    $time = date('H:i', strtotime(" +15 min"));
    $query_add = '';
    if($hide_old){
        $query_add = 'AND `notifications`.`is_show` = 0';
    }
    $query = "
        SELECT `tasks`.*, CONCAT(`clients`.`surname`, ', ', `clients`.`name`) AS `client`, `notifications`.`notification_id`
        FROM 
        `tasks`
        LEFT JOIN `clients` ON `tasks`.`client_id` = `clients`.`client_id`
        LEFT JOIN `notifications` ON `tasks`.`task_id` = `notifications`.`item_id`
        WHERE `tasks`.`is_del` = 0 AND `notifications`.`is_del` = 0 AND `notifications`.`type` = 'task' AND  (STR_TO_DATE(`tasks`.`date`,'%d.%m.%Y') < STR_TO_DATE('".$date."','%d.%m.%Y') OR  (STR_TO_DATE(`tasks`.`date`,'%d.%m.%Y') = STR_TO_DATE('".$date."','%d.%m.%Y') AND `tasks`.`time` < '".$time."')) ".$query_add."
        ORDER BY STR_TO_DATE(`tasks`.`date`,'%d.%m.%Y') DESC, `tasks`.`time` DESC
    ";
    $query = "
        SELECT `tasks`.`task_id`, 
            `tasks`.`name`, 
            `tasks`.`date`, 
            `tasks`.`time`,  
           CONCAT(`clients`.`surname`, ', ', `clients`.`name`) AS `client`, 
           `notifications`.`notification_id`
        FROM `tasks`
        LEFT JOIN `clients` ON `tasks`.`client_id` = `clients`.`client_id`
        LEFT JOIN `notifications` ON `tasks`.`task_id` = `notifications`.`item_id` AND `notifications`.`type` = 'task'
        WHERE `tasks`.`is_del` = 0 
            AND `notifications`.`is_del` = 0 
            AND  (STR_TO_DATE(`tasks`.`date`,'%d.%m.%Y') < STR_TO_DATE('".$date."','%d.%m.%Y') 
                OR  (STR_TO_DATE(`tasks`.`date`,'%d.%m.%Y') = STR_TO_DATE('".$date."','%d.%m.%Y') 
                AND `tasks`.`time` < '".$time."')) 
            ".$query_add."
        ORDER BY STR_TO_DATE(`tasks`.`date`,'%d.%m.%Y') DESC, `tasks`.`time` DESC";
    $result = mysql_query($query);
    return $result;  
}

function getNewRecordsNotification($hide_old){
    $date = date('d.m.Y', strtotime(" +15 min"));
    $time = date('H:i', strtotime(" +15 min"));
    if($hide_old){
        $query_add = 'AND `notifications`.`is_show` = 0';
    }
    $query = "
        SELECT `records`.`record_id`, 
            `records`.`name` as `service`, 
            `records`.`price`,
            `records`.`date`,  
            `records`.`time`, 
            CONCAT(`clients`.`surname`, ' ', `clients`.`name`) AS `client`, 
            `notifications`.`notification_id`
        FROM `records`
        LEFT JOIN `clients` ON `records`.`client_id` = `clients`.`client_id`
        LEFT JOIN `notifications` ON `records`.`record_id` = `notifications`.`item_id` AND `notifications`.`type` = 'record'
        WHERE `records`.`is_del` = 0 
            AND `notifications`.`is_del` = 0 
            AND (STR_TO_DATE(`records`.`date`,'%d.%m.%Y') < STR_TO_DATE('".$date."','%d.%m.%Y') 
                OR  (STR_TO_DATE(`records`.`date`,'%d.%m.%Y') = STR_TO_DATE('".$date."','%d.%m.%Y') 
                AND `records`.`time` < '".$time."')) 
            ".$query_add."
        GROUP BY `records`.`record_id`
        ORDER BY STR_TO_DATE(`records`.`date`,'%d.%m.%Y') DESC, `records`.`time` DESC";
    $result = mysql_query($query);
    return $result;  
}

function deleteNotification($notify_id){
    $query = "
        UPDATE `notifications`
        SET `is_del` = 1
        WHERE `notification_id` = ".$notify_id;
    $result = mysql_query($query);
    return $result;  
}

function hideNewNotification($notify_id){
    $query = "
        UPDATE `notifications`
        SET `is_show` = 1
        WHERE `notification_id` = ".$notify_id;
    $result = mysql_query($query);
    return $result;  
}

function showNotification($type, $item_id){
    $query = "
        UPDATE `notifications`
        SET `is_show` = 0,
            `is_del` = 0
        WHERE `type` = '".$type."' AND `item_id` = ".$item_id;
    $result = mysql_query($query);
    return $result; 
}

?>