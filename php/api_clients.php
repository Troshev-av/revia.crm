<?php
header('Content-Type: text/html; charset=utf-8');
include('html.php');
include('sql.php');
include('../application/models/model_clients.php');

$return = Array();  

switch ($_POST['f']) {
    case "newClient":
        $return['result'] = addClient($_POST['surname'], $_POST['name'], $_POST['phone'], $_POST['email'],  $_POST['address'], $_POST['comment'], $_POST['club_card']);
    break;
    
    case "updateClient":
        $return['result'] = updateClient($_POST['client_id'], $_POST['surname'], $_POST['name'], $_POST['phone'], $_POST['email'], $_POST['address'], $_POST['comment'], $_POST['club_card']);
    break;
    
    case "getFilterClients":
        $result = getFilterClients($_POST['sheet'], $_POST['column'], $_POST['order'], $_POST['filter']);
        $client = Array();
        $clientRows = "";
        while($item = mysql_fetch_assoc($result)){
            $clientRows = $clientRows.clientRow($item['client_id'], $item['surname'], $item['name'], $item['phone'], $item['email'], $item['address'], $item['date']);
        }
        $return['result'] = $clientRows;
        if($_POST['filter'] == ''){
            $clientCount = getCountClients();
            $clientCount = mysql_fetch_row($clientCount);
            $return['tableNavigation'] = tableNavigation($clientCount[0], $_POST['sheet']);
            $return['tableCounter'] = 'Всего клиентов: '.$clientCount[0];
        }else{
            $return['tableNavigation'] = tableNavigation(mysql_num_rows($result), $_POST['sheet']);
            $return['tableCounter'] = 'Найдено клиентов: '.mysql_num_rows($result);
        }
    break;
    
    case "getClientInfo":
        $clientInfo = getClientInfo($_POST["client_id"]);
        $return = mysql_fetch_assoc($clientInfo);
        $records = getClientRecords($_POST["client_id"]);
        $records_html = "";
        if(mysql_num_rows($records) == 0){
            $return['records'] = "Нет ближайших записей";
        }else{
            while($item = mysql_fetch_assoc($records)){
               $records_html = $records_html.clientRecordsHtml($item['time'], $item['date'], $item['name'], $item['price']);
            }    
            $return['records'] = $records_html;
        }
        $tasks = getClientTasks($_POST["client_id"]);
        if(mysql_num_rows($tasks) == 0){
            $return['tasks'] = "Нет активных задач";
        }else{
            while($item = mysql_fetch_assoc($tasks)){
               $tasks_html = $tasks_html.clientTasksHtml($item['time'], $item['date'], $item['name'], $item['name']);
            }    
            $return['tasks'] = $tasks_html;
        }
    break;
    
    case "deleteClient":
        $return['result'] = deleteClient($_POST['client_id']);    
    break;
}

echo json_encode($return);

?>