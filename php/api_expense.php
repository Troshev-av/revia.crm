<?php
header('Content-Type: text/html; charset=utf-8');
include('html.php');
include('sql.php');
include('../application/models/model_expense.php');

$return = Array();  

switch ($_POST['f']) {
        
    case "newExpense":
        $return = addExpense($_POST['name'], $_POST['price'], $_POST['order'], $_POST['date'], $_POST['time']);
    break;
        
    case "getFilterExpense":
        $result = getFilterExpense($_POST['sheet'], $_POST['column'], $_POST['order'], $_POST['filter']);
        $expenseRows = "";
        while($item = mysql_fetch_assoc($result)){
            $expenseRows = $expenseRows.expenseRow($item['expense_id'], $item['name'], $item['price'], $item['order'], $item['date'], $item['time']);
        }
        $return['result'] = $expenseRows;
        if($_POST['filter'] == ''){
            $expenseCount = getCountExpense();
            $expenseCount = mysql_fetch_row($expenseCount);
            $return['tableNavigation'] = tableNavigation($expenseCount[0], $_POST['sheet']);
            $return['tableCounter'] = 'Всего записей в таблице: '.$expenseCount[0];
        }else{
            $return['tableNavigation'] = tableNavigation(mysql_num_rows($result), $_POST['sheet']);
            $return['tableCounter'] = 'Найдено записей: '.mysql_num_rows($result);
        }
    break;
    
    case "getExpenseInfo":
        $records = getExpense($_POST['expense_id']);
        $return = mysql_fetch_assoc($records);
    break;
    
    case "updateExpense":
        $return['result'] = updateExpense($_POST['expense_id'], $_POST['name'], $_POST['price'], $_POST['order'], $_POST['date'], $_POST['time']);
    break;
    
    case "deleteExpense":
        $return['result'] = deleteExpense($_POST['expense_id']);    
    break;
}

echo json_encode($return);

?>