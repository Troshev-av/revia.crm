<?php

class Controller_Tasks extends Controller
{
	function action_index()
	{	
		$this->view->generate('taskList_view.php', 'template_view.php');
	}
    
    function action_add()
	{	
		$this->view->generate('taskAdd_view.php', 'template_view.php');
	}
    
    function action_edit()
	{	
		$this->view->generate('taskEdit_view.php', 'template_view.php');
	}
}