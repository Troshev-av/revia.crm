<?php

class Controller_Clients extends Controller
{

	function action_index()
	{	
		$this->view->generate('clientList_view.php', 'template_view.php');
	}
    
    function action_add()
	{	
		$this->view->generate('clientAdd_view.php', 'template_view.php');
	}
    
    function action_edit()
	{	
        if (isset($_GET['client_id'])){
            $this->view->generate('clientEdit_view.php', 'template_view.php');
        } else {
            header('Location: /');
        }
	}
}