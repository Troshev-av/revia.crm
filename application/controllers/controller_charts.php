<?php

class Controller_Charts extends Controller
{

	function action_index()
	{	
		$this->view->generate('charts_view.php', 'template_view.php');
	}

	function action_clients()
	{	
		$this->view->generate('chartsClients_view.php', 'template_view.php');
	}

	function action_records()
	{	
		$this->view->generate('chartsRecords_view.php', 'template_view.php');
	}

	function action_tasks()
	{	
		$this->view->generate('chartsTasks_view.php', 'template_view.php');
	}

	function action_income()
	{	
		$this->view->generate('chartsIncome_view.php', 'template_view.php');
	}

	function action_expense()
	{	
		$this->view->generate('chartsExpense_view.php', 'template_view.php');
	}

	function action_profit()
	{	
		$this->view->generate('chartsProfit_view.php', 'template_view.php');
	}
}