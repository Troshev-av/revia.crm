<?php

class Controller_Income extends Controller
{

	function action_index()
	{	
		$this->view->generate('incomeList_view.php', 'template_view.php');
	}
    
    function action_add()
	{	
		$this->view->generate('incomeAdd_view.php', 'template_view.php');
	}
    
    function action_edit()
	{	
		$this->view->generate('incomeEdit_view.php', 'template_view.php');
	}
}