<?php

class Controller_Records extends Controller
{

	function action_index()
	{	
		$this->view->generate('recordCalendar_view.php', 'template_view.php');
	}
    
	function action_list()
	{	
		$this->view->generate('recordList_view.php', 'template_view.php');
	}
    
	function action_add()
	{	
		$this->view->generate('recordAdd_view.php', 'template_view.php');
	}
    
	function action_edit()
	{	
		$this->view->generate('recordEdit_view.php', 'template_view.php');
	}
}