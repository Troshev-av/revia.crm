<?php

class Controller_Sources extends Controller
{

	function action_index()
	{	
		$this->view->generate('sourcesList_view.php', 'template_view.php');
	}
    
    function action_add()
	{	
		$this->view->generate('sourcesAdd_view.php', 'template_view.php');
	}
    
    function action_edit()
	{	
		$this->view->generate('sourcesEdit_view.php', 'template_view.php');
	}
}