<?php

class Controller_Print extends Controller
{

	function action_invoice()
	{	
		$this->view->generate('printInvoice_view.php', 'templatePrint_view.php');
	}

	function action_order()
	{	
		$this->view->generate('printOrder_view.php', 'templatePrint_view.php');
	}

	function action_receipt()
	{	
		$this->view->generate('printReceipt_view.php', 'templatePrint_view.php');
	}
    
    function action_act()
	{	
		$this->view->generate('printAct_view.php', 'templatePrint_view.php');
	}
    
    function action_expense()
	{	
		$this->view->generate('printExpense_view.php', 'templatePrint_view.php');
	}
}