<?php

class Controller_Expense extends Controller
{

	function action_index()
	{	
		$this->view->generate('expenseList_view.php', 'template_view.php');
	}
    
    function action_add()
	{	
		$this->view->generate('expenseAdd_view.php', 'template_view.php');
	}
    
    function action_edit()
	{	
		$this->view->generate('expenseEdit_view.php', 'template_view.php');
	}
}