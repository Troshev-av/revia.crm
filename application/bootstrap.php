<?php
ob_start();
// подключаем файлы ядра
require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';
require_once 'core/password.php';

header("Content-Type: text/html; charset=utf-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Expires: " . date("r"));


if($_GET['logoff'] == 'true'){
    setcookie("authorization", 0);
    setcookie("user_id", '');
    header('Location: /');
}

require_once('php/sql.php');
require_once('php/html.php');

// Шифрование пароля
// $hash = password_hash($password, PASSWORD_DEFAULT);

if(!$_COOKIE["authorization"]){
    if($_POST['login'] != '' && $_POST['password'] != ''){
        $login = $_POST['login'];
        $password = $_POST['password'];
        $user = getUser($login);
        if(password_verify($password, $user['password'])){
            setcookie("authorization", 1);
            setcookie("user_id", $user['user_id']);
            $login = true;
            header('Location: /');
        } else {
            $login = false;
        }
    }
    $login = false;
}else{
    $login = true;
}


require_once 'core/route.php';
Route::start($login); // запускаем маршрутизатор