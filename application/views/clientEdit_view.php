<?php

    $client_info = getClientInfo($_GET['client_id']);
?>

<script src="/js/clients.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Редактирование клиента</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a href="/clients">Клиенты</a></div>
        <div class="breadcrumb-item"><a>Редактирование</a></div>
    </div>
    <div id="control">
        <a href="/charts/clients"><div class="control-button graph"></div></a>
    </div>
</section>
<section id="content">
    <?php
    if(mysql_num_rows($client_info)){
        $client = mysql_fetch_assoc($client_info);
    ?>
        <div id="client-edit" client_id="<?php echo $client['client_id'];?>" class="form">
            <div class="form-item">
                <label for="surname">Фамилия:</label>
                <input id="surname" type="text" class="inputs check-required" value="<?php echo $client['surname'];?>">
                <div class="input-errors-list">
                    <div class="error-required">Данное поле не должно быть пустым!</div>
                </div>
            </div>
            <div class="form-item">
                <label for="name">Имя:</label>
                <input id="name" type="text" class="inputs check-required" value="<?php echo $client['name'];?>">
                <div class="input-errors-list">
                    <div class="error-required">Данное поле не должно быть пустым!</div>
                </div>
            </div>
            <div class="form-item">
                <label for="phone">Телефон:</label>
                <input id="phone" type="text" class="inputs check-required" value="<?php echo $client['phone'];?>">
                <div class="input-errors-list">
                    <div class="error-required">Данное поле не должно быть пустым!</div>
                </div>
            </div>
            <div class="form-item">
                <label for="email">Email:</label>
                <input id="email" type="text" class="inputs" value="<?php echo $client['email'];?>">
            </div>
            <div class="form-item">
                <label for="address">Адрес:</label>
                <input id="address" type="text" class="inputs" value="<?php echo $client['address'];?>">
            </div>
            <div class="form-item">
                <label for="club_card">Клубная карта:</label>
                <input id="club_card" type="text" class="inputs" value="<?php echo $client['club_card'];?>">
            </div>
            <div class="form-item">
                <label for="comment">Дополнительная информация:</label>
                <textarea id="comment" class="inputs"><?php echo $client['comment'];?></textarea>
            </div>
            <div class="form-submit">
                Обновить
            </div>
            <div class="form-response-true hidden">Клиент успешно обновлен!</div>
            <div class="form-response-false hidden">Ошибка, некорректные данные!</div>
        </div>
    <?php
    }else{
        echo '<div class="page_error">Произошла ошибка, возможно Вы ввели некорректные данные!</div>';
    }
?>
</section>