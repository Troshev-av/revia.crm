<?php 
    $record_id = $_GET['record_id'];
    $record_date = '';
    $arr_services = array();
    if(isset($_GET['record_id'])){
        $record_id = $_GET['record_id'];
        $result = getPrintOrder($record_id);
        $result = mysql_fetch_assoc($result);
        if($result['record_id']){
            $client = $result['client'];
            $record_date = $result['date'];
            $arr_services = explode(', ', $result['service']);
            $price= $result['price'];
        }
    }
?>

<style>
    div, input{
        font-family:'Times New Roman';
    }
</style>

<div style="text-align: right;">
    <img src="/img/logo.png">
    <span>VIENN DETAILING</span>
</div>
<div style="text-align: right;">
    <span>ООО "Хаманн" 614066, г.Пермь, ул. Стахановская, 54 литер И, №7</span>
</div>
<div style="text-align: right;">
    <span style="margin-right: 20px;">Тел. +7 (342) 240 46 26</span>
    <span>Сот. 8 (922) 240 46 26</span>
</div>
<div style="text-align: right;">
    <span>WWW.VIENN.RU</span>
</div>
<table class="system" border="1px" style="width: 100%">
    <tr>
        <td colspan="2" rowspan="2">ОАО АКБ «Авангард» <br>Банк получателя</td>
        <td>БИК</td>
        <td width="200px" rowspan="2" style="text-align:center;">044525201 <br> 301018100000000201
</td>
    </tr>
    <tr>
        <td>Сч.№</td>
    </tr>
    <tr>
        <td>ИНН 5905031621</td>
        <td>КПП 590501001</td>
        <td rowspan="2">Сч.№</td>
        <td rowspan="2" style="text-align:center;">40702810433100031327</td>
    </tr>
    <tr>
        <td colspan="2">ООО «Хаманн»<div>Получатель</div></td>
    </tr>
</table>
<div style="margin-top:30px;">
    <span>Счет на оплату № </span>
    <input type="text" value="<?php echo $record_id; ?>">
    <span>от</span> 
    <input type="text" value="<?php echo $record_date ?>">
</div>
<div style="margin-top:10px">
    <span>Поставщик услуг: ООО «Хаманн», Россия, 614066, Пермский край, г. Пермь, ул. Стахановская, 54, производственный корпус №1, офис №7 </span> 
</div>
<div style="margin-top:10px" >
    <span>Покупатель:</span> 
</div>
<textarea style="width: 100%; height: 50px;"><?php echo $client; ?></textarea>
<div style="margin-top:10px" >
    <span>Услуги по обслуживанию авто и мото техники:</span> 
</div>
<table id="invoice" border="1px" style="width: 100%;">
    <tr>
        <td>№</td>
        <td>Наименование</td>
        <td>Кол-во</td>
        <td>Цена(руб.)</td>
        <td>Сумма(руб.)</td>
    </tr>
    <?php 
        foreach($arr_services as $i => $service){
            ?>
            <tr>
                <td><div contenteditable="true" class="number edit" style="width: 60px;"><?php echo $i + 1; ?></div></td>
                <td><div contenteditable="true" style="width: 385px;"><?php echo $service; ?></div></td>
                <td><div contenteditable="true" class="count" style="width: 60px;"></div></td>
                <td><div contenteditable="true" class="price" style="width: 120px;"></div></td>
                <td><div class="sum" readonly style="width: 120px;"></div></td>
            </tr>
            <?php
        }
    ?>
    <tr>
        <td><div contenteditable="true" class="number edit" style="width: 60px;"></div></td>
                <td><div contenteditable="true" style="width: 385px;"></div></td>
                <td><div contenteditable="true" class="count" style="width: 60px;"></div></td>
                <td><div contenteditable="true" class="price" style="width: 120px;"></div></td>
                <td><div class="sum" readonly style="width: 120px;"></div></td>
    </tr>
</table>
<div style="text-align: center">Без налога НДС</div>
<div style="margin-top:30px;">
    <span>Всего наименований: </span>
    <input class="total_count" style="width: 30px;" type="text" value="<?php echo count($arr_services); ?>">
    <span> , на сумму: </span> 
    <input class="invoice total_price" style="width: 60px;" type="text" value="<?php echo $price; ?>">
    <span>руб. </span> 
</div>
<div style="margin-top:10px;">
    <span>Прописью</span>
    <input type="text" style="width: 600px;">
</div>
<div style="margin-top:15px;">
    <span>Руководитель</span> <div style="width: 150px; border-bottom: 1px solid; display: inline-block;"></div><span>Букалов Н.М.</span>
</div>
<div style="margin-top:15px;">
    <span>Бухгалтер <div style="width: 150px; border-bottom: 1px solid; display: inline-block;"></div>Букалов Н.М.</span>
</div>

<button id="print-documnet-btn" style="margin-top: 50px; float: right; width: 100px; height: 30px;">Печать</button>