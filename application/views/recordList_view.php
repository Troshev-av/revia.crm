<?php
    if(isset($_GET['setFilter'])){
        $filter = true;
        $search = false;
        $input = array(
            'style' => 'width: 200px; padding: 0px 10px',
            'value' => 'Ближайшие записи',
            'parentClass' => ''
        );
        $tableInfo = 'Найдено записей';
        $client_id = '';
    }else if(isset($_GET['client_id'])){
        $filter = false;
        $search = true;
        $input = array(
            'style' => 'width: 200px; padding: 0px 10px',
            'value' => 'Поиск по клиенту',
            'parentClass' => ''
        );
        $tableInfo = 'Найдено записей';
        $client_id = $_GET['client_id'];
    }else{
        $filter = false;
        $search = false;
        $input = array(
            'style' => '',
            'value' => '',
            'parentClass' => 'hidden'
        );
        $tableInfo = 'Всего записей';
        $client_id = '';
    }

?>
<script src="/js/records.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Список записей</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a href="/records">Записи</a></div>
        <div class="breadcrumb-item"><a>Список</a></div>
    </div>
    <div id="control">
        <a href="/records/add"><div class="control-button add"></div></a>
        <div class="control-button search"></div>
        <div id="search-input" class="<?php echo $input['parentClass']; ?>"><input id="filter-record" style="<?php echo $input['style']; ?>" value="<?php echo $input['value']; ?>" type="text" class="inputs"/></div>
        <a href="/records"><div class="control-button calendar"></div></a>
        <a href="/charts/records"><div class="control-button graph"></div></a>
    </div>
</section>
<section id="content" class="long">
    <div id="list-records" class="table">
        <div class="row">
            <div class="cell-caption active-sort sort table-record_id" sort="desc" column="record_id">#</div>
            <div class="cell-caption sort table-client" column="client">Фамилия, имя</div>
            <div class="cell-caption table-name">Наименование</div>
            <div class="cell-caption table-address">Адрес</div>
            <div class="cell-caption sort table-price" column="price">Стоимость</div>
            <div class="cell-caption sort table-date" column="date">Дата</div>
            <div class="sort-none"></div>
        </div>
        <?php 
            if ($filter){
                $records = getNextRecords();
            }else if ($search){
                $records = getRecordsClient($client_id);
                $recordCount = getCountRecordsClient($client_id);
                $recordCount = mysql_fetch_row($recordCount);
                $recordCount = $recordCount[0];
                
            } else {
                $records = getFilterRecords(0, 'record_id', 'DESC', '');
                $recordCount = getCountRecords();
                $recordCount = mysql_fetch_row($recordCount);
                $recordCount = $recordCount[0];
            }
            while($row = mysql_fetch_assoc($records)){
                echo recordRows($row['record_id'], $row['client'], $row['name'], $row['address'], $row['price'], $row['date'], $row['time']);
            }
         ?>
    </div>
    <?php if (!$filter && !$search){
    ?>
    <div id="table-info">
        <div id="records-info">
            <?php 
                echo $tableInfo.': <span class="record-count">'.$recordCount.'</span>';
            ?>
        </div>
        <div id="table-navigation">
            <div class="navigation-btn active-btn" sheet="0">1</div>
            <?php                 
                $countStr = ceil(intval($recordCount) / 20);
                for($i = 2; $i <= $countStr; $i++){
                    echo '<div class="navigation-btn" sheet="'.($i-1).'">'.$i.'</div>';
                }
            ?>
        </div>
    </div>
    <?php } ?>
</section>
<section id="modal-record" class="modal">
    <input type="hidden" id="record_id">
    <div class="modal-header">
        <h3>Информация о записи</h3>
        <div class="close-button">×</div>
    </div>
    <div class="modal-body">
        <div class="column-item">
            <div class="item-title">Наименование</div>
            <div id="name" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Клиент</div>
            <div id="client-surname" class="item-text"><a></a></div>
        </div>
        <div class="column-item">
            <div class="item-title">Телефон</div>
            <div id="phone" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Адрес</div>
            <div id="address" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Cтоимость</div>
            <div id="price" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Дата</div>
            <div id="date" class="item-text"></div>
        </div>
    </div>
    <div class="modal-footer">
        <div id="delete-record" class="footer-button">Удалить</div>
        <div id="edit-record" class="footer-button">Редактировать</div>
        <div class="dropdown-btn footer-button">
            Печать
            <ul class="dropdown-list">
                <li class="dropdown-item order-btn">Наряд</li>
                <li class="dropdown-item invoice-btn">Счет</li>
                <li class="dropdown-item receipt-btn">Чек</li>
                <li class="dropdown-item act-btn">Акт</li>
            </ul>
        </div>
    </div>
</section>