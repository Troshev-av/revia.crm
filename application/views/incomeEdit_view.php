<?php
    $income_info = getIncome($_GET['income_id']);
?>
<script src="/js/income.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Редактирование дохода</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a href="/income">Доходы</a></div>
        <div class="breadcrumb-item"><a>Редактирование</a></div>
    </div>
    <div id="control">
        <a href="/charts/income"><div class="control-button graph"></div></a>
    </div>
</section>
<section id="content">
    <?php
    if(mysql_num_rows($income_info)){
        $income = mysql_fetch_assoc($income_info);
    ?>
    <div id="income-edit" class="form">
        <input id="income_id" type="hidden" value="<?php echo $_GET['income_id'];?>">
        <div class="form-item">
            <label for="client">Клиент:</label>
            <input id="client" type="text" class="inputs" value="<?php echo $income['client'];?>, <?php echo $income['phone']; ?>">
            <input id="client_id" type="hidden" class="inputs check-required" value="<?php echo $income['client_id'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="name">Наименование:</label>
            <input id="name" type="text" class="inputs check-required" value="<?php echo $income['name']; ?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="price">Сумма:</label>
            <input id="price" type="text" class="inputs check-required" value="<?php echo $income['price'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="date">Дата:</label>
            <input id="date" type="text" class="inputs check-required" value="<?php echo $income['date'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="time">Время:</label>
            <input id="time" type="text" class="inputs check-required" value="<?php echo $income['time'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-submit">Обновить</div>
        <div class="form-response-true hidden">Доход успешно обновлен!</div>
        <div class="form-response-false hidden">Ошибка, некорректные данные!</div>
    </div>
    <?php
    }else{
        echo '<div class="page_error">Произошла ошибка, возможно Вы ввели некорректные данные!</div>';
    }
    ?>
</section>