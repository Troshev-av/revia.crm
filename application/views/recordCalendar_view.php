<script src="/js/records.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Записи</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a>Записи</a></div>
    </div>
    <div id="control">
        <a href="/records/add"><div class="control-button add"></div></a>
        <a href="/records/list"><div class="control-button table"></div></a>
        <a href="/charts/records"><div class="control-button graph"></div></a>
    </div>
</section>
<section id="content" class="long">
    <div id="current-records" class="calendar-month">
        <?php
            $months_arr = array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь');
            $cur_date = date('01.m.Y');
            $month = date('m');
            $year = date('Y');
        ?>
        <div class="content-title"><?php echo $months_arr[date('m') - 1]; ?></div>
        <div class="calendar">
            <div class="row">
                <div class="cell-caption">Понедельник</div>
                <div class="cell-caption">Вторник</div>
                <div class="cell-caption">Среда</div>
                <div class="cell-caption">Четверг</div>
                <div class="cell-caption">Пятница</div>
                <div class="cell-caption">Суббота</div>
                <div class="cell-caption">Воскресенье</div>
            </div>
            <?php
                $calendar = getCalendar($month, $year);
                echo $calendar;
            ?>
        </div>
        <div class="calendar-footer">
            <div class="record-count">Записей за месяц: 
                <?php 
                    $recordCount = getRecordCount($month, $year);
                    $recordCount = mysql_fetch_row($recordCount);
                    echo $recordCount[0];
                ?>
            </div>
            <div class="calendar-navigation">
                <div target="last-records" class="navigation-btn l-arrow"></div>
                <div target="next-records"  class="navigation-btn r-arrow"></div>
            </div>
        </div>
    </div>
    <div id="last-records" class="calendar-month">
        <?php
            $date = DateTime::createFromFormat("d.m.Y", $cur_date);
            $interval = new DateInterval("P1M");
            $new_date = $date->sub($interval);
            $month = $new_date->format("m");
            $year = $new_date->format("Y");
        ?>
        <div class="content-title"><?php echo $months_arr[$month - 1]; ?></div>
        <div class="calendar">
            <div class="row">
                <div class="cell-caption">Понедельник</div>
                <div class="cell-caption">Вторник</div>
                <div class="cell-caption">Среда</div>
                <div class="cell-caption">Четверг</div>
                <div class="cell-caption">Пятница</div>
                <div class="cell-caption">Суббота</div>
                <div class="cell-caption">Воскресенье</div>
            </div>
            <?php
                $calendar = getCalendar($month, $year);
                echo $calendar;
            ?>
        </div>
        <div class="calendar-footer">
            <div class="record-count">Записей за месяц: 
                <?php 
                    $recordCount = getRecordCount($month, $year);
                    $recordCount = mysql_fetch_row($recordCount);
                    echo $recordCount[0];
                ?>
            </div>
            <div class="calendar-navigation">
                <div target="last-last-records" class="navigation-btn l-arrow"></div>
                <div target="current-records"  class="navigation-btn r-arrow"></div>
            </div>
        </div>
    </div>
    <div id="last-last-records" class="calendar-month">
        <?php            
            $date = DateTime::createFromFormat("d.m.Y", $cur_date);
            $interval = new DateInterval("P2M");
            $new_date = $date->sub($interval);
            $month = $new_date->format("m");
            $year = $new_date->format("Y");

        ?>
        <div class="content-title"><?php echo $months_arr[$month - 1]; ?></div>
        <div class="calendar">
            <div class="row">
                <div class="cell-caption">Понедельник</div>
                <div class="cell-caption">Вторник</div>
                <div class="cell-caption">Среда</div>
                <div class="cell-caption">Четверг</div>
                <div class="cell-caption">Пятница</div>
                <div class="cell-caption">Суббота</div>
                <div class="cell-caption">Воскресенье</div>
            </div>
            <?php
                $calendar = getCalendar($month, $year);
                echo $calendar;
            ?>
        </div>
        <div class="calendar-footer">
            <div class="record-count">Записей за месяц: 
                <?php 
                    $recordCount = getRecordCount($month, $year);
                    $recordCount = mysql_fetch_row($recordCount);
                    echo $recordCount[0];
                ?>
            </div>
            <div class="calendar-navigation">
                <div target="last-last-last-records" class="navigation-btn l-arrow"></div>
                <div target="last-records"  class="navigation-btn r-arrow"></div>
            </div>
        </div>
    </div>
    <div id="last-last-last-records" class="calendar-month">
        <?php            
            $date = DateTime::createFromFormat("d.m.Y", $cur_date);
            $interval = new DateInterval("P3M");
            $new_date = $date->sub($interval);
            $month = $new_date->format("m");
            $year = $new_date->format("Y");

        ?>
        <div class="content-title"><?php echo $months_arr[$month - 1]; ?></div>
        <div class="calendar">
            <div class="row">
                <div class="cell-caption">Понедельник</div>
                <div class="cell-caption">Вторник</div>
                <div class="cell-caption">Среда</div>
                <div class="cell-caption">Четверг</div>
                <div class="cell-caption">Пятница</div>
                <div class="cell-caption">Суббота</div>
                <div class="cell-caption">Воскресенье</div>
            </div>
            <?php
                $calendar = getCalendar($month, $year);
                echo $calendar;
            ?>
        </div>
        <div class="calendar-footer">
            <div class="record-count">Записей за месяц: 
                <?php 
                    $recordCount = getRecordCount($month, $year);
                    $recordCount = mysql_fetch_row($recordCount);
                    echo $recordCount[0];
                ?>
            </div>
            <div class="calendar-navigation">
                <div target="last-last-records" class="navigation-btn r-arrow"></div>
            </div>
        </div>
    </div>
    <div id="next-records" class="calendar-month">
        <?php
            $date = DateTime::createFromFormat("d.m.Y", $cur_date);
            $interval = new DateInterval("P1M");
            $new_date = $date->add($interval);
            $month = $new_date->format("m");
            $year = $new_date->format("Y");
        ?>
        <div class="content-title"><?php echo $months_arr[$month - 1]; ?></div>
        <div class="calendar">
            <div class="row">
                <div class="cell-caption">Понедельник</div>
                <div class="cell-caption">Вторник</div>
                <div class="cell-caption">Среда</div>
                <div class="cell-caption">Четверг</div>
                <div class="cell-caption">Пятница</div>
                <div class="cell-caption">Суббота</div>
                <div class="cell-caption">Воскресенье</div>
            </div>
            <?php
                $calendar = getCalendar($month, $year);
                echo $calendar;
            ?>
        </div>
        <div class="calendar-footer">
            <div class="record-count">Записей за месяц: 
                <?php 
                    $recordCount = getRecordCount($month, $year);
                    $recordCount = mysql_fetch_row($recordCount);
                    echo $recordCount[0];
                ?>
            </div>
            <div class="calendar-navigation">
                <div target="current-records" class="navigation-btn l-arrow"></div>
                <div target="next-next-records"  class="navigation-btn r-arrow"></div>
            </div>
        </div>
    </div>
    <div id="next-next-records" class="calendar-month">
        <?php
            $date = DateTime::createFromFormat("d.m.Y", $cur_date);
            $interval = new DateInterval("P2M");
            $new_date = $date->add($interval);
            $month = $new_date->format("m");
            $year = $new_date->format("Y");
        ?>
        <div class="content-title"><?php echo $months_arr[$month - 1]; ?></div>
        <div class="calendar">
            <div class="row">
                <div class="cell-caption">Понедельник</div>
                <div class="cell-caption">Вторник</div>
                <div class="cell-caption">Среда</div>
                <div class="cell-caption">Четверг</div>
                <div class="cell-caption">Пятница</div>
                <div class="cell-caption">Суббота</div>
                <div class="cell-caption">Воскресенье</div>
            </div>
            <?php
                $calendar = getCalendar($month, $year);
                echo $calendar;
            ?>
        </div>
        <div class="calendar-footer">
            <div class="record-count">Записей за месяц: 
                <?php 
                    $recordCount = getRecordCount($month, $year);
                    $recordCount = mysql_fetch_row($recordCount);
                    echo $recordCount[0];
                ?>
            </div>
            <div class="calendar-navigation">
                <div target="next-records" class="navigation-btn l-arrow"></div>
                <div target="next-next-next-records"  class="navigation-btn r-arrow"></div>
            </div>
        </div>
    </div>
    <div id="next-next-next-records" class="calendar-month">
        <?php
            $date = DateTime::createFromFormat("d.m.Y", $cur_date);
            $interval = new DateInterval("P3M");
            $new_date = $date->add($interval);
            $month = $new_date->format("m");
            $year = $new_date->format("Y");
        ?>
        <div class="content-title"><?php echo $months_arr[$month - 1]; ?></div>
        <div class="calendar">
            <div class="row">
                <div class="cell-caption">Понедельник</div>
                <div class="cell-caption">Вторник</div>
                <div class="cell-caption">Среда</div>
                <div class="cell-caption">Четверг</div>
                <div class="cell-caption">Пятница</div>
                <div class="cell-caption">Суббота</div>
                <div class="cell-caption">Воскресенье</div>
            </div>
            <?php
                $calendar = getCalendar($month, $year);
                echo $calendar;
            ?>
        </div>
        <div class="calendar-footer">
            <div class="record-count">Записей за месяц: 
                <?php 
                    $recordCount = getRecordCount($month, $year);
                    $recordCount = mysql_fetch_row($recordCount);
                    echo $recordCount[0];
                ?>
            </div>
            <div class="calendar-navigation">
                <div target="next-next-records" class="navigation-btn l-arrow"></div>
            </div>
        </div>
    </div>
</section>
<section id="modal-record" class="modal">
    <input type="hidden" id="record_id">
    <div class="modal-header">
        <h3>Информация о записи</h3>
        <div class="close-button">×</div>
    </div>
    <div class="modal-body">
        <div class="column-item">
            <div class="item-title">Наименование</div>
            <div id="name" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Клиент</div>
            <div id="client-surname" class="item-text"><a></a></div>
        </div>
        <div class="column-item">
            <div class="item-title">Телефон</div>
            <div id="phone" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Адрес</div>
            <div id="address" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Cтоимость</div>
            <div id="price" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Дата</div>
            <div id="date" class="item-text"></div>
        </div>
    </div>
    <div class="modal-footer">
        <div id="delete-record" class="footer-button">Удалить</div>
        <div id="edit-record" class="footer-button">Редактировать</div>
        <div class="dropdown-btn footer-button">
            Печать
            <ul class="dropdown-list">
<!--
                <li class="dropdown-item order-btn">Наряд</li>
                <li class="dropdown-item invoice-btn">Счет</li>
                <li class="dropdown-item receipt-btn">Чек</li>
                <li class="dropdown-item act-btn">Акт</li>
-->
            </ul>
        </div>
    </div>
</section>
<section id="modal-records-list" class="modal">
    <div class="modal-header">
        <h3>Список записей</h3>
        <div class="close-button">×</div>
    </div>
    <div class="modal-body">
        
    </div>
</section>