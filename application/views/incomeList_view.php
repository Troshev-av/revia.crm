<script src="/js/income.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Доходы</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a>Доходы</a></div>
    </div>
    <div id="control">
        <a href="/income/add"><div class="control-button add"></div></a>
        <div class="control-button search"></div>
        <div id="search-input" class="hidden"><input id="filter-income" type="text" class="inputs"/></div>
        <a href="/charts/income"><div class="control-button graph"></div></a>
    </div>
</section>
<section id="content" class="long">
    <div id="list-income" class="table">
        <div class="row">
            <div class="cell-caption active-sort sort table-income_id" sort="desc" column="income_id">#</div>
            <div class="cell-caption sort table-client" column="client">Клиент</div>
            <div class="cell-caption table-name">Наименование</div>
            <div class="cell-caption sort table-price" column="price">Сумма</div>
            <div class="cell-caption sort table-date" column="date">Дата</div>
            <div class="sort-none"></div>
        </div>
        <?php 
            $income = getFilterIncome('0', 'income_id', 'DESC', '');
            while($row = mysql_fetch_assoc($income)){
                echo incomeRow($row['income_id'], $row['client'], $row['name'], $row['price'], $row['date']);
            }
         ?>
    </div>
    <div id="table-info">
        <div id="records-info">Всего записей в таблице: 
            <?php 
                $incomeCount = getCountIncome();
                $incomeCount = mysql_fetch_row($incomeCount);
                echo $incomeCount[0]; 
            ?>
        </div>
        <div id="table-navigation">
            <div class="navigation-btn active-btn" sheet="0">1</div>
            <?php                 
                $countStr = ceil(intval($incomeCount[0]) / 20);
                for($i = 2; $i <= $countStr; $i++){
                    echo '<div class="navigation-btn" sheet="'.($i-1).'">'.$i.'</div>';
                }
            ?>
        </div>
    </div>
</section>

<section id="modal-income" class="modal">
    <input type="hidden" id="income_id">
    <div class="modal-header">
        <h3>Информация о доходе</h3>
        <div class="close-button">×</div>
    </div>
    <div class="modal-body">
        <div class="column-item">
            <div class="item-title">Клиент</div>
            <div id="client-surname" class="item-text"><a></a></div>
        </div>
        <div class="column-item">
            <div class="item-title">Наименование</div>
            <div id="name" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Сумма</div>
            <div id="price" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Дата</div>
            <div id="date" class="item-text"></div>
        </div>
    </div>
    <div class="modal-footer">
        <div id="delete-income" class="footer-button">Удалить</div>
        <div id="edit-income" class="footer-button">Редактировать</div>
    </div>
</section>