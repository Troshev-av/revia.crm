<?php
    $task_info = getTask($_GET['task_id']);
?>
<script src="/js/tasks.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Редактирование задачи</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a href="/tasks">Задачи</a></div>
        <div class="breadcrumb-item"><a>Редактирование</a></div>
    </div>
    <div id="control">
        <a href="/charts/tasks"><div class="control-button graph"></div></a>
    </div>
</section>
<section id="content">
    <?php
    if(mysql_num_rows($task_info)){
        $task = mysql_fetch_assoc($task_info);
    ?>
    <div id="task-edit" class="form">
        <input id="task_id" type="hidden" value="<?php echo $_GET['task_id'];?>">
        <div class="form-item">
            <label for="client">Клиент:</label>
            <input id="client" type="text" class="inputs" value="<?php echo $task['client'];?>, <?php echo $task['phone'];?>">
            <input id="client_id" type="hidden" class="inputs check-required" value="<?php echo $task['client_id'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="name">Наименование:</label>
            <input id="name" type="text" class="inputs check-required" value="<?php echo $task['name'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="date">Дата:</label>
            <input id="date" type="text" class="inputs check-required" value="<?php echo $task['date'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="time">Время:</label>
            <input id="time" type="text" class="inputs check-required" value="<?php echo $task['time'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-submit">Обновить</div>
        <div class="form-response-true hidden">Задача успешно обновлена!</div>
        <div class="form-response-false hidden">Ошибка, некорректные данные!</div>
    </div>
    <?php
    }else{
        echo '<div class="page_error">Произошла ошибка, возможно Вы ввели некорректные данные!</div>';
    }
    ?>
</section>