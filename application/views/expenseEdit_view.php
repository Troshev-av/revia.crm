<?php
    $expense_info = getExpense($_GET['expense_id']);
?>
<script src="/js/expense.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Редактирование расхода</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a href="/expense">Расходы</a></div>
        <div class="breadcrumb-item"><a>Редактирование</a></div>
    </div>
    <div id="control">
        <a href="/charts/expense"><div class="control-button graph"></div></a>
    </div>
</section>
<section id="content">
    <?php
    if(mysql_num_rows($expense_info)){
        $expense = mysql_fetch_assoc($expense_info);
    ?>
    <div id="expense-edit" class="form">
        <input id="expense_id" type="hidden" value="<?php echo $_GET['expense_id'];?>">
        <div class="form-item">
            <label for="name">Наименование:</label>
            <input id="name" type="text" class="inputs check-required" value="<?php echo $expense['name'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="price">Сумма:</label>
            <input id="price" type="text" class="inputs check-required" value="<?php echo $expense['price'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="order">Заказ-наряд:</label>
            <input id="order" type="text" class="inputs" value="<?php echo $expense['order'];?>">
        </div>
        <div class="form-item">
            <label for="date">Дата:</label>
            <input id="date" type="text" class="inputs check-required" value="<?php echo $expense['date'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="time">Время:</label>
            <input id="time" type="text" class="inputs check-required" value="<?php echo $expense['time'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-submit">Обновить</div>
        <div class="form-response-true hidden">Расход успешно обновлен!</div>
        <div class="form-response-false hidden">Ошибка, некорректные данные!</div>
    </div>
    <?php
    }else{
        echo '<div class="page_error">Произошла ошибка, возможно Вы ввели некорректные данные!</div>';
    }
    ?>
</section>