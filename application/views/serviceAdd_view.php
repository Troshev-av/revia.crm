<script src="/js/services.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Добавление услуги</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a href="/services">Услуги</a></div>
        <div class="breadcrumb-item"><a>Добавление</a></div>
    </div>
</section>
<section id="content">
    <div id="service-add" class="form">
        <div class="form-item">
            <label for="name">Наименование:</label>
            <input id="name" type="text" class="inputs check-required">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-submit">
            Добавить
        </div>
        <div class="form-response-true hidden">Услуга успешно добавлена!</div>
        <div class="form-response-false hidden">Неккоректное заполнение полей!</div>
    </div>
</section>