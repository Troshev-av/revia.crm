<?php
    $service_info = getService($_GET['service_id']);
?>
<script src="/js/services.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Редактирование услуги</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a href="/services">Услуги</a></div>
        <div class="breadcrumb-item"><a>Редактирование</a></div>
    </div>
</section>
<section id="content">
    <?php
    if(mysql_num_rows($service_info)){
        $service = mysql_fetch_assoc($service_info);
    ?>
    <div id="service-edit" class="form">
        <input id="service_id" type="hidden" value="<?php echo $_GET['service_id'];?>">
        <div class="form-item">
            <label for="name">Наименование:</label>
            <input id="name" type="text" class="inputs check-required" value="<?php echo $service['name'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="date">Дата:</label>
            <input id="date" type="text" class="inputs check-required" value="<?php echo $service['date'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-submit">Обновить</div>
        <div class="form-response-true hidden">Услуга успешно обновлена!</div>
        <div class="form-response-false hidden">Ошибка, некорректные данные!</div>
    </div>
    <?php
    }else{
        echo '<div class="page_error">Произошла ошибка, возможно Вы ввели некорректные данные!</div>';
    }
    ?>
</section>