<script src="/js/records.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Добавление записи</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a href="/records">Записи</a></div>
        <div class="breadcrumb-item"><a>Добавление</a></div>
    </div>
    <div id="control">
        <a href="/records/list"><div class="control-button table"></div></a>
        <a href="/records"><div class="control-button calendar"></div></a>
        <a href="/charts/records"><div class="control-button graph"></div></a>
    </div>
</section>
<section id="content">
    <div id="record-add" class="form">
        <div class="form-item">
            <label for="client">Клиент:</label>
            <input id="client" type="text" class="inputs autocomplete">
            <input id="client_id" type="hidden" class="check-required">
            <ul id="client_area" class="autocomplete-area">
            </ul>
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="name">Наименование:</label>
            <input id="name" type="text" class="inputs check-required">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="price">Стоимость:</label>
            <input id="price" type="text" class="inputs check-required">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="date">Дата:</label>
            <input id="date" type="text" class="inputs check-required">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="time">Время:</label>
            <input id="time" type="text" class="inputs check-required">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-submit">
            Добавить
        </div>
        <div class="form-response-true hidden">Запись успешно добавлена!</div>
        <div class="form-response-false hidden">Неккоректное заполнение полей!</div>
    </div>
</section>