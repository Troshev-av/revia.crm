<script src="/js/chart/Chart.min.js" type="text/javascript"></script>
<script src="/js/chart/Chart.Line.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Главная</h2>
    <div id="breadcrumb">
    </div>
    <div id="control">
        <a href="/records"><div class="control-button calendar"></div></a>
        <a href="/charts/"><div class="control-button graph"></div></a>
    </div>
</section>

<section class="main-item-area">
    <a href="/records/list?setFilter=true" class="graph-href">
        <div class="content-title">Ближайшие записи</div>
        <?php
            $records = getNextRecords();
            while($item = mysql_fetch_assoc($records)){
                echo clientRecordsHtml($item['time'], $item['date'], $item['name'], $item['price']);
            }   
        ?>
    </a>
</section>
<section class="main-item-area">
    <a href="/tasks?setFilter=true" class="graph-href">
        <div class="content-title">Ближайшие задачи</div>
        <?php
            $tasks = getNextTasks();
            while($item = mysql_fetch_assoc($tasks)){
                echo clientTasksHtml($item['time'], $item['date'], $item['name'], $item['client']);
            }   
        ?>
    </a>
</section>
<section class="graph-area">
    <?php 
    ?>
    <a href="/charts/income" class="graph-href">
        <div class="content-title">Доходы</div>
            <div class="graph">
                <canvas id="income-canvas" height="210"></canvas>
            </div>
    </a>
</section>
<section class="graph-area">
    <a href="/charts/expense" class="graph-href">
        <div class="content-title">Расходы</div>
            <div class="graph">
                <canvas id="expense-canvas" height="210"></canvas>
            </div>
    </a>
</section>
<section class="graph-area">
    <a href="/charts/profit" class="graph-href">
        <div class="content-title">Прибыль</div>
            <div class="graph">
                <canvas id="profit-canvas" height="210"></canvas>
            </div>
    </a>
</section>
<script>
    <?php 
        $months_arr = array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь');
        $time = strtotime(date('d.m.Y'));
    ?>
    
    var income_data = {
        labels : [
            <?php
                for($i = 0; $i < 5; $i++){
                    echo '"'.$months_arr[date('m', strtotime('-'.(4 - $i)." month", $time)) - 1].'", ';
                }
            ?>
        ],
        datasets : [
            {
                label: "income",
                fillColor : "rgba(182, 10, 49, 0.4)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "#525252",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(220,220,220,1)",
                data : [
                    <?php
                        for($i = 0; $i < 5; $i++){
                            $month = date('m.Y', strtotime('-'.(4 - $i)." month", $time));
                            $count = getCountIncomeInMonth($month);
                            $count = mysql_fetch_row($count);
                            $count = (!$count[0])? 0 : $count[0];
                            echo $count.', ';

                        }
                    ?>
                ]
            }
        ]
    };
    
    var profit_data = {
        labels : [
            <?php
                for($i = 0; $i < 5; $i++){
                    echo '"'.$months_arr[date('m', strtotime('-'.(4 - $i)." month", $time)) - 1].'", ';
                }
            ?>
        ],
        datasets : [
            {
                label: "profit",
                fillColor : "rgba(182, 10, 49, 0.4)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "#525252",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(220,220,220,1)",
                data : [
                    <?php
                        for($i = 0; $i < 5; $i++){
                            $month = date('m.Y', strtotime('-'.(4 - $i)." month", $time));
                            $count = getCountProfitInMonth($month);
                            $count = mysql_fetch_row($count);
                            $count = (!$count[0])? 0 : $count[0];
                            echo $count.', ';

                        }
                    ?>
                ]
            }
        ]
    };
    
    var expense_data = {
        labels : [
            <?php
                for($i = 0; $i < 5; $i++){
                    echo '"'.$months_arr[date('m', strtotime('-'.(4 - $i)." month", $time)) - 1].'", ';
                }
            ?>
        ],
        datasets : [
            {
                label: "expense",
                fillColor : "rgba(182, 10, 49, 0.4)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "#525252",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(220,220,220,1)",
                data : [
                    <?php
                        for($i = 0; $i < 5; $i++){
                            $month = date('m.Y', strtotime('-'.(4 - $i)." month", $time));
                            $count = getCountExpenseInMonth($month);
                            $count = mysql_fetch_row($count);
                            $count = (!$count[0])? 0 : $count[0];
                            echo $count.', ';

                        }
                    ?>
                ]
            }
        ]
    };
    
    window.onload = function(){
        var ctx = document.getElementById("income-canvas").getContext("2d");
        window.myLine = new Chart(ctx).Line(income_data, {
            responsive: true,
            scaleShowGridLines : true,
            pointDotStrokeWidth : 0,
            pointHitDetectionRadius : 7
        });
        var ctx1 = document.getElementById("profit-canvas").getContext("2d");
        window.myLine = new Chart(ctx1).Line(profit_data, {
            responsive: true,
            scaleShowGridLines : true,
            pointDotStrokeWidth : 0,
            pointHitDetectionRadius : 7
        });
        
        var ctx2 = document.getElementById("expense-canvas").getContext("2d");
        window.myLine = new Chart(ctx2).Line(expense_data, {
            responsive: true,
            scaleShowGridLines : true,
            pointDotStrokeWidth : 0,
            pointHitDetectionRadius : 7
        });
    }
</script>