<script src="/js/sources.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Добавление поставщика</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a href="/sources">Поставщики</a></div>
        <div class="breadcrumb-item"><a>Добавление</a></div>
    </div>
</section>
<section id="content">
    <div id="sources-add" class="form">
        <div class="form-item">
            <label for="name">Наименование:</label>
            <input id="name" type="text" class="inputs check-required">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="phone">Телефон:</label>
            <input id="phone" type="text" class="inputs">
        </div>
        <div class="form-item">
            <label for="address">Адрес:</label>
            <input id="address" type="text" class="inputs">
        </div>
        <div class="form-item">
            <label for="comment">Комментарий:</label>
            <textarea id="comment" class="inputs"></textarea>
        </div>
        <div class="form-submit">
            Добавить
        </div>
        <div class="form-response-true hidden">Поставщик успешно добавлен!</div>
        <div class="form-response-false hidden">Неккоректное заполнение полей!</div>
    </div>
</section>