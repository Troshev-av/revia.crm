<?php
    $record_info = getRecord($_GET['record_id']);
?>
<script src="/js/records.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Редактирование записи</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a href="/records">Записи</a></div>
        <div class="breadcrumb-item"><a>Редактирование</a></div>
    </div>
    <div id="control">
        <a href="/records/list"><div class="control-button table"></div></a>
        <a href="/records"><div class="control-button calendar"></div></a>
        <a href="/charts/records"><div class="control-button graph"></div></a>
    </div>
</section>
<section id="content">
    <?php
    if(mysql_num_rows($record_info)){
        $record = mysql_fetch_assoc($record_info);
    ?>
    <div id="record-edit" record_id="<?php echo $client['record_id'];?>" class="form">
        <input id="record_id" type="hidden" value="<?php echo $_GET['record_id'];?>">
        <div class="form-item">
            <label for="client">Клиент:</label>
            <input id="client" type="text" class="inputs autocomplete" value="<?php echo $record['client'].", ".$record['phone'];?>">
            <input id="client_id" type="hidden" class="check-required" value="<?php echo $record['client_id'];?>">
            <ul id="client_area" class="autocomplete-area">
            </ul>
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="name">Наименование:</label>
            <input id="name" type="text" class="inputs check-required" value="<?php echo $record['name'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="price">Стоимость:</label>
            <input id="price" type="text" class="inputs check-required" value="<?php echo $record['price'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="date">Дата:</label>
            <input id="date" type="text" class="inputs check-required" value="<?php echo $record['date'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="time">Время:</label>
            <input id="time" type="text" class="inputs check-required" value="<?php echo $record['time'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-submit">Обновить</div>
        <div class="form-response-true hidden">Запись успешно обновлена!</div>
        <div class="form-response-false hidden">Ошибка, некорректные данные!</div>
    </div>
    <?php
    }else{
        echo '<div class="page_error">Произошла ошибка, возможно Вы ввели некорректные данные!</div>';
    }
    ?>
</section>