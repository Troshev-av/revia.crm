<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="Cache-Control" content="no-cache">
        <title>VIENN</title>
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="/css/bootstrap-datepicker.min.css"/>
		<link rel="stylesheet" type="text/css" href="/css/bootstrap-datepicker3.min.css"/>
		<link rel="stylesheet" type="text/css" href="/css/bootstrap-datepicker3.standalone.min.css"/>
		<link rel="stylesheet" type="text/css" href="/css/style.css"/>
        <script src="/js/jquery-2.1.4.min.js" type="text/javascript"></script>
        <script src="/js/jquery.ui.position.js"></script>
        <script src="/js/jquery.Autocomplete.js"></script>
        <script src="/js/bootstrap-datepicker.min.js"></script>
        <script src="/js/bootstrap-datepicker.ru.min.js"></script>
        <script src="/js/inputmask.js"></script>
        <script src="/js/jquery.inputmask.js"></script>
        <script src="/js/inputmask.regex.extensions.js"></script>
        <script src="/js/inputmask.date.extensions.js"></script>
		<script src="/js/script.js" type="text/javascript"></script>
    </head>
    
    <body>
        
        <?php 
             $user_agent = $_SERVER["HTTP_USER_AGENT"];
            if (strpos($user_agent, "Firefox") !== false){
                ?>
                <section id="modal-firefox" class="modal">
                    <div class="modal-header">
                        <h3>Ошибка браузера</h3>
                        <div class="close-button">×</div>
                    </div>
                    <div class="modal-body">
                        <div><p>Внимание! Корректность работы в данном браузере не гарантируется. Возможна нестабильная работа. <br>Рекомендуем использовать следующие браузеры:</p></div>
                        <div class="browser-list">
                            <a href="https://www.google.ru/chrome/browser/desktop/">Google Chrome</a>
                            <a href="https://browser.yandex.ru/new/desktop/">Yandex</a>
                            <a href="http://www.opera.com/ru">Opera</a>
                            <a href="https://www.microsoft.com/ru-ru/download/internet-explorer.aspx">Internet Explorer 11+</a>
                        </div>
                    </div>
                </section>
                <script>
                    $(document).ready(function(){
                        modalShow($('#modal-firefox'));
                    });
                </script>
                <?php
            }
        ?>
        
        <section id="loading">
            <div id="progress-bar"></div>
        </section>
        <section id="notifications"></section>
        <section id="left-panel" class="close">
            <?php 
                $user_info = getUserInfo($_COOKIE["user_id"]);
                $user_all_access = getUserAllAccess($_COOKIE["user_id"]);
            ?>
            <div class="panel-body">
                <div class="user-info">
                    <div class="user-name"><?php echo $user_info["user_name"]; ?></div>
                    <div class="user-type"><?php echo $user_info["user_type"]; ?></div>
                </div>
                <?php 
                if ($user_all_access['main']) { ?>
                    <a href="/">
                        <div class="panel-item">
                            <div class="panel-item-mark main-icon"></div>
                            <div class="panel-item-title">Главная</div>
                        </div>
                    </a>
                <?php }
                if ($user_all_access['clients']) { ?>
                    <a href="/clients">
                        <div class="panel-item">
                            <div class="panel-item-mark contacts-icon"></div>
                            <div class="panel-item-title">Клиенты</div>
                        </div>
                    </a>
                <?php }
                if ($user_all_access['records']) { ?>
                    <a href="/records">
                        <div class="panel-item">
                            <div class="panel-item-mark records-icon"></div>
                            <div class="panel-item-title">Записи</div>
                        </div>
                    </a>
                <?php }
                if ($user_all_access['tasks']) { ?>
                    <a href="/tasks">
                        <div class="panel-item">
                            <div class="panel-item-mark tasks-icon"></div>
                            <div class="panel-item-title">Задачи</div>
                        </div>
                    </a>
                <?php }
                if ($user_all_access['income']) { ?>
                    <a href="/income">
                        <div class="panel-item">
                            <div class="panel-item-mark income-icon"></div>
                            <div class="panel-item-title">Доходы</div>
                        </div>
                    </a>
                <?php }
                if ($user_all_access['expense']) { ?>
                    <a href="/expense">
                        <div class="panel-item">
                            <div class="panel-item-mark expense-icon"></div>
                            <div class="panel-item-title">Расходы</div>
                        </div>
                    </a>
                <?php }
                if ($user_all_access['sources']) { ?>
                    <a href="/sources">
                        <div class="panel-item">
                            <div class="panel-item-mark contacts-icon"></div>
                            <div class="panel-item-title">Поставшики</div>
                        </div>
                    </a>
                <?php }
                if ($user_all_access['charts']) { ?>
                    <a href="/charts">
                        <div class="panel-item">
                            <div class="panel-item-mark charts-icon"></div>
                            <div class="panel-item-title">Графики</div>
                        </div>
                    </a>
                <?php } ?> 
                <a href="/?logoff=true">
                    <div class="panel-item">
                        <div class="panel-item-mark exit-icon"></div>
                        <div class="panel-item-title">Выход</div>
                    </div>
                </a>
            </div>
        </section>
        <section id="right-panel" class="close">
            <div class="panel-header">
                <h3>Уведомления</h3>
                <div class="close-button">×</div>
            </div>
            <div class="panel-body">
                <?php 
                    insertNewNotifications();
                    $record_notify = getNewRecordsNotification(false);
                    while($item = mysql_fetch_assoc($record_notify)){
                        hideNewNotification($item['notification_id']);
                        echo notificationRecord($item);
                    }
                    $task_notify = getNewTasksNotifications(false);
                    while($item = mysql_fetch_assoc($task_notify)){
                        hideNewNotification($item['notification_id']);
                        echo notificationTask($item);
                    }
                ?>
            </div>
        </section>
        <section id="wrap"></section>
        <section id="navbar">
            <div id="left-panel-button"></div>
            <div id="logo">VIENN</div>
            <div id="right-panel-button"></div>
        </section>
        <?php include 'application/views/'.$content_view; ?>
    </body>
</html>