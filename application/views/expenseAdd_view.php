<script src="/js/expense.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Добавление расхода</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a href="/expense">Расходы</a></div>
        <div class="breadcrumb-item"><a>Добавление</a></div>
    </div>
    <div id="control">
        <a href="/charts/expense"><div class="control-button graph"></div></a>
    </div>
</section>
<section id="content">
    <div id="expense-add" class="form">
        <div class="form-item">
            <label for="name">Наименование:</label>
            <input id="name" type="text" class="inputs check-required">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="price">Сумма:</label>
            <input id="price" type="text" class="inputs check-required">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="order">Заказ-наряд:</label>
            <input id="order" type="text" class="inputs">
        </div>
        <div class="form-item">
            <label for="date">Дата:</label>
            <input id="date" type="text" class="inputs check-required">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="time">Время:</label>
            <input id="time" type="text" class="inputs check-required">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-submit">
            Добавить
        </div>
        <div class="form-response-true hidden">Расход успешно добавлен!</div>
        <div class="form-response-false hidden">Неккоректное заполнение полей!</div>
    </div>
</section>