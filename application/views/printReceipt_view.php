<?php 
    $record_id = $_GET['record_id'];
    $record_date = '';
    $months_arr = array('Января','Февраля','Марта','Апреля','Мая','Июня','Июля','Августа','Сентября','Октября','Ноября','Декабря');
    $arr_services = array();
    if(isset($_GET['record_id'])){
        $record_id = $_GET['record_id'];
        $result = getPrintOrder($record_id);
        $result = mysql_fetch_assoc($result);
        if($result['record_id']){
            $record_date = date_create_from_format('d.m.Y H:i:s', $result['date'].' 00:00:00');
            $arr_services = explode(', ', $result['service']);
            $price= $result['price'];
        }
    }
?>

<div>
    <span>Товарный Чек №</span>
    <input type="text" value="<?php echo $record_id; ?>">
</div>
<div style="margin-top:10px;">
    <span> От «</span>
    <input type="text" maxlength="2" style="width: 15px;" value="<?php echo date_format($record_date, 'd'); ?>">
    <span>»&nbsp;</span>
    <input type="text" style="width: 75px;" value="<?php echo $months_arr[date_format($record_date, 'n') - 1]; ?>">
    <span>&nbsp;</span>
    <input type="text" maxlength="4" style="width: 30px;" value="<?php echo date_format($record_date, 'Y'); ?>">
    <span>г.</span>
</div>
<div style="margin-top:10px;">
    <span>Общество с ограниченной ответственностью  «Хаманн» ИНН 5905031621, Россия, 614066, Пермский край, г. Пермь, ул. Стахановская, 54, производственный корпус №1, офис №7</span>
</div>
<div style="margin-top:30px" >
    <span>Услуги по обслуживанию авто и мото техники:</span> 
</div>
<table id="check" border="1px" style="width: 100%;">
    <tr>
        <td>№</td>
        <td>Наименование</td>
        <td>Кол-во</td>
        <td>Цена(руб.)</td>
        <td>Сумма(руб.)</td>
    </tr>
    <?php 
        foreach($arr_services as $i => $service){
            ?>
            <tr>
                <td><div contenteditable="true" class="number edit" style="width: 60px;"><?php echo $i + 1; ?></div></td>
                <td><div contenteditable="true" style="width: 385px;"><?php echo $service; ?></div></td>
                <td><div contenteditable="true" class="count" style="width: 60px;"></div></td>
                <td><div contenteditable="true" class="price" style="width: 120px;"></div></td>
                <td><div contenteditable="true" class="sum" readonly style="width: 120px;"></div></td>
            </tr>
            <?php
        }
    ?>
    <tr>
        <td><div contenteditable="true" class="number edit" style="width: 60px;"></div></td>
        <td><div contenteditable="true" style="width: 385px;"></div></td>
        <td><div contenteditable="true" class="count" style="width: 60px;"></div></td>
        <td><div contenteditable="true" class="price" style="width: 120px;"></div></td>
        <td><div contenteditable="true" class="sum" readonly style="width: 120px;"></div></td>
    </tr>
</table>
<div style="margin-top:10px;">
    <span>Всего наименований: </span>
    <input class="total_count" style="width: 30px;" type="text" value="<?php echo count($arr_services); ?>">
    <span> , на сумму: </span> 
    <input class="check total_price" style="width: 60px;" type="text" value="<?php echo $price; ?>">
    <span>руб. </span> 
</div>
<div style="margin-top:30px;">
    <span>Исполнитель: </span>
    <div contenteditable="true" class="count" style="min-width: 100px; display:inline-block; border: 1px solid #A9A9A9;"></div>
</div>

<div style="margin-top:10px;">
    <span> Заказчик </span>
    <div contenteditable="true" class="count" style="min-width: 100px; display:inline-block; border: 1px solid #A9A9A9;"></div>
</div>

<div>
    <span><font size="2"> Выполненные услуги принял, претензий не имею</font></span>
</div>

<hr style="margin: 30px 0px;">

<div>
    <span>Общество с ограниченной ответственностью «Хаманн» ИНН 5905031621</span>
</div>
<div style="margin-top:10px; text-align: center;">
    <span>Квитанция об оплате</span>
</div>
<div style="margin-top:10px;">
    <span>Квитация об оплате к  чеку № </span>
    <input type="text" value="<?php echo $record_id; ?>">
</div>
<div style="margin-top: 10px;">
    <span>Принято от </span>
    <input type="text" style="width: 500px;">
</div>
<div style="margin-top: 10px;">
    <span>Оказание услуг по обслуживанию авто и мото техники в студии VIENN</span>
</div>
<table id="receipt" border="1px" style="width: 100%; margin-top: 10px; opacity: 0.3;">
    <tr>
        <td>№</td>
        <td>Наименование</td>
        <td>Кол-во</td>
        <td>Цена(руб.)</td>
        <td>Сумма(руб.)</td>
    </tr>
    <?php 
        foreach($arr_services as $i => $service){
            ?>
            <tr>
                <td><div class="number" style="width: 60px; height:18px;"><?php echo $i + 1; ?></div></td>
                <td><div style="width: 385px; height:18px;"><?php echo $service; ?></div></td>
                <td><div class="count" style="width: 60px; height:18px;"></div></td>
                <td><div class="price" style="width: 120px; height:18px;"></div></td>
                <td><div class="sum" readonly style="width: 120px; height:18px;"></div></td>
            </tr>
            <?php
        }
    ?>
</table>
<div style="margin-top:10px;">
    <span>Сумма </span> 
    <input class="receipt total_price" style="width: 60px;" type="text" value="<?php echo $price; ?>">
    <span> руб.</span> 
</div>
<div style="margin-top: 30px;">
    <span>Всего к оплате(прописью):</span>
    <input value="" type="text" style=" width: 505px;">
</div>

<button id="print-documnet-btn" style="margin-top: 50px; float: right; width: 100px; height: 30px;">Печать</button>