<?php 
    $record_id = '';
    $record_date = '';
    $client = '';
    $phone = '';
    $car = '';
    $car_vin = '';
    $car_class= '';
    $car_color = '';
    $vendor = '';
    $state_number = '';
    $year = '';
    $mileage = '';
    $sum = '';
    $car = '';
    $vin = '';
    $class = '';
    $year = '';
    $mileage = '';
    $arr_services = array();
    if(isset($_GET['record_id'])){
        $record_id = $_GET['record_id'];
        $result = getPrintOrder($record_id);
        $result = mysql_fetch_assoc($result);
        if($result['record_id']){
            $record_date = $result['date'];
            $client = $result['client'];
            $phone = $result['phone'];
            $car = $result['car'];
            $vin = $result['vin'];
            $class = $result['class'];
            $color = $result['color'];
            $year = $result['year'];
            $mileage = $result['mileage'];
            $state_number = $result['state_number'];
            $price= $result['price'];
            $arr_services = explode(', ', $result['service']);
        }
    }
?>
<input type="hidden" id="record_id" value="<?php echo $record_id; ?>">
<div style="overflow: auto;">
    <div style="overflow: auto; float:left;">
        <img src="/img/logo-big.png" style="width: 150px;">
    </div>
    <div style="overflow: auto; float:right;">
        <div style="margin-top: 5px;">
            Детейлинг Центр VIENN - салон красоты для авто и мото техники.
        </div>
        <div style="margin-top: 5px;"> 
            Адрес: Пермский край, г. Пермь, ул. Стахановская, 54 литер И, №7
        </div>
        <div style="margin-top: 5px;"> 
            Телефон: +7-(342)-240-4626
        </div>
        <div style="margin-top: 5px;"> 
            Сот.: 8-(922)-240-4626
        </div>
        <div style="margin-top: 5px;"> 
            WWW.VIENN.RU
        </div>    
    </div>
</div>
<div style="margin-top: 20px; text-align: center;">ЗАКАЗ-НАРЯД № <input value="<?php echo $record_id; ?>" type="text" style="width: 80px;"> от <input type="text" value="<?php echo $record_date; ?>"></div>
<div style="overflow: auto; margin-top: 30px;">
    <div style="overflow:auto; float: left;">
        <div class="client" style="margin-top: 5px; "> <span style="vertical-align: top;">Заказчик/плательщик:</span> <div contenteditable="true" style="width: 250px; display:inline-block; border: 1px solid #A9A9A9;"><?php echo $client; ?></div></div>
        <div class="client" style="margin-top: 5px;"> <span style="vertical-align: top;">Телефон:</span> <div contenteditable="true" style="width: 338px; display:inline-block; border: 1px solid #A9A9A9;"><?php echo $phone; ?></div></div>
        <div style="margin-top: 5px;"> <span style="vertical-align: top;">Марка и модель:</span> <div contenteditable="true" style="width: 287px; display:inline-block; border: 1px solid #A9A9A9;"><?php echo $car; ?></div></div>
        <div style="margin-top: 5px;"> <span style="vertical-align: top;">VIN:</span> <div id="vin" contenteditable="true" style="width: 368px; display:inline-block; border: 1px solid #A9A9A9;"><?php echo $vin; ?></div></div>
        <div style="margin-top: 5px;"> <span style="vertical-align: top;">Класс:</span> <div id="class" contenteditable="true" style="width: 356px; display:inline-block; border: 1px solid #A9A9A9;"><?php echo $class; ?></div></div>
        <div style="margin-top: 5px;"> <span style="vertical-align: top;">Цвет:</span> <div id="color" contenteditable="true" style="width: 361px; display:inline-block; border: 1px solid #A9A9A9;"><?php echo $color; ?></div></div>
    </div>
    <div style="overflow:auto; float: right;">
        <div class="for-client" style="margin-top: 5px;"> <span style="vertical-align: top;">Заказ принял: </span> <div contenteditable="true" style="width: 250px; display:inline-block; border: 1px solid #A9A9A9;"></div></div>
        <div class="for-client" style="margin-top: 5px;"> Расчет: 
            <select style="width: 294px">
                <option>Наличный</option>
                <option>Безналичный</option>
            </select>
        </div>
            <div style="margin-top: 5px;"><span style="vertical-align: top;">Гос. №: </span> <div contenteditable="true" style="width: 288px; display:inline-block; border: 1px solid #A9A9A9;"><?php echo $state_number; ?></div></div>
        <div style="margin-top: 5px;"><span style="vertical-align: top;">Год выпуска: </span> <div id="year" contenteditable="true" style="width: 250px; display:inline-block; border: 1px solid #A9A9A9;"><?php echo $year; ?></div></div>
        <div class="for-client" style="margin-top: 5px;"><span style="vertical-align: top;">Пробег: </span> <div id="mileage" contenteditable="true" style="width: 286px; display:inline-block; border: 1px solid #A9A9A9;"><?php echo $mileage; ?></div></div>
    </div>
</div>
<div style="margin-top: 20px;">
    <div>Причина обращения</div>
    <textarea class="cause" style="width: 100%; height: 30px; text-decoration: underline; resize: vertical; box-sizing: border-box; padding: 5px;"></textarea>
</div>
<div style="margin-top: 20px;">
    <div>Другие повреждения на момент приемки:</div>
    <textarea class="other" style="width: 100%; height: 30px; text-decoration: underline; resize: vertical; box-sizing: border-box; padding: 5px;"></textarea>
</div>
<div style="margin-top: 30px;">
    <div>Перечень работ:</div>
    <table id="order" border="1px" style="width: 100%;">
        <tr>
            <td>№</td>
            <td>Наименование</td>
            <td>Кол-во</td>
            <td class="for-client">Цена(руб.)</td>
            <td class="for-client">Сумма(руб.)</td>
            <td>Исполнитель</td>
        </tr>
        <?php 
            foreach($arr_services as $i => $service){
                ?>
                <tr>
                    <td><div contenteditable="true" class="number" style="width: 30px;"><?php echo $i + 1; ?></div></td>
                    <td><div contenteditable="true" class="service" style="width: 385px;"><?php echo $service; ?></div></td>
                    <td><div contenteditable="true" class="count" style="width: 60px;"></div></td>
                    <td class="for-client"><div contenteditable="true" class="price" style="width: 80px;"></div></td>
                    <td class="for-client"><div class="sum" readonly style="width: 80px;"></div></td>
                    <td><div contenteditable="true" class="performer" style="width: 90px;"></div></td>
                </tr>
                <?php
            }
        ?>
        <tr>
            <td><div contenteditable="true" class="number" style="width: 30px;"></div></td>
            <td><div contenteditable="true" class="service" style="width: 385px;"></div></td>
            <td><div contenteditable="true" class="count" style="width: 60px;"></div></td>
            <td class="for-client"><div contenteditable="true" class="price" style="width: 80px;"></div></td>
            <td class="for-client"><div class="sum" readonly style="width: 80px;"></div></td>
            <td><div contenteditable="true" class="performer" style="width: 90px;"></div></td>
        </tr>
    </table>
</div>
<div class="for-client">
    <div id="damage_area" style="margin-top: 30px;">
        <div>Даю согласие на изменение конструктивных особенностей автомобиля:</div>
        <table id="damage" class="hidden_table" border="1px" style="width: 100%;">
            <tr>
                <td>№</td>
                <td>Наименование</td>
            </tr>
            <tr>
                <td style="width: 30px;"><div contenteditable="true" class="number" style="width: 100%"></div></td>
                <td><div contenteditable="true"></div></td>
            </tr>
            <tr>
                <td style="width: 30px;"><div contenteditable="true" class="number" style="width: 100%"></div></td>
                <td><div contenteditable="true"></div></td>
            </tr>
        </table>
    </div>
    
    <div id="damage_comment" style="margin-top: 30px;">
        <br>Материалы и запчасти необходимые для проведения работ согласованы.<br>Заказчик претензий к процессу исполнителя не имеет, об особенностях монтажа и эксплуатации предупрежден:
        <div>
            <span>_________________________________________________________________________________________________</span>
            <br>
            <span style="font-size: 11px;margin-left: 360px;">(дата; подпись и расшифровка)</span>
        </div>
    </div>
    
    <div style="margin-top: 30px;">
        <span>Итого\общая стоимость работ (руб.):</span>
        <input class="order total_price" value="<?php echo $price; ?>" type="text">
    </div>
    <div style="margin-top: 10px;">
        <span>Предоплатата в размере</span>
        <div contenteditable="true" style="width: 100px; border-bottom: 1px solid;height: 20px; display: inline-block;"></div>
        <span>(руб.)</span>
    </div>
    <div style="margin-top: 10px;">
        <span>Принял:__________________________________________________________________________________________</span>
        <br>
        <span style="font-size: 11px;margin-left: 360px;">(дата; подпись и расшифровка)</span>
    </div>
    <div style="margin-top: 30px;">
        <span>Всего к оплате(прописью):</span>
        <input value="" type="text" style=" width: 505px;">
    </div>
</div>
<div style="margin-top: 30px;">
    <span>Контроль качества : ________________________________________________________________________________</span>
    <br>
    <span style="font-size: 11px;margin-left: 360px;">(дата; подпись и расшифровка)</span>
</div>
<div class="for-client" style="margin-top: 30px;">
    <span>Исполнитель выполнил все обязательства в полном объеме и в срок с надлежащим качеством.<br>Заказчик претензий к исполнителю не имеет: ___________________________________________________________</span>
    <br>
    <span style="font-size: 11px;margin-left: 470px;">(дата; подпись и расшифровка)</span>
</div>
<div class="for-client">
    <h6 style="margin-top: 30px; width: 100%; text-align: center;">НДС не начисляется (VIENN detailing не является плательщиком НДС)</h6>
</div>

<div class="for-work" style="margin-top: 30px; display:none;">
    <span>Особые отметки и напоминания:</span>
</div>

<button id="print-documnet-btn" class="order" style="margin: 50px 0px; float: right; width: 100px; height: 30px;">Печать</button>