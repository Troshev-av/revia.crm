<script src="/js/services.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Услуги</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a>Услуги</a></div>
    </div>
    <div id="control">
        <a href="/services/add"><div class="control-button add"></div></a>
        <div class="control-button search"></div>
        <div id="search-input" class="hidden"><input id="filter-services" type="text" class="inputs"/></div>
    </div>
</section>
<section id="content" class="long">
    <div id="list-services" class="table">
        <div class="row">
            <div class="cell-caption active-sort sort table-service_id" sort="desc" column="service_id">#</div>
            <div class="cell-caption sort table-name" column="name">Наименование</div>
            <div class="cell-caption sort table-date" column="date">Дата</div>
            <div class="sort-none"></div>
        </div>
        <?php 
            $records = getServiceList();
            while($row = mysql_fetch_assoc($records)){
         ?>
        <div class="row data-row" record="<?php echo $row['service_id']; ?>">
            <div class="cell table-service_id"><?php echo $row['service_id']; ?></div>
            <div class="cell table-name"><?php echo $row['name']; ?></div>
            <div class="cell table-date"><?php echo $row['date']; ?></div>
            <div class="cell">
                <div class="drop-menu">
                    <ul class="drop-menu-list">
                        <li class="drop-menu-item edit">Редактировать</li>
                        <li class="drop-menu-item delete">Удалить</li>
                    </ul>
                </div>
            </div>
        </div>
        <?php 
            }
         ?>
    </div>
    <div id="table-info">
        <div id="records-info">Всего услуг: 
            <?php 
                $serviceCount = getCountServices();
                $serviceCount = mysql_fetch_row($serviceCount);
                echo $serviceCount[0]; 
            ?>
        </div>
        <div id="table-navigation">
            <div class="navigation-btn active-btn" sheet="0">1</div>
            <?php                 
                $countStr = ceil(intval($serviceCount[0]) / 20);
                for($i = 2; $i <= $countStr; $i++){
                    echo '<div class="navigation-btn" sheet="'.($i-1).'">'.$i.'</div>';
                }
            ?>
        </div>
    </div>
</section>