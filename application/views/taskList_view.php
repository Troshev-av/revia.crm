<?php
    if(isset($_GET['setFilter'])){
        $filter = true;
        $search = false;
        $input = array(
            'style' => 'width: 200px; padding: 0px 10px',
            'value' => 'Ближайшие записи',
            'parentClass' => ''
        );
        $tableInfo = 'Найдено задач';
    }else if(isset($_GET['client_id'])){
        $filter = false;
        $search = true;
        $input = array(
            'style' => 'width: 200px; padding: 0px 10px',
            'value' => 'Поиск по клиенту',
            'parentClass' => ''
        );
        $tableInfo = 'Найдено записей';
        $client_id = $_GET['client_id'];
    }else{
        $filter = false;
        $search = false;
        $input = array(
            'style' => '',
            'value' => '',
            'parentClass' => 'hidden'
        );
        $tableInfo = 'Всего задач';
    }

?>
<script src="/js/tasks.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Задачи</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a>Задачи</a></div>
    </div>
    <div id="control">
        <a href="/tasks/add"><div class="control-button add"></div></a>
        <div class="control-button search"></div>
        <div id="search-input" class="<?php echo $input['parentClass']; ?>"><input id="filter-tasks" style="<?php echo $input['style']; ?>" value="<?php echo $input['value']; ?>" type="text" class="inputs"/></div>
        <a href="/charts/tasks"><div class="control-button graph"></div></a>
    </div>
</section>
<section id="content" class="long">
    <div id="list-tasks" class="table">
        <div class="row">
            <div class="cell-caption active-sort sort table-task_id" sort="desc" column="task_id">#</div>
            <div class="cell-caption sort table-client" column="client">Клиент</div>
            <div class="cell-caption table-name">Наименование</div>
            <div class="cell-caption sort table-date" column="date">Дата</div>
            <div class="sort-none"></div>
        </div>
        <?php 
            if ($filter){
                $tasks = getNextTasks();
            }else if ($search){ 
                $tasks = getClientTasks($client_id);
            } else {
                $tasks = getFilterTasks(0, 'task_id', 'DESC', '');
                $taskCount = getCountTasks($tasks);
                $taskCount = mysql_fetch_row($taskCount);
                $taskCount = $taskCount[0];
            }
            while($row = mysql_fetch_assoc($tasks)){
                echo taskRow($row['task_id'], $row['client'], $row['name'], $row['date'], $row['time']);
            }
         ?>
    </div>
    <?php if (!$filter && !$search){
    ?>
    <div id="table-info">
        <div id="records-info">
            <?php 
                echo $tableInfo.': '.$taskCount;
            ?>
        </div>
        <div id="table-navigation">
            <div class="navigation-btn active-btn" sheet="0">1</div>
            <?php                 
                $countStr = ceil(intval($taskCount) / 20);
                for($i = 2; $i <= $countStr; $i++){
                    echo '<div class="navigation-btn" sheet="'.($i-1).'">'.$i.'</div>';
                }
            ?>
        </div>
    </div>
    <?php } ?>
</section>

<section id="modal-task" class="modal">
    <input type="hidden" id="task_id">
    <div class="modal-header">
        <h3>Информация о задаче</h3>
        <div class="close-button">×</div>
    </div>
    <div class="modal-body">
        <div class="column-item">
            <div class="item-title">Клиент</div>
            <div id="client-surname" class="item-text"><a></a></div>
        </div>
        <div class="column-item">
            <div class="item-title">Телефон</div>
            <div id="phone" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Наименование</div>
            <div id="name" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Дата</div>
            <div id="date" class="item-text"></div>
        </div>
    </div>
    <div class="modal-footer">
        <div id="delete-task" class="footer-button">Удалить</div>
        <div id="edit-task" class="footer-button">Редактировать</div>
    </div>
</section>