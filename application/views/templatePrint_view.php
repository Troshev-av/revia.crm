<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>VIENN</title>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600' rel='stylesheet' type='text/css'>
        <script src="/js/jquery-2.1.4.min.js" type="text/javascript"></script>
        <script src="/js/jquery.ui.position.js"></script>
        <script src="/js/jquery.Autocomplete.js"></script>
        <script src="/js/bootstrap-datepicker.min.js"></script>
        <script src="/js/bootstrap-datepicker.ru.min.js"></script>
        <script src="/js/inputmask.js"></script>
        <script src="/js/jquery.inputmask.js"></script>
        <script src="/js/inputmask.regex.extensions.js"></script>
        <script src="/js/inputmask.date.extensions.js"></script>
		<script src="/js/script.js" type="text/javascript"></script>
		<script src="/js/print.js" type="text/javascript"></script>
        <style>
            textarea{
                font-family: serif;
                font-size: 14px;
            }
            table{
                text-align: left;
            }
            .print-ready{
                border: 0px;
                text-decoration: underline;
            }
            textarea.print-ready{
                resize:none !important;
            }
            table .print-ready{
                text-decoration: none;
            }
        </style>
    </head>
    
    <body id="print-document" style="width: 785px; margin: auto;">
        <?php include 'application/views/'.$content_view; ?>
    </body>
</html>