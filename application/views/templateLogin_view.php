<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>VIENN</title>        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="/css/style.css"/>
    </head>
    
    <body>
        <?php include 'application/views/'.$content_view; ?>
    </body>
</html>