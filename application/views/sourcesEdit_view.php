<?php
    $source_info = getSource($_GET['source_id']);
?>
<script src="/js/sources.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Редактирование поставщика</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a href="/sources">Поставщики</a></div>
        <div class="breadcrumb-item"><a>Редактирование</a></div>
    </div>
</section>
<section id="content">
    <?php
    if(mysql_num_rows($source_info)){
        $source = mysql_fetch_assoc($source_info);
    ?>
    <div id="sources-edit" class="form">
        <input id="source_id" type="hidden" value="<?php echo $_GET['source_id'];?>">
        <div class="form-item">
            <label for="name">Наименование:</label>
            <input id="name" type="text" class="inputs check-required" value="<?php echo $source['name'];?>">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="phone">Телефон:</label>
            <input id="phone" type="text" class="inputs" value="<?php echo $source['phone'];?>">
        </div>
        <div class="form-item">
            <label for="address">Адрес:</label>
            <input id="address" type="text" class="inputs" value="<?php echo $source['address'];?>">
        </div>
        <div class="form-item">
            <label for="comment">Комментарий:</label>
            <textarea id="comment" class="inputs"><?php echo $source['comment'];?></textarea>
        </div>
        <div class="form-submit">Обновить</div>
        <div class="form-response-true hidden">Поставщик успешно обновлен!</div>
        <div class="form-response-false hidden">Ошибка, некорректные данные!</div>
    </div>
    <?php
        }else{
            echo '<div class="page_error">Произошла ошибка, возможно Вы ввели некорректные данные!</div>';
        }
    ?>
</section>