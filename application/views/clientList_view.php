<?php 
    if(isset($_GET['client_id'])){
        $filter = true;
        $input = array(
            'style' => 'width: 200px; padding: 0px 10px',
            'value' => 'Поиск клиента',
            'parentClass' => ''
        );
        $tableInfo = 'Найдено клиентов';
        $client_id = $_GET['client_id'];
    }else{
        $filter = false;
        $input = array(
            'style' => '',
            'value' => '',
            'parentClass' => 'hidden'
        ); 
        $tableInfo = 'Всего клиентов';
        $client_id = '';
    }
?>

<script src="/js/clients.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Клиенты</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a>Клиенты</a></div>
    </div>
    <div id="control">
        <a href="/clients/add"><div class="control-button add"></div></a>
        <div class="control-button search"></div>
        <div id="search-input" class="<?php echo $input['parentClass']; ?>"><input id="filter-clients" style="<?php echo $input['style']; ?>" value="<?php echo $input['value']; ?>" type="text" class="inputs"/></div>

        <a href="/charts/clients"><div class="control-button graph"></div></a>
    </div>
</section>
<section id="content" class="long">
    <div id="clients" class="table">
        <div class="row">
            <div class="cell-caption table-client_id active-sort sort" sort="desc" column="client_id">#</div>
            <div class="cell-caption table-surname sort" column="surname">Фамилия, имя</div>
            <div class="cell-caption table-phone">Телефон</div>
            <div class="cell-caption table-email">Email</div>
            <div class="cell-caption table-address">Адрес</div>
            <div class="cell-caption table-date sort" column="date">Дата</div>
            <div class="sort-none"></div>
        </div>
        <?php 
            if($filter){
                $result = getClient($client_id);
            }else{
                $result = getFilterClients(0, 'client_id', 'DESC', '');
            }
            
            while($row = mysql_fetch_assoc($result)){
                echo clientRow($row['client_id'], $row['surname'], $row['name'], $row['phone'], $row['email'], $row['address'], $row['date']);
            }
         ?>
    </div>
    <?php if (!$filter){
    ?>
    <div id="table-info">
        <div id="records-info"><?php echo $tableInfo; ?>: 
            <?php 
                if($filter){
                    $clientCount[0] = 1;
                }else{
                    $clientCount = getCountClients();
                    $clientCount = mysql_fetch_row($clientCount);
                }
                
                echo $clientCount[0]; 
            ?>
        </div>
        <div id="table-navigation">
            <div class="navigation-btn active-btn" sheet="0">1</div>
            <?php                 
                $countStr = ceil(intval($clientCount[0]) / 20);
                for($i = 2; $i <= $countStr; $i++){
                    echo '<div class="navigation-btn" sheet="'.($i-1).'">'.$i.'</div>';
                }
            ?>
        </div>
    </div>
    <?php } ?>
</section>
<section id="modal-client" class="modal">
    <div class="modal-header">
        <h3>Информация о клиенте</h3>
        <div class="close-button">×</div>
    </div>
    <div class="modal-body">
        <input id="client_id" type="hidden">
        <div id="client-items" class="modal-column">
            <div id="client-records-item">
                <div id="records-header">
                    <div class="item-title">Записи</div>
                    <div id="client-records-href">
                        <a href="">Все записи клиента</a>
                    </div>
                </div>
                <div id="client-records" class="client-item"></div>
            </div>
            <div id="client-tasks-item">
                <div id="tasks-header">
                    <div class="item-title">Задачи</div>
                    <div id="client-tasks-href">
                        <a href="">Все задачи клиента</a>
                    </div>
                </div>
                <div id="client-tasks" class="client-item">Нет активных задач</div>
            </div>
        </div>
        <div id="client-info" class="modal-column">
            <div class="column-item">
                <div class="item-title">Id клиента</div>
                <div id="client-id" class="item-text"></div>
            </div>
            <div class="column-item">
                <div class="item-title">Дата</div>
                <div id="client-date" class="item-text"></div>
            </div>
            <div class="column-item">
                <div class="item-title">Фамилия</div>
                <div id="client-surname" class="item-text"></div>
            </div>
            <div class="column-item">
                <div class="item-title">Имя</div>
                <div id="client-name" class="item-text"></div>
            </div>
            <div class="column-item">
                <div class="item-title">Телефон</div>
                <div id="client-phone" class="item-text"></div>
            </div>
            <div class="column-item">
                <div class="item-title">Email</div>
                <div id="client-email" class="item-text"></div>
            </div>
            <div class="column-item">
                <div class="item-title">Адрес</div>
                <div id="client-address" class="item-text"></div>
            </div>
            <div class="column-item">
                <div class="item-title">Клубная карта:</div>
                <div id="club_card" class="item-text"></div>
            </div>
            <div class="column-item">
                <div class="item-title">Комментарий</div>
                <div id="client-comment" class="item-text justify"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div id="client_del" class="footer-button">Удалить</div>
        <div id="client_edit" class="footer-button">Редактировать</div>
    </div>
</section>