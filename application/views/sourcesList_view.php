<script src="/js/sources.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Поставщики</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a>Поставщики</a></div>
    </div>
    <div id="control">
        <a href="/sources/add"><div class="control-button add"></div></a>
        <div class="control-button search"></div>
        <div id="search-input" class="hidden"><input id="filter-sources" type="text" class="inputs"/></div>
        <a href="/charts/sources"><div class="control-button graph"></div></a>
    </div>
</section>
<section id="content" class="long">
    <div id="list-sources" class="table">
        <div class="row">
            <div class="cell-caption active-sort sort table-source_id" sort="desc" column="source_id">#</div>
            <div class="cell-caption table-name">Наименование</div>
            <div class="cell-caption table-phone">Телефон</div>
            <div class="cell-caption table-address">Адрес</div>
            <div class="cell-caption sort table-date" column="date">Дата</div>
            <div class="sort-none"></div>
        </div>
        <?php 
            $sources = getFilterSources(0, 'source_id', 'DESC', '');
            while($row = mysql_fetch_assoc($sources)){
                echo sourceRow($row['source_id'], $row['name'], $row['phone'], $row['address'], $row['date']);
            }
         ?>
    </div>
    <div id="table-info">
        <div id="records-info">Всего записей в таблице: 
            <?php 
                $sourcesCount = getCountSources();
                $sourcesCount = mysql_fetch_row($sourcesCount);
                echo $sourcesCount[0]; 
            ?>
        </div>
        <div id="table-navigation">
            <div class="navigation-btn active-btn" sheet="0">1</div>
            <?php                 
                $countStr = ceil(intval($sourcesCount[0]) / 20);
                for($i = 2; $i <= $countStr; $i++){
                    echo '<div class="navigation-btn" sheet="'.($i-1).'">'.$i.'</div>';
                }
            ?>
        </div>
    </div>
</section>


<section id="modal-source" class="modal">
    <input type="hidden" id="source_id">
    <div class="modal-header">
        <h3>Информация о доходе</h3>
        <div class="close-button">×</div>
    </div>
    <div class="modal-body">
        <div class="column-item">
            <div class="item-title">Наименование</div>
            <div id="name" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Телефон</div>
            <div id="phone" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Адрес</div>
            <div id="address" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Дата</div>
            <div id="date" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Комментарий</div>
            <div id="comment" class="item-text"></div>
        </div>
    </div>
    <div class="modal-footer">
        <div id="delete-sources" class="footer-button">Удалить</div>
        <div id="edit-sources" class="footer-button">Редактировать</div>
    </div>
</section>