<!--<script src="/js/chart.js" type="text/javascript"></script>-->
<script src="/js/chart/Chart.min.js" type="text/javascript"></script>
<script src="/js/chart/Chart.Line.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>График записей</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a href="/charts/">Графики</a></div>
        <div class="breadcrumb-item"><a>Записи</a></div>
    </div>
    <div id="control">
        
    </div>
</section>
<section class="graph-area midle">
    <div class="content-title">За неделю</div>
    <div class="graph">
        <canvas id="week-canvas" height="210"></canvas>
    </div>
</section>
<section class="graph-area midle">
    <div class="content-title">За Месяц</div>
    <div class="graph">
        <canvas id="month-canvas" height="210"></canvas>
    </div>
</section>
<section class="graph-area midle">
    <div class="content-title">За год</div>
    <div class="graph">
        <canvas id="year-canvas" height="210"></canvas>
    </div>
</section>
<script>
    <?php 
        $time = strtotime(date('d.m.Y'));
        $day_arr = array('Пн','Вт','Ср','Чт','Пт','Сб','Вс');
        $week_arr = array();
        $week_num_start = date('W', strtotime(date('01.m.Y'), $time));
        $week_num_finish = date('W', strtotime(date('t.m.Y'), $time));
        $date = date('01.m.Y');
        for($i = $week_num_start; $i < $week_num_finish; $i++){
            $week['start'] = $date;
            $week['finish'] = date('d.m.Y', strtotime($date.' sun'));
            array_push($week_arr,$week);
            $date = date('d.m.Y', strtotime($week['finish'].' + 1 day'));
        }
        $week['start'] = $date;
        $week['finish'] = date('t.m.Y');
        array_push($week_arr,$week);
        $months_arr = array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь');
    ?>
    var week_data = {
        labels : [
            <?php
                for($i = 0; $i < 7; $i++){
                    echo '"'.$day_arr[date('N', strtotime('-'.(6 - $i)." day", $time)) - 1].'", ';
                }
            ?>
        ],
        datasets : [
            {
                label: "week",
                fillColor : "rgba(182, 10, 49, 0.4)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "#525252",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(220,220,220,1)",
                data : [
                    <?php
                        for($i = 0; $i < 7; $i++){
                            $date = date('d.m.Y', strtotime('-'.(6 - $i)." day", $time));
                            $count = getCountRecordsInDay($date);
                            $count = mysql_fetch_row($count);
                            $count = (!$count[0])? 0 : $count[0];
                            echo $count.', ';

                        }
                    ?>
                ]
            }
        ]

    }
    
    var month_data = {
        labels : [
            <?php
                foreach($week_arr as $week_item){
                    echo '"'.$week_item['start'].' - '.$week_item['finish'].'", ';
                }
            ?>
        ],
        datasets : [
            {
                label: "month",
                fillColor : "rgba(182, 10, 49, 0.4)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "#525252",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(220,220,220,1)",
                data : [
                    <?php
                        foreach($week_arr as $week_item){
                            $count = getCountRecordsInWeek($week_item);
                            $count = mysql_fetch_row($count);
                            echo $count[0].', ';
                        }
                    ?>
                ]
            }
        ]

    }
    
    var year_data = {
        labels : [
            <?php
                for($i = 0; $i < 12; $i++){
                    echo '"'.$months_arr[date('m', strtotime('-'.(11 - $i)." month", $time)) - 1].'", ';
                }
            ?>
        ],
        datasets : [
            {
                label: "year",
                fillColor : "rgba(182, 10, 49, 0.4)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "#525252",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(220,220,220,1)",
                data : [
                    <?php
                        for($i = 0; $i < 12; $i++){
                            $month = date('m.Y', strtotime('-'.(11 - $i)." month", $time));
                            $count = getCountRecordsInMonth($month);
                            $count = mysql_fetch_row($count);
                            $count = (!$count[0])? 0 : $count[0];
                            echo $count.', ';

                        }
                    ?>
                ]
            }
        ]

    }

    window.onload = function(){
        var ctx = document.getElementById("week-canvas").getContext("2d");
        window.myLine = new Chart(ctx).Line(week_data, {
            responsive: true,
            maintainAspectRatio: false,
            scaleShowGridLines : true,
            pointDotStrokeWidth : 0,
            pointHitDetectionRadius : 7
        });
        
        var ctx1 = document.getElementById("month-canvas").getContext("2d");
        window.myLine = new Chart(ctx1).Line(month_data, {
            responsive: true,
            scaleShowGridLines : true,
            pointDotStrokeWidth : 0,
            pointHitDetectionRadius : 7
        });
        
        var ctx2 = document.getElementById("year-canvas").getContext("2d");
        window.myLine = new Chart(ctx2).Line(year_data, {
            responsive: true,
            scaleShowGridLines : true,
            pointDotStrokeWidth : 0,
            pointHitDetectionRadius : 7
        });
        Chart.defaults.global.responsive = true;
    }
    
</script>