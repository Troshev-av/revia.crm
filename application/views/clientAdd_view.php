<script src="/js/clients.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Добавление клиента</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a href="/clients">Клиенты</a></div>
        <div class="breadcrumb-item"><a>Добавление</a></div>
    </div>
    <div id="control">
        <a href="/charts/clients"><div class="control-button graph"></div></a>
    </div>
</section>
<section id="content">
    <div id="client-add" class="form" function="newClient">
        <div class="form-item">
            <label for="surname">Фамилия:</label>
            <input id="surname" type="text" class="inputs check-required">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="name">Имя:</label>
            <input id="name" type="text" class="inputs check-required">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="phone">Телефон:</label>
            <input id="phone" type="text" class="inputs check-required">
            <div class="input-errors-list">
                <div class="error-required">Данное поле не должно быть пустым!</div>
            </div>
        </div>
        <div class="form-item">
            <label for="email">Email:</label>
            <input id="email" type="text" class="inputs">
        </div>
        <div class="form-item">
            <label for="address">Адрес:</label>
            <input id="address" type="text" class="inputs">
        </div>
        <div class="form-item">
            <label for="club_card">Клубная карта:</label>
            <input id="club_card" type="text" class="inputs">
        </div>
        <div class="form-item">
            <label for="comment">Дополнительная информация:</label>
            <textarea id="comment" class="inputs"></textarea>
        </div>
        <div class="form-submit">
            Добавить
        </div>
        <div class="form-response-true hidden">Клиент успешно добавлен!</div>
        <div class="form-response-false hidden">Данный клиент уже существует!</div>
    </div>
</section>