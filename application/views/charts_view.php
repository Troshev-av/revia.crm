<script src="/js/chart/Chart.min.js" type="text/javascript"></script>
<script src="/js/chart/Chart.Line.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Графики</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a>Графики</a></div>
    </div>
</section>
<section class="graph-area">
    <a href="/charts/clients" class="graph-href">
        <div class="content-title">Клиенты</div>
            <div class="graph">
                <canvas id="clients-canvas" height="210"></canvas>
            </div>
    </a>
</section>
<section class="graph-area">
    <a href="/charts/records" class="graph-href">
        <div class="content-title">Записи</div>
            <div class="graph">
                <canvas id="records-canvas" height="210"></canvas>
            </div>
    </a>
</section>
<section class="graph-area">
    <a href="/charts/tasks" class="graph-href">
        <div class="content-title">Задачи</div>
            <div class="graph">
                <canvas id="tasks-canvas" height="210"></canvas>
            </div>
    </a>
</section>
<section class="graph-area">
    <a href="/charts/income" class="graph-href">
        <div class="content-title">Доходы</div>
            <div class="graph">
                <canvas id="income-canvas" height="210"></canvas>
            </div>
    </a>
</section>
<section class="graph-area">
    <a href="/charts/expense" class="graph-href">
        <div class="content-title">Расходы</div>
            <div class="graph">
                <canvas id="expense-canvas" height="210"></canvas>
            </div>
    </a>
</section>
<section class="graph-area">
    <a href="/charts/profit" class="graph-href">
        <div class="content-title">Прибыль</div>
            <div class="graph">
                <canvas id="profit-canvas" height="210"></canvas>
            </div>
    </a>
</section>
<script>
    <?php 
        $months_arr = array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь');
        $time = strtotime(date('d.m.Y'));
    ?>
    var clients_data = {
        labels : [
            <?php
                for($i = 0; $i < 5; $i++){
                    echo '"'.$months_arr[date('m', strtotime('-'.(4 - $i)." month", $time)) - 1].'", ';
                }
            ?>
        ],
        datasets : [
            {
                label: "clients",
                fillColor : "rgba(182, 10, 49, 0.4)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "#525252",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(220,220,220,1)",
                data : [
                    <?php
                for($i = 0; $i < 5; $i++){
                    $month = date('m.Y', strtotime('-'.(4 - $i)." month", $time));
                    $count = getCountClientsInMonth($month);
                    $count = mysql_fetch_row($count);
                    echo $count[0].', ';
                    
                }
            ?>
                ]
            }
        ]

    }
    
    var tasks_data = {
        labels : [
            <?php
                for($i = 0; $i < 5; $i++){
                    echo '"'.$months_arr[date('m', strtotime('-'.(4 - $i)." month", $time)) - 1].'", ';
                }
            ?>
        ],
        datasets : [
            {
                label: "tasks",
                fillColor : "rgba(182, 10, 49, 0.4)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "#525252",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(220,220,220,1)",
                data : [
                    <?php
                        for($i = 0; $i < 5; $i++){
                            $month = date('m.Y', strtotime('-'.(4 - $i)." month", $time));
                            $count = getCountTasksInMonth($month);
                            $count = mysql_fetch_row($count);
                            echo $count[0].', ';

                        }
                    ?>
                ]
            }
        ]

    }
    
    var records_data = {
        labels : [
            <?php
                for($i = 0; $i < 5; $i++){
                    echo '"'.$months_arr[date('m', strtotime('-'.(4 - $i)." month", $time)) - 1].'", ';
                }
            ?>
        ],
        datasets : [
            {
                label: "records",
                fillColor : "rgba(182, 10, 49, 0.4)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "#525252",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(220,220,220,1)",
                data : [
                    <?php
                        for($i = 0; $i < 5; $i++){
                            $month = date('m.Y', strtotime('-'.(4 - $i)." month", $time));
                            $count = getCountRecordsInMonth($month);
                            $count = mysql_fetch_row($count);
                            echo $count[0].', ';

                        }
                    ?>
                ]
            }
        ]

    }
    
    var income_data = {
        labels : [
            <?php
                for($i = 0; $i < 5; $i++){
                    echo '"'.$months_arr[date('m', strtotime('-'.(4 - $i)." month", $time)) - 1].'", ';
                }
            ?>
        ],
        datasets : [
            {
                label: "income",
                fillColor : "rgba(182, 10, 49, 0.4)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "#525252",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(220,220,220,1)",
                data : [
                    <?php
                        for($i = 0; $i < 5; $i++){
                            $month = date('m.Y', strtotime('-'.(4 - $i)." month", $time));
                            $count = getCountIncomeInMonth($month);
                            $count = mysql_fetch_row($count);
                            $count = (!$count[0])? 0 : $count[0];
                            echo $count.', ';

                        }
                    ?>
                ]
            }
        ]

    }
    
    var expense_data = {
        labels : [
            <?php
                for($i = 0; $i < 5; $i++){
                    echo '"'.$months_arr[date('m', strtotime('-'.(4 - $i)." month", $time)) - 1].'", ';
                }
            ?>
        ],
        datasets : [
            {
                label: "expense",
                fillColor : "rgba(182, 10, 49, 0.4)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "#525252",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(220,220,220,1)",
                data : [
                    <?php
                        for($i = 0; $i < 5; $i++){
                            $month = date('m.Y', strtotime('-'.(4 - $i)." month", $time));
                            $count = getCountExpenseInMonth($month);
                            $count = mysql_fetch_row($count);
                            $count = (!$count[0])? 0 : $count[0];
                            echo $count.', ';

                        }
                    ?>
                ]
            }
        ]

    }
    
    var profit_data = {
        labels : [
            <?php
                for($i = 0; $i < 5; $i++){
                    echo '"'.$months_arr[date('m', strtotime('-'.(4 - $i)." month", $time)) - 1].'", ';
                }
            ?>
        ],
        datasets : [
            {
                label: "profit",
                fillColor : "rgba(182, 10, 49, 0.4)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "#525252",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(220,220,220,1)",
                data : [
                    <?php
                        for($i = 0; $i < 5; $i++){
                            $month = date('m.Y', strtotime('-'.(4 - $i)." month", $time));
                            console.log($month);
                            $count = getCountProfitInMonth($month);
                            $count = mysql_fetch_row($count);
                            $count = (!$count[0])? 0 : $count[0];
                            echo $count.', ';

                        }
                    ?>
                ]
            }
        ]

    }

    window.onload = function(){
        var ctx = document.getElementById("clients-canvas").getContext("2d");
        window.myLine = new Chart(ctx).Line(clients_data, {
            responsive: true,
            scaleShowGridLines : true,
            pointDotStrokeWidth : 0,
            pointHitDetectionRadius : 7
        });
        var ctx1 = document.getElementById("records-canvas").getContext("2d");
        window.myLine = new Chart(ctx1).Line(records_data, {
            responsive: true,
            scaleShowGridLines : true,
            pointDotStrokeWidth : 0,
            pointHitDetectionRadius : 7
        });
        var ctx2 = document.getElementById("tasks-canvas").getContext("2d");
        window.myLine = new Chart(ctx2).Line(tasks_data, {
            responsive: true,
            scaleShowGridLines : true,
            pointDotStrokeWidth : 0,
            pointHitDetectionRadius : 7
        });
        var ctx3 = document.getElementById("income-canvas").getContext("2d");
        window.myLine = new Chart(ctx3).Line(income_data, {
            responsive: true,
            scaleShowGridLines : true,
            pointDotStrokeWidth : 0,
            pointHitDetectionRadius : 7
        });
        var ctx4 = document.getElementById("expense-canvas").getContext("2d");
        window.myLine = new Chart(ctx4).Line(expense_data, {
            responsive: true,
            scaleShowGridLines : true,
            pointDotStrokeWidth : 0,
            pointHitDetectionRadius : 7
        });
        var ctx5 = document.getElementById("profit-canvas").getContext("2d");
        window.myLine = new Chart(ctx5).Line(profit_data, {
            responsive: true,
            scaleShowGridLines : true,
            pointDotStrokeWidth : 0,
            pointHitDetectionRadius : 7
        });
    }
</script>