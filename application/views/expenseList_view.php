<script src="/js/expense.js" type="text/javascript"></script>
<section id="toolbar">
    <h2>Расходы</h2>
    <div id="breadcrumb">
        <div class="breadcrumb-item"><a href="/">Главная</a></div>
        <div class="breadcrumb-item"><a>Расходы</a></div>
    </div>
    <div id="control">
        <a href="/expense/add"><div class="control-button add"></div></a>
        <div class="control-button search"></div>
        <div id="search-input" class="hidden"><input id="filter-expense" type="text" class="inputs"/></div>
        <a href="/charts/expense"><div class="control-button graph"></div></a>
    </div>
</section>
<section id="content" class="long">
    <div id="list-expense" class="table">
        <div class="row">
            <div class="cell-caption active-sort sort table-expense_id" sort="desc" column="expense_id">#</div>
            <div class="cell-caption table-name">Наименование</div>
            <div class="cell-caption sort table-price" column="price">Сумма</div>
            <div class="cell-caption sort table-order" column="order">Заказ-наряд</div>
            <div class="cell-caption sort table-date" column="date">Дата</div>
            <div class="sort-none"></div>
        </div>
        <?php 
            $expense = getFilterExpense(0, 'expense_id', 'DESC', '');
            while($row = mysql_fetch_assoc($expense)){
                echo expenseRow($row['expense_id'], $row['name'], $row['price'], $row['order'], $row['date'], $row['time']);
            }
         ?>
    </div>
    <div id="table-info">
        <div id="records-info">Всего записей в таблице: 
            <?php 
                $expenseCount = getCountExpense();
                $expenseCount = mysql_fetch_row($expenseCount);
                echo $expenseCount[0]; 
            ?>
        </div>
        <div id="table-navigation">
            <div class="navigation-btn active-btn" sheet="0">1</div>
            <?php                 
                $countStr = ceil(intval($expenseCount[0]) / 20);
                for($i = 2; $i <= $countStr; $i++){
                    echo '<div class="navigation-btn" sheet="'.($i-1).'">'.$i.'</div>';
                }
            ?>
        </div>
    </div>
</section>


<section id="modal-expense" class="modal">
    <input type="hidden" id="expense_id">
    <div class="modal-header">
        <h3>Информация о доходе</h3>
        <div class="close-button">×</div>
    </div>
    <div class="modal-body">
        <div class="column-item">
            <div class="item-title">Наименование</div>
            <div id="name" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Сумма</div>
            <div id="price" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Заказ-наряд</div>
            <div id="order" class="item-text"></div>
        </div>
        <div class="column-item">
            <div class="item-title">Дата</div>
            <div id="date" class="item-text"></div>
        </div>
    </div>
    <div class="modal-footer">
        <div id="delete-expense" class="footer-button">Удалить</div>
        <div id="edit-expense" class="footer-button">Редактировать</div>
        <div id="print-expense" class="footer-button">Печать</div>
    </div>
</section>