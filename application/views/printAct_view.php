<?php 
    $record_id = $_GET['record_id'];
    $record_date = '';
    $arr_services = array();
    if(isset($_GET['record_id'])){
        $record_id = $_GET['record_id'];
        $result = getPrintOrder($record_id);
        $result = mysql_fetch_assoc($result);
        if($result['record_id']){
            $record_date = $result['date'];
            $date = date_create_from_format('d.m.Y H:i:s', $record_date.' 00:00:00');
            $year = date_format($date, 'Y');
            $client = $result['client'];
            $arr_services = explode(', ', $result['service']);
            $price= $result['price'];
        }
    }
?>

<style>
    div, input{
        font-family:'Times New Roman';
    }
    .caption{
        text-align: center;
    }
    .text{
        margin-top: 20px;
    }
    .city{
        float: left;
    }
    .year{
        float: right;
    }
    .paragraph{
        overflow: auto;
        clear: both;
    }
    .client{
        text-decoration: underline;
    }
    .total{
        text-align: right;
    }
    #act{
        margin-top: 20px;
    }
    #signs{
        width: 100%;
        text-align: center;
        margin-top: 20px;
    }
    .sign{
        display: block;
        width: 200px;
        height: 30px;
        border-bottom: 1px solid;
        margin: auto;
    }
    #signs h6{
        line-height: 12px;
        margin: 0px 0px 20px;
        font-weight: 400;
    }
</style>

<div class="caption">
    <div><h2>АКТ №<?php echo $record_id; ?> от <?php echo $record_date; ?> г.</h2></div>
    <div>выполненых работ / оказаных услуг по заказ-наряду №<?php echo $record_id; ?> от <?php echo $record_date; ?></div>
</div>

<div class="text">
    <div class="city"><b>г.Пермь</b></div>
    <div class="year"><b><?php echo $year; ?></b></div>
    <div class="paragraph">
Общество с ограниченной ответственностью «Хаманн» ОГРН 1155958075096, в лице генерального директора Букалова Никиты Матвеевича, действующего на основании Устава, именуемое в дальнейшем "Исполнитель" и <span class="client"><?php echo $client; ?></span>, именуемое в дальнейшем "Заказчик", совместно именуемые "Стороны", составили и подписали настоящий Акт о том, что Исполнитель выполнил работы (оказал услуги) в соответствии с условиями заказ-наряда №<?php echo $record_id; ?> от <?php echo $record_date; ?>г. , а именно:
    </div>
</div>
<table id="act" border="1px" style="width: 100%;">
    <tr>
        <td>№</td>
        <td>Наименование</td>
        <td>Кол-во</td>
        <td>Цена(руб.)</td>
        <td>Сумма(руб.)</td>
    </tr>
    <?php 
        foreach($arr_services as $i => $service){
            ?>
            <tr>
                <td><div contenteditable="true" class="number edit" style="width: 60px;"><?php echo $i + 1; ?></div></td>
                <td><div contenteditable="true" style="width: 385px;"><?php echo $service; ?></div></td>
                <td><div contenteditable="true" class="count" style="width: 60px;"></div></td>
                <td><div contenteditable="true" class="price" style="width: 120px;"></div></td>
                <td><div contenteditable="true" class="sum" readonly style="width: 120px;"></div></td>
            </tr>
            <?php
        }
    ?>
    <tr>
        <td><div contenteditable="true" class="number edit" style="width: 60px;"></div></td>
        <td><div contenteditable="true" style="width: 385px;"></div></td>
        <td><div contenteditable="true" class="count" style="width: 60px;"></div></td>
        <td><div contenteditable="true" class="price" style="width: 120px;"></div></td>
        <td><div contenteditable="true" class="sum" readonly style="width: 120px;"></div></td>
    </tr>
    <tr class="last">
        <td class="total" colspan="4">Итого:</td>
        <td><div contenteditable="true" class="act total_price"><?php echo $price; ?></div></td>
    </tr>
</table>
<div class="paragraph">
    Исполнитель выполнил все обязательства в полном объёме в срок с надлежащим качеством.<br>
    Заказчик претензий к Исполнителю не имеет.<br>
    Оплата производится в соответствии с условиями заказ-наряда №<?php echo $record_id; ?> от <?php echo date('d.m.Y'); ?>г.
    </div>

<table id="signs" border="0" class="system">
    <tr>
        <td colspan="2"><b>Подписи сторон</b></td>
    </tr>
    <tr>
        <td>Исполнитель</td>
        <td>Заказчик</td>
    </tr>
    <tr>
        <td><div class="sign"></div><h6>(подпись)</h6></td>
        <td><div class="sign"></div><h6>(подпись)</h6></td>
    </tr>
    <tr>
        <td>М.П.</td>
        <td>М.П.</td>
    </tr>
</table>

<button id="print-documnet-btn" style="margin-top: 50px; float: right; width: 100px; height: 30px;">Печать</button>