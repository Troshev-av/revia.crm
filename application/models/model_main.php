<?php

function getNextRecords(){
    $date = date('d.m.Y');
    $time = date('H:i');
    $query = "SELECT `records`.`record_id`,
                `records`.`name`,
                `records`.`price`,
                `records`.`date`,
                `records`.`time`,
                CONCAT(`clients`.`surname`, ', ', `clients`.`name`)  as `client`,
                `clients`.`phone`,
                `clients`.`address`
            FROM `records`
            LEFT JOIN `clients` ON `records`.`record_id` = `clients`.`client_id`
            WHERE (STR_TO_DATE(`records`.`date`,'%d.%m.%Y') > STR_TO_DATE('".$date."','%d.%m.%Y') 
                OR (STR_TO_DATE(`records`.`date`,'%d.%m.%Y') = STR_TO_DATE('".$date."','%d.%m.%Y') 
                AND `records`.`time` > '".$time."')) 
                AND `records`.`is_del` = 0
            GROUP BY `record_id`
            ORDER BY STR_TO_DATE(`records`.`date`,'%d.%m.%Y') ASC, `records`.`time` ASC
            LIMIT 3";
    $result = mysql_query($query);
    return $result;
}

function getNextTasks(){
    $date = date('d.m.Y');
    $time = date('H:i');
    $query = "
        SELECT `tasks`.`task_id`, 
            `tasks`.`name`, 
            `tasks`.`date`, 
            `tasks`.`time`,
            CONCAT(`clients`.`surname`, ', ', `clients`.`name`)  as `client`
        FROM `tasks`
        LEFT JOIN `clients` ON `tasks`.`client_id` = `clients`.`client_id`
        WHERE `tasks`.`is_del` = 0 
            AND (STR_TO_DATE(`tasks`.`date`,'%d.%m.%Y') > STR_TO_DATE('".$date."','%d.%m.%Y') 
            OR  (STR_TO_DATE(`tasks`.`date`,'%d.%m.%Y') = STR_TO_DATE('".$date."','%d.%m.%Y') 
            AND `tasks`.`time` > '".$time."'))
        ORDER BY STR_TO_DATE(`tasks`.`date`,'%d.%m.%Y') ASC, `tasks`.`time` ASC 
        LIMIT 3";
    $result = mysql_query($query);
    return $result;
}

function getCountIncomeInMonth($month){
    $query = "SELECT sum(`price`) FROM `income` WHERE `is_del` = 0 AND `date` LIKE '%".$month."'";
    $result = mysql_query($query);
    return $result;
}

function getCountProfitInMonth($month){
    $query = "SELECT
    (SELECT COALESCE(sum(`price`), 0) FROM `income` WHERE `is_del` = 0 AND `date` LIKE '%".$month."') - 
    (SELECT COALESCE(sum(`price`), 0) FROM `expense` WHERE `is_del` = 0 AND `date` LIKE '%".$month."')";
    $result = mysql_query($query);
    return $result;
}

function getCountExpenseInMonth($month){
    $query = "SELECT sum(`price`) FROM `expense` WHERE `is_del` = 0 AND `date` LIKE '%".$month."'";
    $result = mysql_query($query);
    return $result;
}

?>