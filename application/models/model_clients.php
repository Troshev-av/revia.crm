<?php

function getClient($client_id){
    $query = "SELECT * FROM `clients` WHERE `client_id` = ".$client_id." AND NOT `is_del` = 1";
    $result = mysql_query($query);
    return $result;
}

function getCountClients(){
    $query = "SELECT count(`client_id`) FROM clients WHERE NOT `is_del` = 1";
    $result = mysql_query($query);
    return $result;
}

function getClientInfo($client_id){
    $query = "SELECT * FROM clients WHERE `client_id` = ".$client_id;
    $result = mysql_query($query);
    return $result;
}

function getClientRecords($client_id){
    $date = date('d.m.Y');
    $time = date('H:i');
    $query = "SELECT `record_id`, `name`, `price`, `date`, `time`
            FROM `records`
            WHERE `records`.`client_id` = ".$client_id." 
                AND (STR_TO_DATE(`records`.`date`,'%d.%m.%Y') > STR_TO_DATE('".$date."','%d.%m.%Y') 
                OR  (STR_TO_DATE(`records`.`date`,'%d.%m.%Y') = STR_TO_DATE('".$date."','%d.%m.%Y') 
                AND `records`.`time` > '".$time."')) 
                AND `records`.`is_del` = 0
            ORDER BY STR_TO_DATE(`records`.`date`,'%d.%m.%Y') ASC, `time` ASC
            LIMIT 2";
    $result = mysql_query($query);
    return $result;
}

function getClientTasks($client_id){
    $date = date('d.m.Y');
    $time = date('H:i');
    $query = "SELECT `name`, `date`, `time` 
            FROM `tasks` 
            WHERE `is_del` = 0 
                AND (STR_TO_DATE(`date`,'%d.%m.%Y') > STR_TO_DATE('".$date."','%d.%m.%Y') 
                OR  (STR_TO_DATE(`date`,'%d.%m.%Y') = STR_TO_DATE('".$date."','%d.%m.%Y') 
                AND `time` > '".$time."')) 
                AND `client_id` = ".$client_id." 
            ORDER BY STR_TO_DATE(`date`,'%d.%m.%Y') ASC, `time` ASC 
            LIMIT 2";
    $result = mysql_query($query);
    return $result;
}

function getFilterClients($offset, $column, $order, $str){
    $count = 20;
    $offset = $offset * $count;
    if($column == 'date'){
        $order_str = "STR_TO_DATE(`date`,'%d.%m.%Y') ".$order.", `time` ".$order;
    }else{
        $order_str = '`'.$column.'` '.$order;
    }
    $query = "
        SELECT `client_id`, `surname`, `name`, `date`, `phone`, `email`, `address`
        FROM `clients`
        WHERE (`surname` LIKE '%".$str."%'
            OR `name` LIKE '%".$str."%'
            OR `date` LIKE '%".$str."%'
            OR `phone` LIKE '%".$str."%'
            OR `email` LIKE '%".$str."%'
            OR `address` LIKE '%".$str."%')
            AND `is_del` = 0
        ORDER BY ".$order_str." LIMIT ".$offset.", ".$count;
    $result = mysql_query($query);
    return $result;
}

function addClient($surname, $name, $phone, $email, $address, $comment, $club_card){
    $date = date("d.m.Y");
    $query = "INSERT INTO `clients` (`surname`, `name`, `date`, `phone`, `email`, `address`,  `comment`, `club_card`) VALUES('".$surname."', '".$name."', '".$date."', '".$phone."', '".$email."', '".$address."', '".$comment."', '".$club_card."')";
    $result = mysql_query($query);
    return $result;
}

function updateClient($client_id, $surname, $name, $phone, $email, $address, $comment, $club_card){
    $query = "UPDATE `clients`
              SET `surname` = '".$surname."',
                  `name` = '".$name."',
                  `phone` = '".$phone."',
                  `email` = '".$email."',
                  `address` = '".$address."',
                  `comment` = '".$comment."',
                  `club_card` = '".$club_card."'
              WHERE  `client_id` =  '".$client_id."'";
    $result = mysql_query($query);
    return $result;
}

function deleteClient($client_id){
    $query = "UPDATE `clients` SET `is_del`= 1 WHERE `client_id` = ".$client_id;
    $result = mysql_query($query);
    return $result;
}

?>