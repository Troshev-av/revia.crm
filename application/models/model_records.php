<?php

function updateRecord($record_id, $client_id, $name, $price, $date, $time){
    $query = "
        UPDATE `notifications`
        SET `is_show` = 0,
            `is_del` = 0
        WHERE `type` = '".$type."' AND `item_id` = ".$item_id."
    ";
    $result = mysql_query($query);
    
    $query = "UPDATE `records`
              SET `client_id` = ".$client_id.",
                  `name` = '".$name."',
                  `price` = ".$price.",
                  `date` = '".$date."',
                  `time` = '".$time."'            
              WHERE  `record_id` =  ".$record_id;
    $result = mysql_query($query);
    return $result;
}

function getRecordFromDate($day, $month, $year){
    $query = "
        SELECT
            `records`.`record_id`,
            `records`.`client_id`,
            `clients`.`surname`,
            `clients`.`name`,
            `records`.`date`,
            `records`.`time`
        FROM `records`
        LEFT JOIN `clients` ON `records`.`client_id` = `clients`.`client_id`
        WHERE `records`.`date` = '".$day.".".$month.".".$year."'
        ORDER BY `records`.`date`, `records`.`time`
    ";
    $result = mysql_query($query);
    return $result;
}

function addRecord($client_id, $name, $price, $date, $time){
    $query = "INSERT INTO `records` (`client_id`, `name`, `price`, `date`, `time`) VALUES(".$client_id.", '".$name."', ".$price.", '".$date."', '".$time."')";
    $result = mysql_query($query);
    return $result;
}

function getRecordCount($month, $year){
    $query = "SELECT count(`records`.`record_id`) FROM `records` WHERE `records`.`is_del` = 0 AND `records`.`date` LIKE'%".$month.".".$year."'";
    $result = mysql_query($query);
    return $result;
}

function getRecord($record_id){
    $query = "
        SELECT `records`.`record_id`,
            `records`.`client_id`,
            `records`.`price`, 
            `records`.`name`, 
            `records`.`date`, 
            `records`.`time`,
            CONCAT(`clients`.`surname`, ', ',`clients`.`name`) as `client`,
            `clients`.`phone`,
            `clients`.`address`
        FROM `records`
        LEFT JOIN `clients` ON `records`.`client_id` = `clients`.`client_id`
        WHERE `records`.`record_id` = ".$record_id;
    $result = mysql_query($query);
    return $result;
}

function getCountRecords(){
    $query = "
        SELECT count(`record_id`)
        FROM `records`
        WHERE `is_del` = 0
    ";
    $result = mysql_query($query);
    return $result;
}

function getCountRecordsClient($client_id){
    $query = "
        SELECT count(`record_id`)
        FROM `records`
        WHERE `is_del` = 0 AND `client_id` = ".$client_id;
    $result = mysql_query($query);
    return $result;
}

function getFilterRecords($offset, $column, $order, $str){
    $count = 20;
    $offset = $offset * $count;
    if($column == 'date'){
        $order_str = "STR_TO_DATE(`records`.`date`,'%d.%m.%Y') ".$order.", `records`.`time` ".$order;
    }else{
        $order_str = '`'.$column.'`'." ".$order;
    }
    $query = "
        SELECT
            `records`.`record_id`,
            `records`.`name`,
            `records`.`price`,
            `records`.`date`,
            `records`.`time`,
            CONCAT(`clients`.`surname`, ', ',`clients`.`name`) as `client`,
            `clients`.`address`
        FROM `records`
        LEFT JOIN `clients` ON `records`.`client_id` = `clients`.`client_id`
        WHERE `records`.`is_del` = 0 AND
            (`records`.`record_id` LIKE '%".$str."%' 
            OR `records`.`client_id` LIKE '%".$str."%' 
            OR `records`.`date` LIKE '%".$str."%' 
            OR `records`.`price` LIKE '%".$str."%' 
            OR `records`.`name` LIKE '%".$str."%' 
            OR `clients`.`surname` LIKE '%".$str."%'
            OR `clients`.`name` LIKE '%".$str."%'
            OR `clients`.`address` LIKE '%".$str."%')
        ORDER BY ".$order_str."
        LIMIT ".$offset.", ".$count;
    $result = mysql_query($query);
    return $result;
}

function getRecordsClient($client_id){
    $query = "
        SELECT 
            `records`.`record_id`,
            `records`.`name`,
            `records`.`price`,
            `records`.`date`,
            `records`.`time`,
            CONCAT(`clients`.`surname`, ', ',`clients`.`name`) as `client`,
            `clients`.`phone`,
            `clients`.`address`
        FROM `records`
        LEFT JOIN `clients` ON `records`.`client_id` = `clients`.`client_id`
        WHERE `records`.`is_del` = 0 AND `records`.`client_id` = ".$client_id;
    $result = mysql_query($query);
    return $result;
}

function deleteRecord($record_id){
    $query = "UPDATE `records` SET `is_del`= 1 WHERE `record_id` = ".$record_id;
    $result = mysql_query($query);
    return $result;
}

function getNextRecords(){
    $month = date('m.Y');
    $date = date('d.m.Y');
    $time = date('H:i');
    $query = "SELECT `records`.`record_id`,
                `records`.`name`,
                `records`.`price`,
                `records`.`date`,
                `records`.`time`,
                CONCAT(`clients`.`surname`, ', ', `clients`.`name`)  as `client`,
                `clients`.`phone`,
                `clients`.`address`
            FROM `records`
            LEFT JOIN `clients` ON `records`.`record_id` = `clients`.`client_id`
            WHERE (STR_TO_DATE(`records`.`date`,'%d.%m.%Y') > STR_TO_DATE('".$date."','%d.%m.%Y') 
                OR (STR_TO_DATE(`records`.`date`,'%d.%m.%Y') = STR_TO_DATE('".$date."','%d.%m.%Y') 
                AND `records`.`time` > '".$time."')) 
                AND `records`.`is_del` = 0
            GROUP BY `record_id`
            ORDER BY STR_TO_DATE(`records`.`date`,'%d.%m.%Y') ASC, `time` ASC
            LIMIT 3";
    $result = mysql_query($query);
    return $result;
}

function clientSearch($str){
    $query = "SELECT `client_id`,
            CONCAT(`surname`, ' ',`name`, ', ', `phone`) as `name`
            FROM `clients`
            WHERE `is_del` = 0 AND CONCAT(`surname`, ' ',`name`) LIKE '".$str."%'";
    $result = mysql_query($query);
    return $result;
}

?>