<?php

function getCountClientsInMonth($month){
    $query = "SELECT count(`client_id`) FROM `clients` WHERE NOT `is_del` = 1 AND `date` LIKE '%".$month."'";
    $result = mysql_query($query);
    return $result;
}

function getCountClientsInDay($date){
    $query = "SELECT count(`client_id`) FROM `clients` WHERE `is_del` = 0 AND `date` = '".$date."'";
    $result = mysql_query($query);
    return $result;
}

function getCountClientsInWeek($week){
    $query = "
        SELECT count(`client_id`) 
        FROM `clients` 
        WHERE `is_del` = 0 
            AND STR_TO_DATE(`date`,'%d.%m.%Y') >= STR_TO_DATE('".$week['start']."','%d.%m.%Y') 
            AND  STR_TO_DATE(`date`,'%d.%m.%Y') <= STR_TO_DATE('".$week['finish']."','%d.%m.%Y')";
    $result = mysql_query($query);
    return $result;
}

function getCountRecordsInMonth($month){
    $query = "SELECT count(`record_id`) FROM `records` WHERE `is_del` = 0 AND `date` LIKE '%".$month."'";
    $result = mysql_query($query);
    return $result;
}

function getCountRecordsInDay($date){
    $query = "SELECT count(`record_id`) FROM `records` WHERE `is_del` = 0 AND `date` = '".$date."'";
    $result = mysql_query($query);
    return $result;
}

function getCountRecordsInWeek($week){
    $query = "
        SELECT count(`record_id`) 
        FROM `records` 
        WHERE `is_del` = 0 
            AND STR_TO_DATE(`date`,'%d.%m.%Y') >= STR_TO_DATE('".$week['start']."','%d.%m.%Y') 
            AND  STR_TO_DATE(`date`,'%d.%m.%Y') <= STR_TO_DATE('".$week['finish']."','%d.%m.%Y')";
    $result = mysql_query($query);
    return $result;
}

function getCountTasksInMonth($month){
    $query = "SELECT count(`task_id`) FROM `tasks` WHERE `is_del` = 0 AND `date` LIKE '%".$month."'";
    $result = mysql_query($query);
    return $result;
}

function getCountTasksInDay($date){
    $query = "SELECT count(`task_id`) FROM `tasks` WHERE `is_del` = 0 AND `date` = '".$date."'";
    $result = mysql_query($query);
    return $result;
}

function getCountTasksInWeek($week){
    $query = "
        SELECT count(`task_id`) 
        FROM `tasks` 
        WHERE `is_del` = 0 
            AND STR_TO_DATE(`date`,'%d.%m.%Y') >= STR_TO_DATE('".$week['start']."','%d.%m.%Y') 
            AND  STR_TO_DATE(`date`,'%d.%m.%Y') <= STR_TO_DATE('".$week['finish']."','%d.%m.%Y')";
    $result = mysql_query($query);
    return $result;
}

function getCountIncomeInMonth($month){
    $query = "SELECT sum(`price`) FROM `income` WHERE `is_del` = 0 AND `date` LIKE '%".$month."'";
    $result = mysql_query($query);
    return $result;
}

function getCountIncomeInDay($date){
    $query = "SELECT sum(`price`) FROM `income` WHERE `is_del` = 0 AND `date` = '".$date."'";
    $result = mysql_query($query);
    return $result;
}

function getCountIncomeInWeek($week){
    $query = "
        SELECT sum(`price`)
        FROM `income` 
        WHERE `is_del` = 0 
            AND STR_TO_DATE(`date`,'%d.%m.%Y') >= STR_TO_DATE('".$week['start']."','%d.%m.%Y') 
            AND  STR_TO_DATE(`date`,'%d.%m.%Y') <= STR_TO_DATE('".$week['finish']."','%d.%m.%Y')";
    $result = mysql_query($query);
    return $result;
}

function getCountExpenseInMonth($month){
    $query = "SELECT sum(`price`) FROM `expense` WHERE `is_del` = 0 AND `date` LIKE '%".$month."'";
    $result = mysql_query($query);
    return $result;
}

function getCountExpenseInDay($date){
    $query = "SELECT sum(`price`) FROM `expense` WHERE `is_del` = 0 AND `date` = '".$date."'";
    $result = mysql_query($query);
    return $result;
}

function getCountExpenseInWeek($week){
    $query = "
        SELECT sum(`price`) 
        FROM `expense`
        WHERE `is_del` = 0 
            AND STR_TO_DATE(`date`,'%d.%m.%Y') >= STR_TO_DATE('".$week['start']."','%d.%m.%Y') 
            AND  STR_TO_DATE(`date`,'%d.%m.%Y') <= STR_TO_DATE('".$week['finish']."','%d.%m.%Y')";
    $result = mysql_query($query);
    return $result;
}

function getCountProfitInDay($date){
    $query = "SELECT
    (SELECT COALESCE(sum(`price`), 0) FROM `income` WHERE `is_del` = 0 AND `date` = '".$date."') - 
    (SELECT COALESCE(sum(`price`), 0) FROM `expense` WHERE `is_del` = 0 AND `date` = '".$date."')";
    $result = mysql_query($query);
    return $result;
}

function getCountProfitInWeek($week){
    $query = "
        SELECT
            (
                SELECT COALESCE(sum(`price`), 0) 
                FROM `income` 
                WHERE `is_del` = 0 
                    AND STR_TO_DATE(`date`,'%d.%m.%Y') >= STR_TO_DATE('".$week['start']."','%d.%m.%Y') 
                    AND  STR_TO_DATE(`date`,'%d.%m.%Y') <= STR_TO_DATE('".$week['finish']."','%d.%m.%Y')
            ) - 
        (
            SELECT COALESCE(sum(`price`), 0) 
            FROM `expense`
                WHERE `is_del` = 0 
                    AND STR_TO_DATE(`date`,'%d.%m.%Y') >= STR_TO_DATE('".$week['start']."','%d.%m.%Y') 
                    AND  STR_TO_DATE(`date`,'%d.%m.%Y') <= STR_TO_DATE('".$week['finish']."','%d.%m.%Y')
        )";
    $result = mysql_query($query);
    return $result;
}

function getCountProfitInMonth($month){
    $query = "SELECT
    (SELECT COALESCE(sum(`price`), 0) FROM `income` WHERE `is_del` = 0 AND `date` LIKE '%".$month."') - 
    (SELECT COALESCE(sum(`price`), 0) FROM `expense` WHERE `is_del` = 0 AND `date` LIKE '%".$month."')";
    $result = mysql_query($query);
    return $result;
}

?>