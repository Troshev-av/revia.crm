<?php

function addIncome($client_id, $name, $price, $date, $time){
    $query = "INSERT INTO `income` (`client_id`, `name`, `price`, `date`, `time`) VALUES(".$client_id.", '".$name."', '".$price."', '".$date."', '".$time."')";
    $result = mysql_query($query);
    return $result;
}

function getIncome($income_id){
    $query = "
        SELECT `income`.`income_id`,
            `income`.`client_id`,
            `income`.`name`,
            `income`.`price`,
            `income`.`date`,
            `income`.`time`,
            CONCAT(`clients`.`surname`, ' ',`clients`.`name`) as `client`,
            `clients`.`phone`
        FROM `income`
        LEFT JOIN `clients` ON `income`.`client_id` = `clients`.`client_id`
        WHERE `income`.`income_id` = ".$income_id;
    $result = mysql_query($query);
    return $result;
}

function updateIncome($income_id, $client_id, $service, $price, $date, $time){
    $query = "
        UPDATE `income`
        SET `client_id` = ".$client_id.",
            `name` = '".$service."',
            `price` = ".$price.",
            `date` = '".$date."',
            `time` = '".$time."'
        WHERE `income_id` = ".$income_id;
    $result = mysql_query($query);
    return $result;
}

function getCountIncome(){
    $query = "SELECT count(`income_id`) FROM `income` WHERE `is_del` = 0";
    $result = mysql_query($query);
    return $result;
}

function getFilterIncome($offset, $column, $order, $str){
    $count = 20;
    $offset = $offset * $count;
    if($column == 'date'){
        $order_str = "STR_TO_DATE(`income`.`date`,'%d.%m.%Y') ".$order.", `income`.`time` ".$order;
    }else{
        $order_str = '`'.$column.'` '.$order;
    }
    $query = "
        SELECT `income`.`income_id`,
            `income`.`name`,
            `income`.`price`,
            CONCAT(`income`.`date`, ' ',`income`.`time`) as `date`,
            CONCAT(`clients`.`surname`, ' ',`clients`.`name`) as `client`
        FROM `income`
        LEFT JOIN `clients` ON `income`.`client_id` = `clients`.`client_id`
        WHERE `income`.`is_del` = 0 
            AND (`income`.`income_id` LIKE '%".$str."%' 
            OR `income`.`name` LIKE '%".$str."%' 
            OR `income`.`price` LIKE '%".$str."%' 
            OR CONCAT(`income`.`date`, ' ',`income`.`time`) LIKE '%".$str."%'
            OR  CONCAT(`clients`.`surname`, ' ',`clients`.`name`) LIKE '%".$str."%')
        ORDER BY ".$order_str."
        LIMIT ".$offset.", ".$count;;
    $result = mysql_query($query);
    return $result;
}

function deleteIncome($income_id){
    $query = "UPDATE `income` SET `is_del`= 1 WHERE `income_id` = ".$income_id;
    $result = mysql_query($query);
    return $result;
}

function clientSearch($str){
    $query = "SELECT `client_id`,
            CONCAT(`surname`, ' ',`name`, ', ', `phone`) as `name`
            FROM `clients`
            WHERE `is_del` = 0 AND CONCAT(`surname`, ' ',`name`) LIKE '".$str."%'";
    $result = mysql_query($query);
    return $result;
}

?>