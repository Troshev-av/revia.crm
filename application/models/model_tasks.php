<?php

function addTask($client_id, $name, $date, $time){
    $query = "INSERT INTO `tasks` (`client_id`, `name`, `date`, `time`) VALUES(".$client_id.", '".$name."', '".$date."', '".$time."')";
    $result_task = mysql_query($query);
    return $result_task;
    
}

function getCountTasks(){
    $query = "SELECT count(`task_id`) FROM `tasks` WHERE `is_del` = 0";
    $result = mysql_query($query);
    return $result;
}

function getFilterTasks($offset, $column, $order, $str){
    $count = 20;
    $offset = $offset * $count;
    if($column == 'date'){
        $order_str = "STR_TO_DATE(`tasks`.`date`,'%d.%m.%Y') ".$order.", `tasks`.`time` ".$order;
    }else{
        $order_str = '`'.$column.'` '.$order;
    }
    $query = "
        SELECT `tasks`.`task_id`,
            `tasks`.`name`,
            `tasks`.`date`,
            `tasks`.`time`,
            CONCAT(`clients`.`surname`, ' ',`clients`.`name`) as `client`
        FROM `tasks`
        LEFT JOIN `clients` ON `tasks`.`client_id` = `clients`.`client_id`
        WHERE `tasks`.`is_del` = 0
            AND (`tasks`.`name` LIKE '%".$str."%'
            OR `tasks`.`date` LIKE '%".$str."%'
            OR CONCAT(`clients`.`surname`, ' ',`clients`.`name`) LIKE '%".$str."%')
        ORDER BY ".$order_str."
        LIMIT ".$offset.", ".$count;
    $result = mysql_query($query);
    return $result;
}

function getClientTasks($client_id){
    $query = "
        SELECT `tasks`.`task_id`,
            `tasks`.`name`,
            `tasks`.`date`,
            `tasks`.`time`,
            CONCAT(`clients`.`surname`, ' ',`clients`.`name`) as `client`
        FROM `tasks`
        LEFT JOIN `clients` ON `tasks`.`client_id` = `clients`.`client_id`
        WHERE `tasks`.`client_id` = ".$client_id;
    $result = mysql_query($query);
    return $result;
}

function getTask($task_id){
    $query = "
        SELECT `tasks`.`task_id`,
            `tasks`.`name`,
            `tasks`.`date`,
            `tasks`.`time`,
            `clients`.`client_id`,
            CONCAT(`clients`.`surname`, ' ',`clients`.`name`) as `client`,
            `clients`.`phone`
        FROM `tasks`
        LEFT JOIN `clients` ON `tasks`.`client_id` = `clients`.`client_id`
        WHERE `tasks`.`task_id` = ".$task_id;
    $result = mysql_query($query);
    return $result;
}

function updateTask($task_id, $client_id, $name, $date, $time){
    showNotification('task', $task_id);
    $query = "UPDATE `tasks`
              SET `client_id` = ".$client_id.",
                  `name` = '".$name."',
                  `date` = '".$date."',
                  `time` = '".$time."'
              WHERE `task_id` = ".$task_id;
    $result = mysql_query($query);
    return $result;
}

function deleteTask($task_id){
    $query = "UPDATE `tasks` SET `is_del`= 1 WHERE `task_id` = ".$task_id;
    $result = mysql_query($query);
    return $result;
}

function getNextTasks(){
    $month = date('m.Y');
    $date = date('d.m.Y');
    $time = date('H:i');
    $query = "
        SELECT `tasks`.`task_id`, 
            `tasks`.`name`, 
            `tasks`.`date`, 
            `tasks`.`time`,
            CONCAT(`clients`.`surname`, ', ', `clients`.`name`)  as `client`
        FROM `tasks`
        LEFT JOIN `clients` ON `tasks`.`client_id` = `clients`.`client_id`
        WHERE `tasks`.`is_del` = 0 
            AND (STR_TO_DATE(`tasks`.`date`,'%d.%m.%Y') > STR_TO_DATE('".$date."','%d.%m.%Y') 
            OR  (STR_TO_DATE(`tasks`.`date`,'%d.%m.%Y') = STR_TO_DATE('".$date."','%d.%m.%Y') 
            AND `tasks`.`time` > '".$time."'))
        ORDER BY STR_TO_DATE(`tasks`.`date`,'%d.%m.%Y') ASC, `time` ASC 
        LIMIT 3";
    $result = mysql_query($query);
    return $result;
}

function clientSearch($str){
    $query = "SELECT `client_id`,
            CONCAT(`surname`, ' ',`name`, ', ', `phone`) as `name`
            FROM `clients`
            WHERE `is_del` = 0 AND CONCAT(`surname`, ' ',`name`) LIKE '".$str."%'";
    $result = mysql_query($query);
    return $result;
}

?>