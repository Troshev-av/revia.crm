<?php

function getPrintOrder($record_id){
    $query = "
        SELECT `records`.`record_id`, 
                CONCAT(`clients`.`surname`, ' ',`clients`.`name`) as `client`,
                `clients`.`phone`,
                `cars`.`name` as `car`,
                `cars`.`state_number`,
                `cars`.`vin`,
                `cars`.`class`,
                `cars`.`color`,
                `cars`.`year`,
                `cars`.`mileage`,
                `records`.`price`,
                `records`.`date`,
                GROUP_CONCAT(`services`.`name` SEPARATOR ', ') as `service`
        FROM `records`, `clients`, `cars`, `record_services`, `services`
        WHERE `records`.`record_id` = ".$record_id." AND `clients`.`client_id` = `records`.`client_id` AND `records`.`car_id` = `cars`.`car_id` AND `record_services`.`record_id` = `records`.`record_id` AND `services`.`service_id` = `record_services`.`service_id`
    ";
    $result = mysql_query($query);
    return $result;  
}

function getPrintExpense($expense_id){
    $query = "
        SELECT `expense`.`expense_id`, 
                `expense`.`name`,
                `expense`.`price`,
                `expense`.`order`,
                `expense`.`date`,
                `expense`.`time`
        FROM `expense`
        WHERE `expense`.`expense_id` = ".$expense_id;
    $result = mysql_query($query);
    return $result;  
}

function addIncomeFromOrder($record_id, $service, $price){
    $date = date('d.m.Y');
    $time = date('H:i');
    $query = "
        INSERT INTO `income` (
            `client_id`, 
            `service`, 
            `price`, 
            `date`, 
            `time`
        ) 
        VALUES (
            (SELECT `client_id` FROM `records` Where `record_id` = ".$record_id."), 
            '".$service."', 
            '".$price."', 
            '".$date."', 
            '".$time."'
        )
    ";
    $result = mysql_query($query);
    return $result;  
}
?>