<?php

function addSource($name, $phone, $address, $comment){
    $date = date("d.m.Y");
    $query = "INSERT INTO `sources` (`name`, `phone`, `address`, `comment`, `date`) VALUES('".$name."', '".$phone."', '".$address."', '".$comment."', '".$date."')";
    $result = mysql_query($query);
    return $result;
}

function getSource($source_id){
    $query = "
        SELECT `sources`.`source_id`,
            `sources`.`name`,
            `sources`.`phone`,
            `sources`.`address`,
            `sources`.`comment`,
            `sources`.`date`
        FROM `sources`
        WHERE `sources`.`source_id` = ".$source_id;
    $result = mysql_query($query);
    return $result;
}

function updateSource($source_id, $name, $phone, $address, $comment){
    $date = date("d.m.Y");
    $query = "
        UPDATE `sources`
        SET
            `name` = '".$name."',
            `phone` = '".$phone."',
            `address` = '".$address."',
            `comment` = '".$comment."',
            `date` = '".$date."'
        WHERE `source_id` = ".$source_id;
    $result = mysql_query($query);
    return $result;
}

function getCountSources(){
    $query = "SELECT count(`source_id`) FROM `sources` WHERE `is_del` = 0";
    $result = mysql_query($query);
    return $result;
}


function getFilterSources($offset, $column, $order, $str){
    $count = 20;
    $offset = $offset * $count;
    if($column == 'date'){
        $order_str = "STR_TO_DATE(`sources`.`date`,'%d.%m.%Y') ".$order;
    }else{
        $order_str = '`'.$column.'` '.$order;
    }
    $query = "
        SELECT
            `sources`.`source_id`,
            `sources`.`name`,
            `sources`.`phone`,
            `sources`.`address`,
            `sources`.`comment`,
            `sources`.`date`
        FROM `sources`
        WHERE `sources`.`is_del` = 0 
            AND (`sources`.`source_id` LIKE '%".$str."%' 
            OR `sources`.`name` LIKE '%".$str."%' 
            OR `sources`.`phone` LIKE '%".$str."%' 
            OR `sources`.`address` LIKE '%".$str."%' 
            OR `sources`.`date` LIKE '%".$str."%')
        ORDER BY ".$order_str."
        LIMIT ".$offset.", ".$count;
    $result = mysql_query($query);
    return $result;
}

function deleteSources($source_id){
    $query = "UPDATE `sources` SET `is_del`= 1 WHERE `source_id` = ".$source_id;
    $result = mysql_query($query);
    return $result;
}

?>