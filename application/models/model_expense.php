<?php

function addExpense($name, $price, $order, $date, $time){
    if (!$order) {
        $order = "NULL";
    }
    $query = "INSERT INTO `expense` (`name`, `price`, `order`, `date`, `time`) VALUES('".$name."', ".$price.", ".$order.", '".$date."', '".$time."')";
    $result_expense = mysql_query($query);
    $result = Array('expense_id' => mysql_insert_id(), 'result' => $result_expense);
    return $result;
}

function getExpense($expense_id){
    $query = "
        SELECT `expense`.`expense_id`,
            `expense`.`name`,
            `expense`.`price`,
            `expense`.`order`,
            `expense`.`date`,
            `expense`.`time`
        FROM `expense`
        WHERE `expense`.`expense_id` = ".$expense_id;
    $result = mysql_query($query);
    return $result;
}

function updateExpense($expense_id, $name, $price, $order, $date, $time){
    if (!$order) {
        $order = "NULL";
    }
    $query = "UPDATE `expense`
              SET
                  `name` = '".$name."',
                  `price` = ".$price.",
                  `order` = ".$order.",
                  `date` = '".$date."',
                  `time` = '".$time."'
              WHERE `expense_id` = ".$expense_id;
    $result = mysql_query($query);
    return $result;
}

function getCountExpense(){
    $query = "SELECT count(`expense_id`) FROM `expense` WHERE `is_del` = 0";
    $result = mysql_query($query);
    return $result;
}

function getFilterExpense($offset, $column, $order, $str){
    $count = 20;
    $offset = $offset * $count;
    if($column == 'date'){
        $order_str = "STR_TO_DATE(`expense`.`date`,'%d.%m.%Y') ".$order.", `expense`.`time` ".$order;
    }else{
        $order_str = '`'.$column.'` '.$order;
    }
    $query = "
        SELECT `expense`.`expense_id`,
            `expense`.`name`,
            `expense`.`price`,
            `expense`.`order`,
            `expense`.`date`,
            `expense`.`time`
        FROM `expense` 
        WHERE `expense`.`is_del` = 0 
            AND (`expense`.`expense_id` LIKE '%".$str."%' 
            OR `expense`.`name` LIKE '%".$str."%' 
            OR `expense`.`price` LIKE '%".$str."%' 
            OR `expense`.`order` LIKE '%".$str."%' 
            OR CONCAT(`expense`.`date`, ' ',`expense`.`time`) LIKE '%".$str."%')
        ORDER BY ".$order_str."
        LIMIT ".$offset.", ".$count;
    $result = mysql_query($query);
    return $result;
}

function deleteExpense($expense_id){
    $query = "UPDATE `expense` SET `is_del`= 1 WHERE `expense_id` = ".$expense_id;
    $result = mysql_query($query);
    return $result;
}

?>