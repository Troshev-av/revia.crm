<?php
/*
Класс-маршрутизатор для определения запрашиваемой страницы.
> цепляет классы контроллеров и моделей;
> создает экземпляры контролеров страниц и вызывает действия этих контроллеров.
*/
class Route
{

	static function start($login)
	{
		// контроллер и действие по умолчанию
        $controller_name = 'main';
        $action_name = 'index';
		$model_path = "application/models/";
		$controller_path = "application/controllers/";
        
        // удаление get параметров из строки
        $get_position = mb_strpos($_SERVER['REQUEST_URI'], '?');
        if ($get_position){
            $url = substr($_SERVER['REQUEST_URI'], 0, $get_position);
        }else{
            $url = $_SERVER['REQUEST_URI'];
        }
        
		$routes = explode('/', $url);

		// получаем имя контроллера
		if ( !empty($routes[1]) )
		{	
			$controller_name = $routes[1];
		}
		
		// получаем имя экшена
		if ( !empty($routes[2]) )
		{
			$action_name = $routes[2];
		}
        
		if($login){
            // Проверяем правильность раздела
            if(file_exists($controller_path.'controller_'.$controller_name.'.php')){
                // Проверяем доступ к разделу
                $page_access = getUserAccess($_COOKIE["user_id"], $controller_name);
                if(!$page_access[$controller_name]) {
                    $start_page = $page_access['start_page'];
                    header('Location: /'.$start_page);
                }
            }else{
                    header('Location: /404');
            }
        }else{
            $model_name = 'login';
            $controller_name = 'login';
            $action_name = 'index';
        }
        
        $model_name = 'model_'.$controller_name;
        $controller_name = 'controller_'.$controller_name;
        $action_name = 'action_'.$action_name;
		

		// подцепляем файл с классом модели (файла модели может и не быть)

		$model_file = strtolower($model_name).'.php';
		$model_path = $model_path.$model_file;
		if(file_exists($model_path))
		{
			include "application/models/".$model_file;
		}
        
		// подцепляем файл с классом контроллера
		$controller_file = strtolower($controller_name).'.php';
		$controller_path = $controller_path.$controller_file;
		if(file_exists($controller_path))
		{
			include "application/controllers/".$controller_file;
		}
		
		// создаем контроллер
		$controller = new $controller_name;
		$action = $action_name;
		
		if(method_exists($controller, $action))
		{
			// вызываем действие контроллера
			$controller->$action($get);
		}
	
	}
    
}