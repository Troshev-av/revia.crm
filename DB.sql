SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `clients` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` text NOT NULL,
  `name` text NOT NULL,
  `date` text NOT NULL,
  `phone` text NOT NULL,
  `email` text NOT NULL,
  `address` text NOT NULL,
  `comment` text NOT NULL,
  `club_card` text NOT NULL,
  `is_del` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`client_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

TRUNCATE TABLE `clients`;
INSERT INTO `clients` (`client_id`, `surname`, `name`, `date`, `phone`, `email`, `address`, `comment`, `club_card`, `is_del`) VALUES
(1, 'Трошев', 'Алексей', '11.11.2017', '+7 (950) 461-9968', 'troshev-av@yandex.ru', 'Пермь, ул. Кочегаров 71-65', 'Тестовая запись', '0001', 0),
(2, 'Тест 11', 'Тест 1а1', '18.11.2017', '+7(999)9999998', 'ty@asd1', 'нету1', '12313241', '1111', 0);

CREATE TABLE IF NOT EXISTS `expense` (
  `expense_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `price` int(11) NOT NULL,
  `order` int(11) DEFAULT NULL,
  `date` text NOT NULL,
  `time` text NOT NULL,
  `is_del` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`expense_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

TRUNCATE TABLE `expense`;
INSERT INTO `expense` (`expense_id`, `name`, `price`, `order`, `date`, `time`, `is_del`) VALUES
(1, 'Тест', 50, 1, '11.11.2017', '20:33', 0),
(2, 'Тест 2', 2000, 1, '18.11.2017', '16:31', 0);

CREATE TABLE IF NOT EXISTS `income` (
  `income_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `price` int(11) NOT NULL,
  `date` text NOT NULL,
  `time` text NOT NULL,
  `is_del` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`income_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

TRUNCATE TABLE `income`;
INSERT INTO `income` (`income_id`, `client_id`, `name`, `price`, `date`, `time`, `is_del`) VALUES
(1, 1, 'Тест', 200, '11.11.2017', '20:34', 0),
(2, 2, 'Тест 2', 1200, '18.11.2017', '15:53', 0),
(3, 1, 'Тест 3', 300, '11.11.2017', '12:12', 0);

CREATE TABLE IF NOT EXISTS `notifications` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinytext,
  `item_id` int(11) DEFAULT '0',
  `is_del` int(11) DEFAULT '0',
  `is_show` int(11) DEFAULT '0',
  PRIMARY KEY (`notification_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

TRUNCATE TABLE `notifications`;
INSERT INTO `notifications` (`notification_id`, `type`, `item_id`, `is_del`, `is_show`) VALUES
(1, 'record', 1, 0, 1),
(2, 'record', 2, 0, 1),
(3, 'task', 1, 0, 1),
(4, 'task', 2, 0, 1),
(5, 'record', 3, 0, 0);

CREATE TABLE IF NOT EXISTS `records` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `price` int(11) NOT NULL,
  `date` text NOT NULL,
  `time` text NOT NULL,
  `is_del` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`record_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

TRUNCATE TABLE `records`;
INSERT INTO `records` (`record_id`, `client_id`, `name`, `price`, `date`, `time`, `is_del`) VALUES
(1, 1, 'Тест 1', 1000, '11.11.2017', '20:13', 0),
(2, 2, 'Тест 2', 10001, '18.11.2017', '23:15', 0),
(3, 1, 'Тест 3', 1156, '19.11.2017', '01:20', 0);

CREATE TABLE IF NOT EXISTS `sources` (
  `source_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `date` text NOT NULL,
  `phone` text NOT NULL,
  `address` text NOT NULL,
  `comment` text NOT NULL,
  `is_del` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`source_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

TRUNCATE TABLE `sources`;
INSERT INTO `sources` (`source_id`, `name`, `date`, `phone`, `address`, `comment`, `is_del`) VALUES
(1, 'Тест', '11.11.2017', '+7(950)461-9968', 'нету', 'Тест', 0),
(2, 'Тест 2', '18.11.2017', '+7(999)9999990', 'тест ', 'тест \ndfgdfg', 0);

CREATE TABLE IF NOT EXISTS `tasks` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `date` text NOT NULL,
  `time` text NOT NULL,
  `is_del` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`task_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

TRUNCATE TABLE `tasks`;
INSERT INTO `tasks` (`task_id`, `client_id`, `name`, `date`, `time`, `is_del`) VALUES
(1, 1, 'Тест 1', '11.11.2017', '20:11', 0),
(2, 2, 'Тест 2', '18.11.2017', '23:18', 0);

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` text,
  `password` text,
  `name` text,
  `user_type_id` int(11) DEFAULT NULL,
  `is_del` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

TRUNCATE TABLE `users`;
INSERT INTO `users` (`user_id`, `login`, `password`, `name`, `user_type_id`, `is_del`) VALUES
(1, 'admin', '$2y$10$YpRjVwRAop2J90t3t14A/.pdfGf82foiVttcgQII6UHOkF0dGoCdu', 'Администратор 1', 1, 0);

CREATE TABLE IF NOT EXISTS `user_types` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `start_page` text,
  `main` tinyint(1) DEFAULT '0',
  `clients` tinyint(1) DEFAULT '0',
  `records` tinyint(1) DEFAULT '0',
  `tasks` tinyint(1) DEFAULT '0',
  `expense` tinyint(1) DEFAULT '0',
  `income` tinyint(1) DEFAULT '0',
  `charts` tinyint(1) DEFAULT '0',
  `print` tinyint(1) DEFAULT '0',
  `sources` tinyint(1) NOT NULL DEFAULT '0',
  `is_del` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`user_type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

TRUNCATE TABLE `user_types`;
INSERT INTO `user_types` (`user_type_id`, `name`, `start_page`, `main`, `clients`, `records`, `tasks`, `expense`, `income`, `charts`, `print`, `sources`, `is_del`) VALUES
(1, 'Администратор', 'main', 1, 1, 1, 1, 1, 1, 1, 1, 1, 0);
